package co.com.arlsura.personas.ui.DefinidasParaTrabajador;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.widget.Switch;

import org.junit.After;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

public class DefinicionesPerfilTrabajador {


    EsperarTiempo tiempoEsperar = new EsperarTiempo();
    CerrarSesion cerrarSesion = new CerrarSesion();

    public void lLenarPantallaIngresoTrabajadorExterno() {
        tiempoEsperar.tiempoEsperar();
        onView(withId(R.id.edtName)).perform(typeText("Juan Alejandro Garzon Perez"), closeSoftKeyboard());
        tiempoEsperar.tiempoEsperar();
        onView(withId(R.id.edtCompanyName)).perform(typeText("SURA"), closeSoftKeyboard());

        onView(withId(R.id.edtCargoTrabajador)).perform(typeText("Developer"), closeSoftKeyboard());

        onView(withId(R.id.edtDate)).perform(click());
        tiempoEsperar.tiempoEsperar();

        onView(withText(R.string.agree))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.edtHour)).perform(click());
        tiempoEsperar.tiempoEsperar();

        onView(withText(R.string.agree))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtInstalaciones)).perform(click());
        onView(withText("CLIENTE")).perform(click());

        llenarPantallaDeNivelesYProcesosTrabajadorExterno();
    }


    public void llenarPantallaDeNivelesYProcesosTrabajadorExterno() {

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        tiempoEsperar.tiempoEsperar();
       // onView(withId(R.id.edtArea)).perform(click());
        //onView(withText("Almacen")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel1)).perform(click());
        onView(withText("AREAMOVIL")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel2)).perform(click());
        onView(withText("POPAYAN")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel3)).perform(click());
        onView(withText("PROCESOS POPAYAN")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());


        onView(withId(R.id.edtNivel4)).perform(click());
        onView(withText("EJEMPLO NIVEL 4")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel5)).perform(click());
        onView(withText("EJEMPLO NIVEL 5")).perform(click());
        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

        tiempoEsperar.tiempoEsperar();

        onView(withId(R.id.edtArea)).perform(click());
        onView(withText("DESARROLLO")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_External)).perform(ViewActions.swipeUp());

/*
        onView(withId(R.id.edtNivel6)).perform(click());
        onView(withText("PROCESO NIVEL 5")).perform(click());


        onView(withId(R.id.edtNivel8)).perform(click());
        onView(withText("AYUDANTÍA")).perform(click());*/
    }

    public void llenarPantallaReporteTrabajadorExterno(String Categoria, String TipoReporte, String Mensaje) {

        onView(withId(R.id.edtCategoria)).perform(click());



        onView(withText(Categoria))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.edtTipoReporte)).perform(click());

        onView(withText(TipoReporte))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.edtIncidente)).perform(typeText(Mensaje), closeSoftKeyboard());
    }

    public void llenarPantallaDeAccionesATomarTrabajadorExterno(String Estado, String Observacion, String MensajeObservacion) {
        if (Estado != "") {
            onView(withId(R.id.edtEstado)).perform(click());
            onView(withText(Estado)).perform(click());
        }
        if (Observacion == "NO") {
            onView(withId(R.id.radioNO)).perform(click());
        }
        if (Observacion == "Si, un diálogo con oportunidad de mejora") {
            onView(withId(R.id.radioMejora)).perform(click());
            onView(withId(R.id.edtCual)).perform(typeText(MensajeObservacion), closeSoftKeyboard());
            tiempoEsperar.tiempoEsperar();
        }
        if (Observacion == "Si, di las felicitaciones por un buen trabajo") {
            onView(withId(R.id.radioFelicitaciones)).perform(click());
        }

    }


    //trabajador interno

    public void llenarPantallaReporteTrabajadorInterno(String Categoria, String TipoReporte, String Mensaje) {

        onView(withId(R.id.edtCategoria)).perform(click());
        onView(withText(Categoria)).perform(click());

        onView(withId(R.id.edtTipoReporte)).perform(click());
        onView(withText(TipoReporte)).perform(click());

        onView(withId(R.id.edtIncidente)).perform(typeText(Mensaje), closeSoftKeyboard());
    }

    public void LlenarPantallaDeAccionesATomarTrabajadorInterno(String Estado, String Observacion, String MensajeObservacion) {
        if (Estado != "") {
            onView(withId(R.id.edtEstado)).perform(click());
            onView(withText(Estado)).perform(click());
        }
        if (Observacion == "NO") {
            onView(withId(R.id.radioNO)).perform(click());
        }
        if (Observacion == "Si, un diálogo con oportunidad de mejora") {
            onView(withId(R.id.radioMejora)).perform(click());
            onView(withId(R.id.edtCual)).perform(typeText(MensajeObservacion), closeSoftKeyboard());
            tiempoEsperar.tiempoEsperar();
        }
        if (Observacion == "Si, di las felicitaciones por un buen trabajo") {
            onView(withId(R.id.radioFelicitaciones)).perform(click());
        }

    }


    public void lLenarPantallaIngresoTrabajadorInterno() {
        tiempoEsperar.tiempoEsperar();

        onView(withId(R.id.edtCargoTrabajador)).perform(typeText("Developer"), closeSoftKeyboard());
        tiempoEsperar.tiempoEsperar();

        onView(withId(R.id.edtDate)).perform(click());
        onView(withText(R.string.agree))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.edtHour)).perform(click());
        onView(withText(R.string.agree)).perform(click());

        onView(withId(R.id.edtInstalaciones)).perform(click());

        onView(withText("CLIENTE")).perform(click());

    }

    public void llenarPantallaDeNivelesYProcesosTrabajadorInterno() {

        onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());

        tiempoEsperar.tiempoEsperar();

        onView(withId(R.id.edtArea)).perform(click());
        onView(withText("Almacen")).perform(click());

        //onView(withText(R.string.agree)).perform(click());
        onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel1)).perform(click());
        onView(withText("AREAMOVIL")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel2)).perform(click());
        onView(withText("POPAYAN")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel3)).perform(click());
        onView(withText("PROCESOS POPAYAN")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());


        onView(withId(R.id.edtNivel4)).perform(click());
        onView(withText("EJEMPLO NIVEL 4")).perform(click());

        onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());

        onView(withId(R.id.edtNivel5)).perform(click());
        onView(withText("EJEMPLO NIVEL 5")).perform(click());

        //onView(ViewMatchers.withId(R.id.Activity_Ingress_Intern)).perform(ViewActions.swipeUp());


        /*onView(withId(R.id.edtNivel6)).perform(click());
        onView(withText("PROCESO NIVEL 5")).perform(click());


        onView(withId(R.id.edtNivel8)).perform(click());
        onView(withText("AYUDANTÍA")).perform(click());*/
    }


    public void llenarPantallaDeValoracionDeComportamientos(String Valoracion, String Mensaje, String SeleccionRadioButton) {

        if (SeleccionRadioButton == "si") {
            onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText(Valoracion)).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText(Mensaje), closeSoftKeyboard());

            onView(withId(R.id.radioSi)).perform(click());
            onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(ViewActions.swipeUp());
            onView(withId(R.id.edtSituacion)).perform(typeText(Mensaje), closeSoftKeyboard());
            onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(ViewActions.swipeUp());
            onView(withId(R.id.edtMejora)).perform(typeText(Mensaje), closeSoftKeyboard());
            onView(withId(R.id.imgNext)).perform(click());
            onView(withId(R.id.btnStatusAction)).perform(click());

            cerrarSesion.CerrarSesion();
            onView(withText(R.string.observador_comportamientos)).perform(click());
        } else if (SeleccionRadioButton == "no") {

            onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText(Valoracion)).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText(Mensaje), closeSoftKeyboard());

            onView(withId(R.id.radioNo)).perform(click());
            onView(withId(R.id.imgNext)).perform(click());

            onView(withId(R.id.btnStatusAction)).perform(click());

            cerrarSesion.CerrarSesion();

        } else if (Valoracion == "" && Mensaje == "" && SeleccionRadioButton == "") {
            onView(withId(R.id.imgNext)).perform(click());
            onView(withId(R.id.btnStatusAction)).perform(click());
            onView(withId(R.id.lblDescripcion)).perform(click());

            //para finalizar y cerrar sesion para evitar que otras pruebas fallen por que en caso tal quedaria guardado
            //que hay observaciones pendientes.
            onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText("No aplica")).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText("Terminando..."), closeSoftKeyboard());
            onView(withId(R.id.radioNo)).perform(click());
            onView(withId(R.id.imgNext)).perform(click());
            onView(withId(R.id.btnStatusAction)).perform(click());
            cerrarSesion.CerrarSesion();
        }
    }
}
