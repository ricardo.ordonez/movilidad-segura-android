package co.com.arlsura.personas.ui.base;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.hamcrest.CoreMatchers;

import java.util.Objects;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import timber.log.Timber;



public abstract class BaseTest {

  private static DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();
  private static CerrarSesion cerrarSesion = new CerrarSesion();

  private UiDevice device;

  public void SetUp() throws InterruptedException {
    Timber.e("setup");
    Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
        .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
    device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
  }

/*
  protected void cerrarSession(Activity context) throws UiObjectNotFoundException {
    UiObject salirButton =
        device.findObject(new UiSelector().text(context.getString(R.string.salir)));
    salirButton.click();

    UiObject aceptButton =
        device.findObject(new UiSelector().text(context.getString(R.string.agree)));
    aceptButton.click();
  }
  */
  /*protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
          throws UiObjectNotFoundException {
    Timber.e("MensajeTrabajadores");
    if (allow) {
      Mensaje(context.getString(R.string.agree), Responsable);
    }
  }*/

  /*
  private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
    Timber.e("MensajeTrabajadores");
    UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
    allowButton.click();

    if (!Objects.equals(Responsable, "")) {
      llenarDatos.LlenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
          "Felicitaciones", "XXXXXXXXX");

      onView(withId(R.id.btnSiguiente)).perform(click());

      llenarDatos.LlenarPantallaDeAccionesATomarTrabajadorExterno("",
          "Si, di las felicitaciones por un buen trabajo", "");

      onView(withId(R.id.btnSiguiente)).perform(click());

      onView(withId(R.id.edtResponsable)).perform(click());

      onView(withText(Responsable)).perform(click());

      onView(withId(R.id.btnSiguiente)).perform(click());

      onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));

      onView(withId(R.id.btnHacerOtro)).perform(click());
      cerrarSesion.CerrarSesion();
    } else {
      onView(withId(R.id.lblDescripcion)).perform(click());
      llenarDatos.LlenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
    }
  }*/
}
