package co.com.arlsura.personas.ui.Definidas;

import android.support.test.espresso.action.ViewActions;

import co.com.arlsura.personas.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;

public class InicioDeSesion {

    public void IniciarSesionConUsuarioYClaveTrabajador(String TipoDocumentoEmpresa, String DocumentoEmpresa, String TipoDocumentoTrabajdor,
                                              String DocumentoTrabajador) {
        onView(withId(R.id.edtTipoDocumentoEmpresa)).perform(click());
        onView(withText(endsWith(TipoDocumentoEmpresa))).perform(click());

        onView(withId(R.id.edtNumeroDocumentoEmpresa)).perform(typeText(DocumentoEmpresa),
                ViewActions.closeSoftKeyboard());


        onView(withId(R.id.edtTipoDocumentoPersona)).perform(click());
        onView(withText(endsWith(TipoDocumentoTrabajdor))).perform(click());

        onView(withId(R.id.edtNumeroDocumentoPersona)).perform(typeText(DocumentoTrabajador),
                ViewActions.closeSoftKeyboard());
    }

    public void IniciarSesionConUsuarioYClave(String TipoDocumento, String Documento, String Usuario) {
        onView(withId(R.id.edtTipoDocumentoEmpresa)).perform(click());
        onView(withText(endsWith(TipoDocumento))).perform(click());
        onView(withId(R.id.edtNumeroDocumentoEmpresa)).perform(typeText(Documento),
                ViewActions.closeSoftKeyboard());

        onView(withId(R.id.edtNumeroDocumentoPersona)).perform(click());
        onView(withText(endsWith(TipoDocumento))).perform(click());
        onView(withId(R.id.edtPasswordPersona)).perform(typeText(Usuario),
                ViewActions.closeSoftKeyboard());
    }

    public void IniciarSesionConUsuarioYClaveObservador(String TipoDocumento, String Documento, String Usuario, String Clave) {
        onView(withId(R.id.edtTipoDocumentoEmpresa)).perform(click());
        onView(withText(endsWith(TipoDocumento))).perform(click());

        onView(withId(R.id.edtNumeroDocumentoEmpresa)).perform(typeText(Documento),
                ViewActions.closeSoftKeyboard());

        onView(withId(R.id.edtNumeroDocumentoPersona)).perform(typeText(Usuario),
                ViewActions.closeSoftKeyboard());


        onView(withId(R.id.edtPasswordPersona)).perform(typeText(Clave),
                ViewActions.closeSoftKeyboard());



    }
    public void SeleccionarLaOpcionDeObservador(){
        onView(withId(R.id.btnObservador)).perform(click());

    }
}
