package co.com.arlsura.personas.ui.NuevaObservacionParaObservadores;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaObservador.DefinidasPerfilObservador;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.Splash;
import co.com.arlsura.personas.ui.base.BaseTest;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.util.Objects;

import timber.log.Timber;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NuevaObservacion extends BaseTest {

    @Rule
    public IntentsTestRule<Splash> mActivityTestRule =
            new IntentsTestRule<>(Splash.class);

    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private DefinidasPerfilObservador llenarDatos = new DefinidasPerfilObservador();
    private EsperarTiempo esperarTiempo = new EsperarTiempo();
    private static DefinicionesPerfilTrabajador llenarDatosT = new DefinicionesPerfilTrabajador();
    private static CerrarSesion cerrarSesion = new CerrarSesion();
    private UiDevice device;


    @Before


    public void SetUp() {
        esperarTiempo.tiempoEsperar();
        iniciarsesion.SeleccionarLaOpcionDeObservador();

        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);
            initSetup();
        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "");
                initSetup();
            } catch (Exception ex) {
                e.printStackTrace();
                initSetup();
            }
        }
    }

    private void initSetup() {
        Timber.e("initSetup");
        onView(withText(
                endsWith(mActivityTestRule.getActivity().getString(R.string.login_observer)))).perform(
                click());
        iniciarsesion.IniciarSesionConUsuarioYClaveObservador("NIT", "1037606401", "1037606402", "clave");
        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.ingresar)))).perform(
                click());
    }

    @Test
    public void a_testNuevaObservacionNOAplica() {

        Timber.e("a_testNuevaObservacionNOAplica");
        onView(withId(R.id.btnNuevaObservacion)).check((matches(withText("NUEVA OBSERVACIÓN"))));

        onView(withId(R.id.btnNuevaObservacion)).perform(click());

        llenarDatos.lLenarPantallaNuevaObservacion();
        esperarTiempo.tiempoEsperar();

        onView(withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();

        onView(withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());

        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.lblDescripcion)).perform(click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica",
                "No se observan comportamientos inseguros", "no");
        esperarTiempo.tiempoEsperar();

    }

    @Test
    public void b_testIngresarAPantallaDeNuevaObservacionYSeleccionarSiParaIngresarMasDescripciones() {
        onView(withId(R.id.btnNuevaObservacion)).check((matches(withText("NUEVA OBSERVACIÓN"))));
        onView(withId(R.id.btnNuevaObservacion)).perform(click());

        llenarDatos.lLenarPantallaNuevaObservacion();

        onView(withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
            click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();

        onView(withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
            click());

        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.lblDescripcion)).perform(click());
        llenarDatos.llenarPantallaDeValoracionDeComportamientos("Comportamiento seguro",
    "No se observan comportamientos inseguros", "si");

        esperarTiempo.tiempoEsperar();


    }

    @Test
    public void c_testIngresarAPantallaDeNuevaObservacionYSeleccionarComportamientoAintervenir() {

        onView(withId(R.id.btnNuevaObservacion)).check((matches(withText(R.string.nueva_observacion))));
        onView(withId(R.id.btnNuevaObservacion)).perform(click());

        llenarDatos.lLenarPantallaNuevaObservacion();

        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();

        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());

        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.lblDescripcion)).perform(click());
        llenarDatos.llenarPantallaDeValoracionDeComportamientos("Comportamiento a intervenir",
                "No se observan comportamientos inseguros", "si");
    }

    @Test
    public void d_testIngresarAPantallaDeNuevaObservacionSinIngresarValoracionesDebeAvisar() {

        onView(withText(
                endsWith(mActivityTestRule.getActivity().getString(R.string.nueva_observacion)))).perform(
                click());

        llenarDatos.lLenarPantallaNuevaObservacion();

        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();
        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        onView(withId(R.id.lblDescripcion)).perform(click());
        llenarDatos.llenarPantallaDeValoracionDeComportamientos("", "", "");
    }

    @Test
    public void e_testIngresarAPantallaDeNuevaObservacionYdebePermitirmeDesplazarConGestoEnLaPantallaDeValoracionDeComportamientosValoracionSinTerminar() {
        onView(withText(
                endsWith(mActivityTestRule.getActivity().getString(R.string.nueva_observacion)))).perform(
                click());

        llenarDatos.lLenarPantallaNuevaObservacion();

        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();
        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        onView(withId(R.id.lblDescripcion)).perform(click());

        esperarTiempo.tiempoEsperar();
        onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(
                ViewActions.swipeLeft());
        esperarTiempo.tiempoEsperar();
        onView(ViewMatchers.withId(R.id.scroll_Statatus_Fail)).perform(ViewActions.swipeRight());
        esperarTiempo.tiempoEsperar();

        llenarDatos.llenarPantallaDeValoracionDeComportamientos("", "", "");
        esperarTiempo.tiempoEsperar();

    }

    @Test
    public void f_testIngresarAPantallaDeNuevaObservacionYdebePermitirmeDesplazarConGestoEnLaPantallaDeValoracionDeComportamientosValoracionTerminada() {
        onView(withText(
                endsWith(mActivityTestRule.getActivity().getString(R.string.nueva_observacion)))).perform(
                click());

        llenarDatos.lLenarPantallaNuevaObservacion();

        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();
        onView(
                withText(endsWith(mActivityTestRule.getActivity().getString(R.string.continuar)))).perform(
                click());
        onView(withId(R.id.lblDescripcion)).perform(click());

        onView(withId(R.id.edtValoracion)).perform(click());
        onView(withText(endsWith("Comportamiento seguro"))).perform(click());
        onView(withId(R.id.edtObservacion)).perform(
                typeText("No se observan comportamientos inseguros"), closeSoftKeyboard());

        onView(withId(R.id.radioNo)).perform(click());

        esperarTiempo.tiempoEsperar();
        onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(
                ViewActions.swipeLeft());
        esperarTiempo.tiempoEsperar();
        onView(ViewMatchers.withId(R.id.scroll_Statatus_Fail)).perform(ViewActions.swipeRight());
        esperarTiempo.tiempoEsperar();
        onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(
                ViewActions.swipeLeft());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_ok))));
        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnStatusAction)).perform(click());
        esperarTiempo.tiempoEsperar();
    }

    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @After

    public void finalized() {
        esperarTiempo.tiempoEsperar();
        cerrarSesion.CerrarSesion();
    }
}
