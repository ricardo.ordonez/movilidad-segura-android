package co.com.arlsura.personas.ui.NuevaObservacionParaTrabajadores;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.pushwoosh.internal.platform.AndroidPlatformModule;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.base.BaseTest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NuevaObservacionEnEstadoDePendienteYRetomadaTrabajadorInterno extends BaseTest {

    @Rule
    public IntentsTestRule<SelectProfile> mActivityTestRule =
            new IntentsTestRule<>(SelectProfile.class);


    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();
    private CerrarSesion cerrarsesion = new CerrarSesion();
    private EsperarTiempo esperarTiempo = new EsperarTiempo();

    @Test
    public void a_testNuevaObservacionSinCompletarParaSiguienteTestDeconfirmarQueSePuedaContinuarConLaObservacionYLosDatosPersistieron() {
        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "800256161", "Cédula de ciudadanía", "1152198279");
        onView(withId(R.id.btnLogin)).perform(click());

        llenarDatos.lLenarPantallaIngresoTrabajadorInterno();

        llenarDatos.llenarPantallaDeNivelesYProcesosTrabajadorInterno();

        onView(withText(R.string.iniciar_valoracion)).perform(click());

        llenarDatos.llenarPantallaReporteTrabajadorInterno("Condiciones de orden y aseo",
                "Comportamiento a intervenir",
                "Se tiene que realizar capacitaciones para mejorar la calidad de los empleados");
        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnSiguiente)).perform(click());
    }

    @Test
    public void b_testNuevaObservacionCompletarObservacionQueSeDejoPendiente() {
        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "800256161", "Cédula de ciudadanía", "1152198279");
        onView(withId(R.id.btnLogin)).perform(click());

        onView(withText(R.string.agree))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withText("Condiciones de orden y aseo")).check((matches(isDisplayed())));
        onView(withText("Comportamiento a intervenir")).check((matches(isDisplayed())));
        onView(withText(
                "Se tiene que realizar capacitaciones para mejorar la calidad de los empleados")).check(
                (matches(isDisplayed())));

        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnSiguiente)).perform(click());
        esperarTiempo.tiempoEsperar();

        llenarDatos.LlenarPantallaDeAccionesATomarTrabajadorInterno("Cerrado", "NO", "");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());
        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));
        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnHacerOtro)).perform(click());
        esperarTiempo.tiempoEsperar();
        cerrarsesion.CerrarSesion();
    }

    private void deleteAppData() {
        try {
            // clearing app data
            String packageName = AndroidPlatformModule.getApplicationContext().getPackageName();
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear co.com.arlsura.personas");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
