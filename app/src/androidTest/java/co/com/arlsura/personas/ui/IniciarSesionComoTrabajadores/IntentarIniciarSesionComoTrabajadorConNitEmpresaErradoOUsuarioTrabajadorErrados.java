package co.com.arlsura.personas.ui.IniciarSesionComoTrabajadores;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.Splash;
import co.com.arlsura.personas.ui.base.BaseTest;
import co.com.arlsura.personas.util.Constants;
import timber.log.Timber;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Objects;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntentarIniciarSesionComoTrabajadorConNitEmpresaErradoOUsuarioTrabajadorErrados {

    @Rule
    public IntentsTestRule<Splash> mActivityTestRule =
            new IntentsTestRule<>(Splash.class);

    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private EsperarTiempo esperarTiempo = new EsperarTiempo();
    private UiDevice device;
    private static DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();
    CerrarSesion cerrarSesion = new CerrarSesion();

    @Before
    public void SetUp() {
        esperarTiempo.tiempoEsperar();
        iniciarsesion.SeleccionarLaOpcionDeObservador();
        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);
        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "JUAN ESTEBAN ESTRADA");
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
    }

    //Estos test son comendatados por que la pantalla de trabajador fue modificada, por tanto ya no se ingresa con clave si no con documentos, a lo cual
    //ya no es necesario estos test, de igual forma dejo comentados por si a futuro es modificada la pantalla nuevamente.
    @Test
    public void a_testIntentarIniciarSesionConNitEmpresaInvalidoYUsuarioValida() {

        onView(withId(R.id.btnWorker)).perform(click());
        onView(swipeUp().getConstraints());

        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "1037606401",
                 "Cédula de ciudadanía", Constants.userFailDefault);

        onView(withId(R.id.btnLogin)).perform(click());

        onView(withText(R.string.usuario_no_autorizado)).check((matches(isDisplayed())));
        onView(withId(R.id.md_buttonDefaultPositive)).perform(click());
    }

    @Test
    public void b_testIntentarIniciarSesionConNitEmpresaYUsuarioInvalido() {

        onView(withId(R.id.btnWorker)).perform(click());

        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "1037606401", "Cédula de ciudadanía",
                Constants.keyFailDefault);

        onView(withId(R.id.btnLogin)).perform(click());
        esperarTiempo.tiempoEsperar();
        onView(withText(R.string.usuario_no_autorizado)).check((matches(isDisplayed())));
        onView(withId(R.id.md_buttonDefaultPositive)).perform(click());
    }

    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatos.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatos.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }
}
