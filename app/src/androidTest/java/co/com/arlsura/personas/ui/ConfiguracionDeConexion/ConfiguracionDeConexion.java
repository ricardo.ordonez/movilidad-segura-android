package co.com.arlsura.personas.ui.ConfiguracionDeConexion;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaObservador.DefinidasPerfilObservador;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.Splash;
import co.com.arlsura.personas.ui.base.BaseTest;
import timber.log.Timber;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Objects;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ConfiguracionDeConexion extends BaseTest {

    @Rule
    public IntentsTestRule<Splash> mActivityTestRule =
            new IntentsTestRule<>(Splash.class);

    private DefinidasPerfilObservador configuracion = new DefinidasPerfilObservador();
    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private  static EsperarTiempo esperarTiempo = new EsperarTiempo();
    private static DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();

    private static CerrarSesion cerrarSesion = new CerrarSesion();
    private UiDevice device;


    @Before
    public void SetUp() {

        esperarTiempo.tiempoEsperar();
        iniciarsesion.SeleccionarLaOpcionDeObservador();





        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);
        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "JUAN ESTEBAN ESTRADA");
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
    }


    @Test
    public void testIngresarAConfiguracionYSeleccionarWifiComoPreferenciaDeConexionAInternet() {
        onView(ViewMatchers.withId(R.id.Select_profile)).perform(ViewActions.swipeUp());
        onView(ViewMatchers.withId(R.id.Select_profile)).perform(ViewActions.swipeUp());
        onView(withId(R.id.btnConfiguracion)).perform(click());
        configuracion.SeleccionarTipoDeConexionDatosOWifi("Solo con Wifi");
        pressBack();
    }

    @Test
    public void testIngresarAConfiguracionYSeleccionarDatosMovilesComoPreferenciaDeConexionAInternet() {
        onView(ViewMatchers.withId(R.id.Select_profile)).perform(ViewActions.swipeUp());
        onView(withId(R.id.btnConfiguracion)).perform(click());
        configuracion.SeleccionarTipoDeConexionDatosOWifi("Usar datos móviles");
        pressBack();
    }

    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatos.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatos.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");
            onView(withId(R.id.btnSiguiente)).perform(click());
            onView(withId(R.id.edtResponsable)).perform(click());
            onView(withText(Responsable)).perform(click());
            onView(withId(R.id.btnSiguiente)).perform(click());
            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));
            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }
}
