package co.com.arlsura.personas.ui.IniciarSesionComoTrabajadores;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.Splash;
import co.com.arlsura.personas.ui.base.BaseTest;
import timber.log.Timber;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.util.Objects;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InicioSesionTrabajadorExterno extends BaseTest {

    @Rule
    public IntentsTestRule<Splash> mActivityTestRule =
            new IntentsTestRule<>(Splash.class);

    InicioDeSesion iniciarsesion = new InicioDeSesion();
    CerrarSesion cerrarSesion = new CerrarSesion();
    private static EsperarTiempo esperarTiempo = new EsperarTiempo();
    private static DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();
    private UiDevice device;

    @Before
    public void SetUp() {
        esperarTiempo.tiempoEsperar();
        iniciarsesion.SeleccionarLaOpcionDeObservador();
        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);
        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "JUAN ESTEBAN ESTRADA");
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
    }


    @Test
    public void a_testIntentarIniciarSesionConNitEmpresaYUsuarioValido() {
       // iniciarsesion.SeleccionarLaOpcionDeObservadorMovilidad();

        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "1037606401", "Tarjeta de identidad", "1037606401");
        onView(withId(R.id.btnLogin)).perform(click());

    }

    @Test
    public void b_testIngresarDatosParaIniciarSesionComoTrabajadorExternoConDocumentodeTarjeIdentidad() {

        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "1037606401", "Tarjeta de identidad",
                "1037606401");
        onView(withId(R.id.btnLogin)).perform(click());
    }

    @Test
    public void c_testIngresarDatosParaIniciarSesionComoTrabajadorExternoConNumeroDeCedula() {
        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "1037606401", "Cédula de ciudadanía", "1037606401");
        onView(withId(R.id.btnLogin)).perform(click());
    }

    @Test
    public void d_testIngresarLoginDeTrabajadoresYNoDebePermitirmePresionarElBotonIngresarSinLLenarCorrectamanteTodosLosCampos() {

        onView(withId(R.id.btnWorker)).perform(click());

        onView(withId(R.id.btnLogin)).perform(click());

        onView(withId(R.id.edtTipoDocumentoEmpresa)).perform(click());

        onView(withText("NIT")).perform(click());

        onView(withId(R.id.btnLogin)).perform(click());

        onView(withId(R.id.edtNumeroDocumentoEmpresa)).perform(click());
        onView(withId(R.id.edtNumeroDocumentoEmpresa)).perform(typeText("1037606401"),
                ViewActions.closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());

        onView(withId(R.id.edtNumeroDocumentoPersona)).perform(click());
        onView(withId(R.id.edtNumeroDocumentoPersona)).perform(typeText("1037606401"),
                ViewActions.closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());

        /*onView(withId(R.id.edtPasswordPersona)).perform(click());
        onView(withId(R.id.edtPasswordPersona)).perform(typeText("clave"),
                ViewActions.closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());
        */
        onView(withText(R.string.ingreso)).check(ViewAssertions.matches(withText(("Ingreso"))));
        //onView(withText("Ingreso")).check((matches(withText("Ingreso"))));
    }

    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatos.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatos.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }


    @After
    public void finalized() {
        cerrarSesion.CerrarSesion();
    }


    private static Matcher<View> childAtPosition(final Matcher<View> parentMatcher,
                                                 final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent) && view.equals(
                        ((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
