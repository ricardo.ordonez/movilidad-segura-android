package co.com.arlsura.personas.ui.NuevaObservacionParaTrabajadores;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaObservador.DefinidasPerfilObservador;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.base.BaseTest;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Objects;

import timber.log.Timber;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NuevoObservacionComotrabajadorInterno extends BaseTest {

    @Rule
    public IntentsTestRule<SelectProfile> mActivityTestRule =
            new IntentsTestRule<>(SelectProfile.class);

    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private DefinicionesPerfilTrabajador llenarDatosT = new DefinicionesPerfilTrabajador();
    private DefinidasPerfilObservador llenarDatos = new DefinidasPerfilObservador();

    private CerrarSesion cerrarSesion = new CerrarSesion();
    private UiDevice device;


    @Before
    public void SetUp() {
        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);
            initSetup();
        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "");
                initSetup();
            } catch (Exception ex) {
                e.printStackTrace();
                initSetup();
            }
        }
    }

    private void initSetup() {
        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "800256161", "Cédula de ciudadanía", "1152198279");
        onView(withId(R.id.btnLogin)).perform(click());
    }

    @Test
    public void a_testIngresarAPantallaYRealizarNuevaObservacionComoTrabajadorInternoYSelecionarEnReporteComportamientoAIntervenir() {
        llenarDatosT.lLenarPantallaIngresoTrabajadorInterno();

        llenarDatosT.llenarPantallaDeNivelesYProcesosTrabajadorInterno();

        onView(withText(R.string.iniciar_valoracion)).perform(click());

        llenarDatosT.llenarPantallaReporteTrabajadorInterno("Condiciones de orden y aseo",
                "Comportamiento a intervenir",
                "Se tiene que realizar capacitaciones para mejorar la calidad de los empleados");

        onView(withId(R.id.btnSiguiente)).perform(click());

        llenarDatosT.LlenarPantallaDeAccionesATomarTrabajadorInterno("",
                "Si, un diálogo con oportunidad de mejora",
                "Estar mas atento de sus empleados y las posturas al estar en su lugar de trabajo");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));

        onView(withId(R.id.btnHacerOtro)).perform(click());
    }

    @Test
    public void b_testIngresarAPantallaYRealizarNuevaObservacionComoTrabajadorInternoYSelecionarEnReporteCondicionInsegura() {
        llenarDatosT.lLenarPantallaIngresoTrabajadorInterno();

        llenarDatosT.llenarPantallaDeNivelesYProcesosTrabajadorInterno();

        onView(withText(R.string.iniciar_valoracion)).perform(click());

        llenarDatosT.llenarPantallaReporteTrabajadorExterno("Condiciones de orden y aseo",
                "Condicion insegura",
                "Se tiene que realizar mantenimiento a las instalacciones para mejorar la calidad del empleado");

        onView(withId(R.id.btnSiguiente)).perform(click());

        llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("Abierto",
                "Si, un diálogo con oportunidad de mejora",
                "Estar mas atento en las instalaciones para evitar futuros problemas en su lugar de trabajo");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));

        onView(withId(R.id.btnHacerOtro)).perform(click());
    }

    @Test
    public void c_testIngresarAPantallaYRealizarNuevaObservacionComoTrabajadorInternoYSelecionarEnReporteFelicitaciones() {
        llenarDatosT.lLenarPantallaIngresoTrabajadorInterno();

        llenarDatosT.llenarPantallaDeNivelesYProcesosTrabajadorInterno();

        onView(withText(R.string.iniciar_valoracion)).perform(click());

        llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                "Felicitaciones",
                "Se encontro buen manejo de todo el sistema de desplazamientos para los trabajadores");

        onView(withId(R.id.btnSiguiente)).perform(click());

        llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                "Si, di las felicitaciones por un buen trabajo", "");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));

        onView(withId(R.id.btnHacerOtro)).perform(click());
    }

    @Test
    public void d_testIngresarAPantallaYRealizarNuevaObservacionComoTrabajadorInternoYSelecionarEnReporteConIncidente() {
        llenarDatosT.lLenarPantallaIngresoTrabajadorInterno();

        llenarDatosT.llenarPantallaDeNivelesYProcesosTrabajadorInterno();

        onView(withText(R.string.iniciar_valoracion)).perform(click());

        llenarDatosT.llenarPantallaReporteTrabajadorExterno("Elementos de proteccion personal",
                "Incidente", "Se encontro mas de una persona mal sentada.");

        onView(withId(R.id.btnSiguiente)).perform(click());

        llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("Cerrado", "NO", "");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));

        onView(withId(R.id.btnHacerOtro)).perform(click());
    }

    @Test
    public void e_testIngresarAPantallaYRealizarNuevaObservacionComoTrabajadorInternoYSinTerminarDeLlenarReporteNoMeDebePermitirRegresar() {
        llenarDatosT.lLenarPantallaIngresoTrabajadorInterno();

        llenarDatosT.llenarPantallaDeNivelesYProcesosTrabajadorInterno();

        onView(withText(R.string.iniciar_valoracion)).perform(click());

        llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                "Felicitaciones", "XXXXXXXXXXXX");

        onView(withId(R.id.toolbar)).perform(pressBack());

        onView(withText("Tienes observaciones pendientes de finalizar")).check(
                (matches(isDisplayed())));

        onView(withText(R.string.agree)).perform(click());

        llenarDatosT.llenarPantallaReporteTrabajadorExterno("Juego y bromas en el area de trabajo",
                "Felicitaciones", "XXXXXXXXXXXX");

        onView(withId(R.id.btnSiguiente)).perform(click());

        llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                "Si, di las felicitaciones por un buen trabajo", "");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));

        onView(withId(R.id.btnHacerOtro)).perform(click());
    }

    @After
    public void finalized() {
        cerrarSesion.CerrarSesion();
    }


    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }
}
