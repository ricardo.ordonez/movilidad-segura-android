package co.com.arlsura.personas.ui.NuevaObservacionParaTrabajadores;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import co.com.arlsura.personas.BuildConfig;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.Splash;
import co.com.arlsura.personas.ui.base.BaseTest;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.IOException;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NuevaObservacionEnEstadoDePendienteYRetomadaTrabajadorExterno extends BaseTest {

    @Rule
    public IntentsTestRule<Splash> mActivityTestRule =
            new IntentsTestRule<>(Splash.class);

    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();
    private CerrarSesion cerrarSesion = new CerrarSesion();
    private EsperarTiempo esperarTiempo = new EsperarTiempo();

  /*@Before @Override public void SetUp()
  {
    //removeDatabase();
    super.SetUp();
  }*/

  @Before
  public void SetUp() {
      esperarTiempo.tiempoEsperar();
      iniciarsesion.SeleccionarLaOpcionDeObservador();

  }

    private void removeDatabase() {
        RealmConfiguration configuration = Realm.getDefaultConfiguration();
        if (configuration != null) {
            Timber.e("deleteDatabase");
            Realm.deleteRealm(configuration);
        }
    }

    @Test
    public void a_testNuevaObservacionSinCompletarParaSiguienteTestDeconfirmarQueSePuedaContinuarConLaObservacionYLosDatosPersistieron() {
        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "800256161", "Cédula de ciudadanía", "1037606402");
        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnLogin)).perform(click());

        llenarDatos.lLenarPantallaIngresoTrabajadorExterno();
        esperarTiempo.tiempoEsperar();
        onView(withText(R.string.iniciar_valoracion)).perform(click());

        esperarTiempo.tiempoEsperar();

        llenarDatos.llenarPantallaReporteTrabajadorExterno("Condiciones de orden y aseo",
                "Comportamiento a intervenir",
                "Se tiene que realizar capacitaciones para mejorar la calidad de los empleados");
        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnSiguiente)).perform(click());

    }

    @Test
    public void b_testNuevaObservacionCompletarObservacionQueSeDejoPendiente() {

        onView(withId(R.id.btnWorker)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveTrabajador("NIT", "800256161", "Cédula de ciudadanía", "1037606402");
        onView(withId(R.id.btnLogin)).perform(click());

        esperarTiempo.tiempoEsperar();

        onView(withText(R.string.agree))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()))
                .perform(click());


        onView(withText("Condiciones de orden y aseo")).check((matches(isDisplayed())));
        onView(withText("Comportamiento a intervenir")).check((matches(isDisplayed())));
        onView(withText("Se tiene que realizar capacitaciones para mejorar la calidad de los empleados")).check(
                (matches(isDisplayed())));
        esperarTiempo.tiempoEsperar();

        onView(withId(R.id.btnSiguiente)).perform(click());
        esperarTiempo.tiempoEsperar();

        llenarDatos.LlenarPantallaDeAccionesATomarTrabajadorInterno("Cerrado", "NO", "");

        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.edtResponsable)).perform(click());

        onView(withText("JUAN ESTEBAN ESTRADA")).perform(click());
        onView(withId(R.id.btnSiguiente)).perform(click());

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.reporte_enviado))));
        esperarTiempo.tiempoEsperar();
        onView(withId(R.id.btnHacerOtro)).perform(click());
        esperarTiempo.tiempoEsperar();


        finalized();
    }

    public void finalized() {
       cerrarSesion.CerrarSesion();
        try {
            Process process = Runtime.getRuntime().exec("adb uninstall co.com.arlsura.personas");
            process.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (io.realm.internal.IOException e) {
            e.printStackTrace();
        }
    }
}
