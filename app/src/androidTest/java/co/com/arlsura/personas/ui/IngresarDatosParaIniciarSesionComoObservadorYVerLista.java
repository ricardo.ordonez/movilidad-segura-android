package co.com.arlsura.personas.ui;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaObservador.DefinidasPerfilObservador;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.base.BaseTest;
import timber.log.Timber;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Objects;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IngresarDatosParaIniciarSesionComoObservadorYVerLista extends BaseTest {

    @Rule
    public IntentsTestRule<SelectProfile> mActivityTestRule =
            new IntentsTestRule<>(SelectProfile.class);

    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private CerrarSesion cerrarSesion = new CerrarSesion();
    private EsperarTiempo EsperarTiempo = new EsperarTiempo();
    private DefinicionesPerfilTrabajador llenarDatosT = new DefinicionesPerfilTrabajador();
    private DefinidasPerfilObservador llenarDatos = new DefinidasPerfilObservador();
    private int cont = 0;
    private UiDevice device;

    @Before
    public void SetUp() {
        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);
            initSetup();
        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "");
                initSetup();
            } catch (Exception ex) {
                e.printStackTrace();
                initSetup();
            }
        }
    }

    private void initSetup() {
        onView(withId(R.id.btnObserver)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveObservador("NIT", "1037606401", "1037606401", "clave123");
        onView(withId(R.id.btnLogin)).perform(click());
    }


    @Test
    public void a_IngresarDatosParaIniciarSesionComoObservadorYVerLista() {

        EsperarTiempo.tiempoEsperar();


        while (cont < 3) {
            onView(ViewMatchers.withId(R.id.listHistorial)).perform(ViewActions.swipeUp());
            cont++;
        }
        EsperarTiempo.tiempoEsperar();
        onView(withText("PRUEBAS FUNCIONAMIENTO 5")).check((matches(isDisplayed())));
        onView(withText("Área: BOGOTA")).check((matches(isDisplayed())));
        onView(withText("Estándar: VERIFICACION DE COMPONENTES 5")).check((matches(isDisplayed())));
        onView(withText("Actividad: VALIDACION DE PROCESOS 5")).check((matches(isDisplayed())));
        onView(withText("Número de personas: 5")).check((matches(isDisplayed())));
    }


    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
            cerrarSesion.CerrarSesion();
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @After
    public void finalized() {
        cerrarSesion.CerrarSesion();
    }
}
