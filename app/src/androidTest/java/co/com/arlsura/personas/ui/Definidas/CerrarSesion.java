package co.com.arlsura.personas.ui.Definidas;

import co.com.arlsura.personas.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class CerrarSesion {

  private static EsperarTiempo esperarTiempo = new EsperarTiempo();

  public void CerrarSesion() {

    onView(withId(R.id.action_salir)).check(matches(isDisplayed()));
    onView(withId(R.id.action_salir)).perform(click());
    esperarTiempo.tiempoEsperar();
    onView(withText(R.string.agree)).check(matches(isDisplayed()));
    onView(withText(R.string.agree)).perform(click());
  }
}
