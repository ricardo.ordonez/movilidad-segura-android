package co.com.arlsura.personas.ui.NuevaObservacionParaObservadores;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import co.com.arlsura.personas.App;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.Definidas.InicioDeSesion;
import co.com.arlsura.personas.ui.DefinidasParaObservador.DefinidasPerfilObservador;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.Splash;
import co.com.arlsura.personas.ui.base.BaseTest;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.util.Objects;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NuevaObservacionEnEstadoDePendienteYRetomada extends BaseTest {

    @Rule
    public IntentsTestRule<Splash> mActivityTestRule =
            new IntentsTestRule<>(Splash.class);

    private InicioDeSesion iniciarsesion = new InicioDeSesion();
    private DefinidasPerfilObservador llenarDatos = new DefinidasPerfilObservador();
    private CerrarSesion cerrarSesion = new CerrarSesion();
    private static DefinicionesPerfilTrabajador llenarDatosT = new DefinicionesPerfilTrabajador();
    private UiDevice device;
    private EsperarTiempo esperarTiempo = new EsperarTiempo();

    @Before
    public void SetUp() {
        esperarTiempo.tiempoEsperar();
        iniciarsesion.SeleccionarLaOpcionDeObservador();
        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);

        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "");

            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void a_testNuevaObservacionSinCompletarParaSiguienteTestDeconfirmarQueSePuedaContinuarConLaObservacionYLosDatosPersistieron() {
        onView(withId(R.id.btnObserver)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveObservador("NIT", "1037606401", "1037606402", "clave");
        onView(withId(R.id.btnLogin)).perform(click());


        onView(withId(R.id.btnNuevaObservacion)).check((matches(withText(R.string.nueva_observacion))));
        onView(withId(R.id.btnNuevaObservacion)).perform(click());

        llenarDatos.lLenarPantallaNuevaObservacion();

        onView(withId(R.id.btnContinuar)).perform(click());
        esperarTiempo.tiempoEsperar();
        llenarDatos.llenarPantallaDeNivelesYProcesos();
        onView(withId(R.id.btnContinuar)).perform(click());
        onView(withId(R.id.lblDescripcion)).perform(click());

        onView(withId(R.id.edtValoracion)).perform(click());
        onView(withText("Comportamiento seguro")).perform(click());
        onView(withId(R.id.edtObservacion)).perform(typeText("No observan comportamientos inseguros"),
                closeSoftKeyboard());

        onView(withId(R.id.radioNo)).perform(click());

    }

    @Test
    public void b_testNuevaObservacionCompletarObservacionQueSeDejoPendiente() {
        onView(withId(R.id.btnObserver)).perform(click());
        iniciarsesion.IniciarSesionConUsuarioYClaveObservador("NIT", "1037606401", "1037606402", "clave");
        onView(withId(R.id.btnLogin)).perform(click());


            onView(withText(R.string.agree)).perform(click());


            onView(withId(R.id.lblDescripcion)).perform(click());


             onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText("Comportamiento seguro")).perform(click());

            onView(withId(R.id.edtObservacion)).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText("No observan comportamientos inseguros"),
             closeSoftKeyboard());

            onView(withId(R.id.radioNo)).perform(click());
            esperarTiempo.tiempoEsperar();
            onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(
                ViewActions.swipeLeft());
            esperarTiempo.tiempoEsperar();

            onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_ok))));
            esperarTiempo.tiempoEsperar();
            onView(withId(R.id.btnStatusAction)).perform(click());

            esperarTiempo.tiempoEsperar();
            onView(withId(R.id.action_salir)).perform(click());
            onView(withId(R.id.md_buttonDefaultPositive)).perform(click());

    }

    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    private void removeDatabase() {
        RealmConfiguration configuration = Realm.getDefaultConfiguration();
        if (configuration != null) {
            Timber.e("deleteDatabase");
            Realm.deleteRealm(configuration);
        }
    }
}

