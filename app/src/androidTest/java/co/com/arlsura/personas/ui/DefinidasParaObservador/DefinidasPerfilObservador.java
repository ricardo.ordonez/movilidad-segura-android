package co.com.arlsura.personas.ui.DefinidasParaObservador;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;

import org.junit.Rule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class DefinidasPerfilObservador {
    private EsperarTiempo esperarTiempo = new EsperarTiempo();

    public void lLenarPantallaNuevaObservacion() {

        TiempoEsperar();
        onView(withId(R.id.edtDate)).perform(click());
        TiempoEsperar();
        onView(withText(R.string.agree)).perform(click());
        TiempoEsperar();

        onView(withId(R.id.edtHour)).perform(click());
        onView(withText(R.string.agree)).perform(click());
        onView(withId(R.id.edtNumeroPersonas)).perform(typeText("2"), closeSoftKeyboard());
    }

    public void llenarPantallaDeNivelesYProcesos() {
        onView(withId(R.id.edtNivel1)).perform(click());
        onView(withText("AREAMOVIL")).perform(click());

        onView(withId(R.id.edtNivel2)).perform(click());
        onView(withText("POPAYAN")).perform(click());


        onView(withId(R.id.edtNivel3)).perform(click());
        onView(withText("PROCESOS POPAYAN")).perform(click());

        onView(withId(R.id.edtNivel4)).perform(click());
        onView(withText("EJEMPLO NIVEL 4")).perform(click());


        onView(withId(R.id.edtNivel5)).perform(click());
        onView(withText("EJEMPLO NIVEL 5")).perform(click());

        onView(withId(R.id.edtNivel6)).perform(click());
        onView(withText("PROCESO NIVEL 5")).perform(click());


        onView(ViewMatchers.withId(R.id.scrollViewDateBasic)).perform(ViewActions.swipeUp());
        onView(withId(R.id.edtNivel8)).perform(click());
        onView(withText("AYUDANTÍA")).perform(click());
    }


    public void llenarPantallaDeValoracionDeComportamientos(String Valoracion, String Mensaje, String SeleccionRadioButton) {
        if (SeleccionRadioButton == "si") {
            onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText(Valoracion)).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText(Mensaje), closeSoftKeyboard());

            onView(withId(R.id.radioSi)).perform(click());

            onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(ViewActions.swipeUp());
            onView(withId(R.id.edtSituacion)).perform(click());
            onView(withId(R.id.edtSituacion)).perform(typeText(Mensaje), closeSoftKeyboard());

            onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(ViewActions.swipeUp());
            onView(withId(R.id.edtMejora)).perform(click());
            onView(withId(R.id.edtMejora)).perform(typeText(Mensaje), closeSoftKeyboard());

            onView(withId(R.id.imgNext)).perform(click());
            onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_ok))));
            onView(withId(R.id.btnStatusAction)).perform(click());

        } else if (SeleccionRadioButton == "no") {
            onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText(Valoracion)).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText(Mensaje), closeSoftKeyboard());

            onView(withId(R.id.radioNo)).perform(click());
            onView(withId(R.id.imgNext)).perform(click());

            onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_ok))));
            onView(withId(R.id.btnStatusAction)).perform(click());
            esperarTiempo.tiempoEsperar();


        } else if (Valoracion == "" && Mensaje == "" && SeleccionRadioButton == "") {
            onView(withId(R.id.imgNext)).perform(click());
            onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_error))));

            onView(withId(R.id.btnStatusAction)).perform(click());
            //onView(withId(R.id.lblDescripcion)).perform(click());

            //para finalizar y cerrar sesion para evitar que otras pruebas fallen por que en caso tal quedaria guardado
            //que hay observaciones pendientes.
            onView(withId(R.id.edtValoracion)).perform(click());
            onView(withText("No aplica")).perform(click());
            onView(withId(R.id.edtObservacion)).perform(typeText("Terminando..."), closeSoftKeyboard());
            onView(withId(R.id.radioNo)).perform(click());
            onView(withId(R.id.imgNext)).perform(click());
            onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_ok))));
            esperarTiempo.tiempoEsperar();
            onView(withId(R.id.btnStatusAction)).perform(click());
            esperarTiempo.tiempoEsperar();
        }
    }


    public void SeleccionarTipoDeConexionDatosOWifi(String TipoDeConexion) {
        if (TipoDeConexion == "Solo con Wifi") {
            onView(withText("Solo con Wifi")).perform(click());

        }
        if (TipoDeConexion == "Usar datos móviles") {
            onView(withText("Usar datos móviles")).perform(click());
        }
    }

    public void ObservacionesPendientesDeFinalizarNuevas(String prueba){
    if (prueba == "ARL PERSONAS"){
        /*onView(withId(R.id.lblDescripcion)).perform(click());

        onView(withText("Comportamiento seguro")).check((matches(isDisplayed())));
        onView(withText("No observan comportamientos inseguros")).check((matches(isDisplayed())));

        TiempoEsperar();
        onView(ViewMatchers.withId(R.id.scrollViewValoracionComportamientos)).perform(
            ViewActions.swipeLeft());
        TiempoEsperar();

        onView(withId(R.id.txtStatus)).check(matches(withText((R.string.state_ok))));
        TiempoEsperar();
        onView(withId(R.id.btnStatusAction)).perform(click());
        TiempoEsperar();
        onView(withId(R.id.action_salir)).perform(click());
        onView(withId(R.id.md_buttonDefaultPositive)).perform(click());*/
    }

    }


    public void TiempoEsperar() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
