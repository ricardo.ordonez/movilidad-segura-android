package co.com.arlsura.personas.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Objects;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.Definidas.CerrarSesion;
import co.com.arlsura.personas.ui.Definidas.EsperarTiempo;
import co.com.arlsura.personas.ui.DefinidasParaTrabajador.DefinicionesPerfilTrabajador;
import timber.log.Timber;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VerificarPantallaDeInicioDeObservadoresDeComportamientos {
    private static DefinicionesPerfilTrabajador llenarDatos = new DefinicionesPerfilTrabajador();
    private static CerrarSesion cerrarSesion = new CerrarSesion();
    private DefinicionesPerfilTrabajador llenarDatosT = new DefinicionesPerfilTrabajador();
    @Rule
    public IntentsTestRule<SelectProfile> mActivityTestRule =
            new IntentsTestRule<>(SelectProfile.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule =
            GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE);

    private EsperarTiempo tiempoEsperar = new EsperarTiempo();
    private UiDevice device;

    @Before
    public void SetUp() {
        try {
            verificarPermisos();
            checkMensajeAlerta(mActivityTestRule.getActivity(), true);

        } catch (Exception e) {
            try {
                checkMensajeAlerta(mActivityTestRule.getActivity(), true, "");

            } catch (Exception ex) {
                e.printStackTrace();

            }
        }
    }


    @Test
    public void a_testVerificarPantallaDeInicioDeObservadoresDeComportamientos() {
        onView(withText(R.string.observador_comportamientos)).check(matches(isDisplayed()));
        onView(withText(R.string.slecciona_perfil)).check(matches(isDisplayed()));
        onView(withText(R.string.login_worker)).check(matches(isDisplayed()));
        onView(withText(R.string.login_observer)).check(matches(isDisplayed()));
    }

    @Test
    public void b_testIngresarAlLoginDeObservadoresYRegresarMedianteElBotonDeAtrasAlInicioDeLaAplicacion() {
        onView(withId(R.id.btnObserver)).perform(click());
        onView(withText(R.string.ingreso_observadores)).check(matches(isDisplayed()));
        onView(withId(R.id.toolbar)).perform(pressBack());
        tiempoEsperar.tiempoEsperar();
    }

    @Test
    public void c_testIngresarAlLoginDeTrabajadoresYRegresarMedianteElBotonDeAtrasAlInicioDeLaAplicacion() {
        onView(withId(R.id.btnWorker)).perform(click());
        onView(withText(R.string.ingreso_trabajadores)).check(matches(isDisplayed()));
        onView(withId(R.id.toolbar)).perform(pressBack());
        tiempoEsperar.tiempoEsperar();
    }


    protected void checkMensajeAlerta(Activity context, boolean allow)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.allow_permision));
        }
    }

    protected void checkMensajeAlerta(Activity context, boolean allow, String Responsable)
            throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        if (allow) {
            Mensaje(context.getString(R.string.agree), Responsable);
        }
    }

    private void Mensaje(String agreeText, String Responsable) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();

        if (!Objects.equals(Responsable, "")) {
            llenarDatosT.llenarPantallaReporteTrabajadorExterno("Desplazamiento de las personas",
                    "Felicitaciones", "XXXXXXXXX");

            onView(withId(R.id.btnSiguiente)).perform(click());

            llenarDatosT.llenarPantallaDeAccionesATomarTrabajadorExterno("",
                    "Si, di las felicitaciones por un buen trabajo", "");

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.edtResponsable)).perform(click());

            onView(withText(Responsable)).perform(click());

            onView(withId(R.id.btnSiguiente)).perform(click());

            onView(withId(R.id.txtStatus)).check(ViewAssertions.matches(withText((R.string.reporte_enviado))));

            onView(withId(R.id.btnHacerOtro)).perform(click());
            cerrarSesion.CerrarSesion();
        } else {
            onView(withId(R.id.lblDescripcion)).perform(click());
            llenarDatos.llenarPantallaDeValoracionDeComportamientos("No aplica", "XXXXX", "no");
            cerrarSesion.CerrarSesion();
        }
    }

    private void Mensaje(String agreeText) throws UiObjectNotFoundException {
        Timber.e("MensajeTrabajadores");
        UiObject allowButton = device.findObject(new UiSelector().text(agreeText));
        allowButton.click();
    }


    public void verificarPermisos() {
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }


    private static Matcher<View> childAtPosition(final Matcher<View> parentMatcher,
                                                 final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent) && view.equals(
                        ((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
