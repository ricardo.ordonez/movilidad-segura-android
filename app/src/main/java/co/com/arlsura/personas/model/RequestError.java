package co.com.arlsura.personas.model;

/**
 * Created by home on 2/8/18.
 */

public class RequestError {

  /**
   * status : INTERNAL_SERVER_ERROR
   * timestamp : {"nano":973000000,"hour":12,"minute":13,"second":21,"dayOfYear":39,"dayOfWeek":"THURSDAY","month":"FEBRUARY","dayOfMonth":8,"year":2018,"monthValue":2,"chronology":{"id":"ISO","calendarType":"iso8601"}}
   * message : Ha ocurrido un error!
   * debugMessage : Usuario no encontrado.---|--- Usuario no encontrado.
   */

  private String status;
  private Timestamp timestamp;
  private String message;
  private String debugMessage;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Timestamp getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Timestamp timestamp) {
    this.timestamp = timestamp;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getDebugMessage() {
    return (debugMessage == null && status.equals("401")) ? "En estos momentos no tienes acceso a este modulo, por favor contactate con el area encargada." : debugMessage;
  }

  public void setDebugMessage(String debugMessage) {
    this.debugMessage = debugMessage;
  }

  public static class Timestamp {
    /**
     * nano : 973000000
     * hour : 12
     * minute : 13
     * second : 21
     * dayOfYear : 39
     * dayOfWeek : THURSDAY
     * month : FEBRUARY
     * dayOfMonth : 8
     * year : 2018
     * monthValue : 2
     * chronology : {"id":"ISO","calendarType":"iso8601"}
     */

    private int nano;
    private int hour;
    private int minute;
    private int second;
    private int dayOfYear;
    private String dayOfWeek;
    private String month;
    private int dayOfMonth;
    private int year;
    private int monthValue;
    private Chronology chronology;

    public int getNano() {
      return nano;
    }

    public void setNano(int nano) {
      this.nano = nano;
    }

    public int getHour() {
      return hour;
    }

    public void setHour(int hour) {
      this.hour = hour;
    }

    public int getMinute() {
      return minute;
    }

    public void setMinute(int minute) {
      this.minute = minute;
    }

    public int getSecond() {
      return second;
    }

    public void setSecond(int second) {
      this.second = second;
    }

    public int getDayOfYear() {
      return dayOfYear;
    }

    public void setDayOfYear(int dayOfYear) {
      this.dayOfYear = dayOfYear;
    }

    public String getDayOfWeek() {
      return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
      this.dayOfWeek = dayOfWeek;
    }

    public String getMonth() {
      return month;
    }

    public void setMonth(String month) {
      this.month = month;
    }

    public int getDayOfMonth() {
      return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
      this.dayOfMonth = dayOfMonth;
    }

    public int getYear() {
      return year;
    }

    public void setYear(int year) {
      this.year = year;
    }

    public int getMonthValue() {
      return monthValue;
    }

    public void setMonthValue(int monthValue) {
      this.monthValue = monthValue;
    }

    public Chronology getChronology() {
      return chronology;
    }

    public void setChronology(Chronology chronology) {
      this.chronology = chronology;
    }

    public static class Chronology {
      /**
       * id : ISO
       * calendarType : iso8601
       */

      private String id;
      private String calendarType;

      public String getId() {
        return id;
      }

      public void setId(String id) {
        this.id = id;
      }

      public String getCalendarType() {
        return calendarType;
      }

      public void setCalendarType(String calendarType) {
        this.calendarType = calendarType;
      }
    }
  }
}
