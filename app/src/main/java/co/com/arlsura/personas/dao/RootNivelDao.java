package co.com.arlsura.personas.dao;

import co.com.arlsura.personas.base.BaseDaoContract;
import co.com.arlsura.personas.model.niveles.RootNivel1;
import io.realm.Realm;

/**
 * Created by home on 11/8/17.
 */

public class RootNivelDao implements BaseDaoContract<RootNivel1> {
  @Override public void save(RootNivel1 item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void update(RootNivel1 item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void delete(RootNivel1 item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item.deleteFromRealm();
    realm.commitTransaction();
  }

  @Override public RootNivel1 findFirst() {
    RootNivel1 item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item = realm.where(RootNivel1.class).findFirst();
    realm.commitTransaction();
    return item;
  }

  @Override public RootNivel1 createFromJson(String json) {
    return null;
  }
}
