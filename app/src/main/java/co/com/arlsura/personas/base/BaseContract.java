package co.com.arlsura.personas.base;

/**
 * Created by home on 11/7/17.
 */

public interface BaseContract {
  void setLoading();
  void hideLoading();
  void onErr(String err);
  void onErr(int err);

}
