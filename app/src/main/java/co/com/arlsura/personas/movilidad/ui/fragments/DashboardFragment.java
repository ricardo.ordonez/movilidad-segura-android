package co.com.arlsura.personas.movilidad.ui.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.arlsura.personas.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class DashboardFragment extends Fragment implements View.OnClickListener{

    @BindView(R.id.btnAddUserVehicle) Button btnAddUserVehicle;
    @BindView(R.id.btnVehicleDetails) Button btnVehicleDetails;
    @BindView(R.id.btnUserVehicleList) Button btnUserVehicleList;
    @BindView(R.id.btnCompanyVehicleList) Button btnCompanyVehicleList;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setNavigationClickListeners();
    }

    private void setNavigationClickListeners(){
        btnAddUserVehicle.setOnClickListener(this);
        btnVehicleDetails.setOnClickListener(this);
        btnCompanyVehicleList.setOnClickListener(this);
        btnUserVehicleList.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnUserVehicleList:
                Navigation.findNavController(view).navigate(R.id.toUserVehicleListFragment);
            break;
            case R.id.btnCompanyVehicleList:
                Navigation.findNavController(view).navigate(R.id.toCompanyVehicleListFragment);
            break;
            case R.id.btnAddUserVehicle:
                Navigation.findNavController(view).navigate(R.id.toAddVehicleForms);
            break;
            case R.id.btnVehicleDetails:
                Navigation.findNavController(view).navigate(R.id.toVehicleDetailsFragment);
            break;
        }
    }
}
