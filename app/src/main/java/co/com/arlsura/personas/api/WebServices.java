package co.com.arlsura.personas.api;


import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.realm.RealmObject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by home on 11/7/17.
 */

public class WebServices {

  private static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
   interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return interceptor;
  }

  private static OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
    List<Protocol> protocols = new ArrayList<>();
    protocols.add(Protocol.HTTP_1_1);
    OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
    okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
    okHttpClient.readTimeout(1, TimeUnit.MINUTES);
    okHttpClient.protocols(protocols);
    okHttpClient.addInterceptor(httpLoggingInterceptor);

    return okHttpClient.build();
  }

  public static Gson getGson() {
    return new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
      @Override public boolean shouldSkipField(FieldAttributes f) {
        return f.getDeclaringClass().equals(RealmObject.class);
      }

      @Override public boolean shouldSkipClass(Class<?> clazz) {
        return false;
      }
    })
        //.registerTypeAdapter(Class.forName("io.realm.PersonRealmProxy"), new PersonSerializer())
        //.registerTypeAdapter(Class.forName("io.realm.DogRealmProxy"), new DogSerializer())
        .create();
  }

  private static ApiService provideApiService(OkHttpClient okHttpClient) {
    return new Retrofit.Builder().baseUrl("https://personas.arlsura.com/")
        .addConverterFactory(GsonConverterFactory.create(getGson()))
        .addCallAdapterFactory(new ErrorHandlingExecutorCallAdapterFactory(
            new ErrorHandlingExecutorCallAdapterFactory.MainThreadExecutor()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build()
        .create(ApiService.class);
  }

  public static ApiService getApi() {
    return provideApiService(provideOkHttpClient(provideHttpLoggingInterceptor()));
  }

  private static SuraLoginService provideSuraLoginService(OkHttpClient okHttpClient){
    return new Retrofit.Builder().baseUrl("https://arlappslab.suramericana.com")
            .addConverterFactory(GsonConverterFactory.create(getGson()))
            .addCallAdapterFactory(new ErrorHandlingExecutorCallAdapterFactory(
                    new ErrorHandlingExecutorCallAdapterFactory.MainThreadExecutor()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(SuraLoginService.class);
  }

  public static SuraLoginService getLoginApi() {
    return provideSuraLoginService(provideOkHttpClient(provideHttpLoggingInterceptor()));
  }
}
