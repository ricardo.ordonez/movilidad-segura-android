package co.com.arlsura.personas.dao;

import co.com.arlsura.personas.base.BaseDaoContract;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.StatusSync;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by home on 11/8/17.
 */

public class ProcesoDao implements BaseDaoContract<Proceso> {
  @Override public void save(Proceso item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void update(Proceso item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void delete(Proceso item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item.deleteFromRealm();
    realm.commitTransaction();
  }

  @Override public Proceso findFirst() {
    Proceso item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item = realm.where(Proceso.class).findFirst();
    realm.commitTransaction();
    return item;
  }

  public Proceso findFirstPending() {
    Proceso item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item = realm.where(Proceso.class).equalTo("status", StatusSync.Pending.toString()).findFirst();
    realm.commitTransaction();
    return item;
  }

  public Proceso findById(int id) {
    Proceso item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item = realm.where(Proceso.class).equalTo("id", id).findFirst();
    realm.commitTransaction();
    return item;
  }

  public Proceso findFirstPendingSync() {
    Proceso item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item =
        realm.where(Proceso.class).equalTo("status", StatusSync.PendingSync.toString()).findFirst();
    realm.commitTransaction();
    return item;
  }

  public List<Proceso> findPendingSync() {
    List<Proceso> list = null;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    RealmResults<Proceso> results =
        realm.where(Proceso.class).equalTo("status", StatusSync.PendingSync.toString()).findAll();
    if (!results.isEmpty()) {
      list = new ArrayList<>();
      list.addAll(results);
    }
    realm.commitTransaction();
    return list;
  }

  @Override public Proceso createFromJson(String json) {
    return null;
  }
}
