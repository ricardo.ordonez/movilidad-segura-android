package co.com.arlsura.personas.view;

/**
 * Created by home on 11/9/17.
 */

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import co.com.arlsura.personas.R;
import timber.log.Timber;

/**
 * @author piotr on 31.07.16.
 */

public class CustomSpinner extends androidx.appcompat.widget.AppCompatSpinner implements View.OnTouchListener,
        AdapterView.OnItemSelectedListener
{
  private static final String TAG = "CustomSpinner";
  private OnSpinnerEventsListener mListener;
  private boolean mOpenInitiated = false;
  private CustomSpinnerAdapter spinnerArrayAdapter;
  private OnItemSelectedListener listener;

  private String promptMessage;
  public boolean userTouch;
  private String nuevo;
  private String viejo;
  private List<String> list;

  public interface OnItemSelectedListener{
    void onItemSelected(AdapterView<?> adapterView, View view, int i, long l);
    void onNothingSelected(AdapterView<?> adapterView);
    void onAfterItemSelected(AdapterView<?> adapterView, View view, int i, long l);
  }




  @Override
  public boolean onTouch(View v, MotionEvent event) {
    userTouch = true;

    if(spinnerArrayAdapter != null)
      spinnerArrayAdapter.setOnViewSelected(true);

    return false;
  }

  @Override
  public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    if(listener != null && userTouch){


      final Handler handler = new Handler();
      handler.postDelayed(new Runnable() {


        @Override
        public void run() {
          //Do something after 100ms
          listener.onItemSelected(adapterView,view,i,l);
          listener.onAfterItemSelected(adapterView,view,i,l);
            userTouch = false;

        }
      }, 100);

    }


  }

  public void setOnItemSelectedListener (OnItemSelectedListener listener){
    this.listener = listener;
  }



  @Override
  public void onNothingSelected(AdapterView<?> adapterView) {

  }

  private void commonListeners(){

      setOnTouchListener(this);
      setOnItemSelectedListener(this);
  }

  public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
    super(context, attrs, defStyleAttr, mode);
      commonListeners();
  }

  public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
      commonListeners();
  }

  public CustomSpinner(Context context, AttributeSet attrs) {
    super(context, attrs);
      commonListeners();
  }

  public CustomSpinner(Context context, int mode) {
    super(context, mode);
      commonListeners();
  }

  public CustomSpinner(Context context) {
    super(context);
      commonListeners();
  }



  public interface OnSpinnerEventsListener {

    void onSpinnerOpened();

    void onSpinnerClosed();
  }

  @Override public boolean performClick() {
    // register that the Spinner was opened so we have a status
    // indicator for the activity(which may lose focus for some other
    // reasons)
    mOpenInitiated = true;
    if (mListener != null) {
      mListener.onSpinnerOpened();
    }
    return super.performClick();
  }

  private void removePromptMessage(){
    spinnerArrayAdapter.remove(promptMessage);
    spinnerArrayAdapter.notifyDataSetChanged();
  }

  private void addPromptMessage(){
    spinnerArrayAdapter.insert(promptMessage,0);
    spinnerArrayAdapter.notifyDataSetChanged();
  }


  /**
   * Propagate the closed Spinner event to the listener from outside.
   */
  public void performClosedEvent() {
    mOpenInitiated = false;
    if (mListener != null) {
      mListener.onSpinnerClosed();
      addPromptMessage();
    }
  }

  /**
   * A boolean flag indicating that the Spinner triggered an open event.
   *
   * @return true for opened Spinner
   */
  public boolean hasBeenOpened() {
    return mOpenInitiated;
  }

  @Override public void onWindowFocusChanged(boolean hasWindowFocus) {
    Timber.d( "onWindowFocusChanged");
    super.onWindowFocusChanged(hasWindowFocus);
    if (hasBeenOpened() && hasWindowFocus) {
      Timber.d( "closing popup");
      performClosedEvent();
    }
  }


  public void initializeStringValues(List<String> values, String promptText) {

    if(values.isEmpty())
      return;

    if(!values.get(0).equals(promptText))
      values.add(0,promptText);


    spinnerArrayAdapter =
        new CustomSpinnerAdapter(this.getContext(), R.layout.layout_custom_spinner_item, values);


    spinnerArrayAdapter.addAll(values);
  //  this.setOnItemSelectedListener(this);

    setAdapter(spinnerArrayAdapter);
  }

  public @Nullable Object getItem(int position) {
    if (spinnerArrayAdapter != null && position < spinnerArrayAdapter.getCount() + 1) {
      return spinnerArrayAdapter.getItem(position);
    } else {
      return null;
    }
  }


  @Override
  public void setSelection(int position, boolean animate) {
    boolean sameSelected = position == getSelectedItemPosition();
    super.setSelection(position, animate);
    if (sameSelected) {
      // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
      getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
    }
  }

  @Override
  public void setSelection(int position) {
    boolean sameSelected = position == getSelectedItemPosition();
    super.setSelection(position);
    if (sameSelected) {
      // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
      getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
    }
  }

  @Override
  public Object getSelectedItem() {
    return (getItem(getSelectedItemPosition() + 1 ) != null) ? getItem(getSelectedItemPosition() + 1 ) : super.getSelectedItem() ;
  }

  public Object reviewSelectedObject(){
    return getSelectedItem();
  }

  public String getSelectedOptionString(){
    String selectedText = getPrompt().toString();

    TextView selectedView = (TextView) getSelectedView();

    if(selectedView != null){
      if(selectedView.getText() != null){
       selectedText = selectedView.getText().toString();
      }
    }

    return selectedText;
  }
}
