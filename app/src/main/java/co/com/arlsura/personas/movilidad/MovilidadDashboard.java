package co.com.arlsura.personas.movilidad;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationMenu;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import butterknife.BindView;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseActivity;

public class MovilidadDashboard extends BaseActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.nb_main) BottomNavigationView navigationMenu;

    @Override
    public int getLayoutId() {
        return R.layout.activity_movilidad_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationMenu.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, getNavHostId()).navigateUp();
    }

    public int getNavHostId(){
        return R.id.nav_host_fragment;
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        NavOptions.Builder navOptions = new NavOptions.Builder();
        navOptions.setLaunchSingleTop(true);

        switch (menuItem.getItemId()){
            case R.id.menu_home:
                Navigation.findNavController(this,getNavHostId()).navigate(R.id.dashboardFragment,null,navOptions.build());
                return true;
            case R.id.menu_profile:
                Navigation.findNavController(this,getNavHostId()).navigate(R.id.profileFragment,null,navOptions.build());
                return true;
            case R.id.menu_notifications:
                Navigation.findNavController(this,getNavHostId()).navigate(R.id.notificationFragment,null,navOptions.build());
                return true;
        }
        return false;
    }
}
