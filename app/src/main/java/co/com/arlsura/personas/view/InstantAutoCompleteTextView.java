package co.com.arlsura.personas.view;

/**
 * Created by home on 11/9/17.
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by PasiMatalamaki on 7.9.2015.
 */
public class InstantAutoCompleteTextView
    extends androidx.appcompat.widget.AppCompatAutoCompleteTextView {

  private boolean showAlways;

  public InstantAutoCompleteTextView(Context context) {
    super(context);
    setOnClickListener(onClickListener);
  }

  public InstantAutoCompleteTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    setOnClickListener(onClickListener);
    setKeyListener(null);
  }

  public InstantAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setOnClickListener(onClickListener);
    setKeyListener(null);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public InstantAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr,
      int defStyleRes) {
    super(context);
    setOnClickListener(onClickListener);
    setKeyListener(null);
  }

  public void setShowAlways(boolean showAlways) {
    this.showAlways = showAlways;
  }

  @Override public boolean enoughToFilter() {
    return showAlways || super.enoughToFilter();
  }

  @Override
  protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
    super.onFocusChanged(focused, direction, previouslyFocusedRect);

    showDropDownIfFocused();
  }

  private void showDropDownIfFocused() {
    if (enoughToFilter() && getWindowVisibility() == View.VISIBLE) {

      showDropDown();
    }
  }

  @Override protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    showDropDownIfFocused();
  }

  private OnClickListener onClickListener = view -> showDropDownIfFocused();
}