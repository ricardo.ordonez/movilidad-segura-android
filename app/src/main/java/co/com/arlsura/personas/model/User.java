package co.com.arlsura.personas.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/5/17.
 */

public class User extends RealmObject {


  @PrimaryKey public String document;
  public String name;
  public String typeDocument;
  public String typeDocumentCompany;
  public String documentCompany;
  public String token;
  public String status;
  public String message;
  public String companyName;


  public boolean isObservador() {
    return status.equalsIgnoreCase("SI");
  }

  public boolean isIntern() {
    return status.equalsIgnoreCase("INTERNO");
  }
}
