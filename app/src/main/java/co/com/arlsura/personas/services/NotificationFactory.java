package co.com.arlsura.personas.services;

import android.app.Notification;
import androidx.annotation.NonNull;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataContract;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import com.pushwoosh.notification.PushMessage;
import com.pushwoosh.notification.PushwooshNotificationFactory;
import java.util.List;
import timber.log.Timber;

/**
 * Created by home on 11/21/17.
 */

public class NotificationFactory extends PushwooshNotificationFactory {
  @Override public Notification onGenerateNotification(@NonNull PushMessage pushMessage) {
    Timber.e("onGenerateNotification ".concat(pushMessage.toJson().toString()));
    BasicDataPresenter presenter =
        new BasicDataPresenter(WebServices.getApi(), new ProcesoDao(), basicDataContract);
    presenter.sync(getApplicationContext());
    return super.onGenerateNotification(pushMessage);
  }

  private BasicDataContract basicDataContract = new BasicDataContract() {
    @Override public void onLoadNiveles(List<Nivel1> list) {

    }

    @Override public void onLoad(List<Comportamientos> list) {

    }

    @Override public void onLoadRootProceso(Proceso procesoRoot) {

    }

    @Override public void setLoading() {

    }

    @Override public void hideLoading() {

    }

    @Override public void onErr(String err) {

    }

    @Override public void onErr(int err) {

    }
  };
}