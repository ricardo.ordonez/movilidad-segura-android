package co.com.arlsura.personas.movilidad.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import co.com.arlsura.personas.movilidad.view.EnterpriceCustomDialog;
import co.com.arlsura.personas.R;

public class VehicleMovilidad extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageNuevoVehiculo, imageInfoPerfil;
    private Spinner spinnerEnterprice;
    private HorizontalScrollView hslVehicles;
    private LinearLayout linearEnterprice;
    private LinearLayout linearVehicles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_movilidad);
        initUi();
    }

    private void initUi() {
        imageNuevoVehiculo = (ImageView) findViewById(R.id.imageNuevoVehiculo);
        imageInfoPerfil = (ImageView) findViewById(R.id.imageInfoPerfil);
        spinnerEnterprice = (Spinner) findViewById(R.id.spinnerEnterprice);
        hslVehicles = (HorizontalScrollView) findViewById(R.id.hslVehicles);
        linearEnterprice = (LinearLayout) findViewById(R.id.linearEnterprice);

        linearVehicles = (LinearLayout) findViewById(R.id.linearVehicle);

        String[] documents = {"   Empresa 1", "   Empresa 2", "   Empresa 3", "   Empresa 4", "   Empresa 5"};

        ArrayAdapter<String> documentsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, documents);
        documentsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEnterprice.setAdapter(documentsAdapter);

        setSelectionWithoutDispatch(spinnerEnterprice, 0);
        imageNuevoVehiculo.setOnClickListener(this);
        linearVehicles.setOnClickListener(this);
        imageInfoPerfil.setOnClickListener(this);

        //ArrayAdapter<String> adaptador;

        //adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, documents);
        //listViewVehicle.setAdapter(adaptador);

        //spinnerEnterprice.setOnTouchListener(Spinner_OnTouch);
        //spinnerEnterprice.setOnKeyListener(Spinner_OnKey);

        if (getIntent() != null) {
            try {
                switch (getIntent().getExtras().getString("llave")){
                    case "FragmentVehicle":
                        imageNuevoVehiculo.setVisibility(View.GONE);
                        hslVehicles.setVisibility(View.VISIBLE);
                        break;

                    case "CreateVehicleMovilidad":
                        LinearLayout linearVehicle = (LinearLayout) findViewById(R.id.linearVehicle);
                        linearVehicle.setVisibility(View.VISIBLE);
                        //imageNuevoVehiculo.setVisibility(View.GONE);
                        linearEnterprice.setVisibility(View.VISIBLE);
                        break;
                }
            } catch (Exception e) {
                Log.i("VehicleMovilidad", e.getMessage());
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.linearVehicle:
                Log.i("LoginCustomDialog", "Iniciando vehiculos");
                Intent vehiculoIntent = new Intent(VehicleMovilidad.this, CompleteVehicleInfo.class);
                startActivity(vehiculoIntent);
                break;
            case R.id.imageNuevoVehiculo:
                Log.i("LoginCustomDialog", "Iniciando vehiculos");
                Intent completeIntent = new Intent(VehicleMovilidad.this, CreateVehicle.class);
                startActivity(completeIntent);
                break;

            case R.id.imageInfoPerfil:
                Log.i("LoginCustomDialog", "Iniciando vehiculos");
                Intent PerfilMovilidadIntent = new Intent(VehicleMovilidad.this, PerfilMovilidad.class);
                startActivity(PerfilMovilidadIntent);
                break;
        }
    }

    private void setSelectionWithoutDispatch(Spinner spinner, int position) {
        AdapterView.OnItemSelectedListener onItemSelectedListener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.setSelection(position, false);
        spinner.setOnItemSelectedListener(onItemSelectedListener);
        spinnerEnterprice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                //Toast.makeText(adapterView.getContext(), (String) adapterView.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();
                EnterpriceCustomDialog dialog = new EnterpriceCustomDialog();
                dialog.show(getSupportFragmentManager(), "Enterprice Alert");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void onBackPressed() {

    }

    /*private View.OnTouchListener Spinner_OnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Toast.makeText(v.getContext(), "hola", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    };

    private static View.OnKeyListener Spinner_OnKey = new View.OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                Toast.makeText(v.getContext(), "hola", Toast.LENGTH_SHORT).show();
                return true;
            } else {
                return false;
            }
        }
    };*/

}