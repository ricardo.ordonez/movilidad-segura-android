package co.com.arlsura.personas.model.niveles;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class Estandares extends RealmObject {
  /**
   * id : 1
   * estandar : VERIFICACION DE COMPONENTES
   * nitEmpresa : 800256161
   */

  @PrimaryKey private int id;
  private String estandar;
  private String nitEmpresa;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEstandar() {
    return estandar;
  }

  public void setEstandar(String estandar) {
    this.estandar = estandar;
  }

  public String getNitEmpresa() {
    return nitEmpresa;
  }

  public void setNitEmpresa(String nitEmpresa) {
    this.nitEmpresa = nitEmpresa;
  }
}