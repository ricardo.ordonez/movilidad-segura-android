package co.com.arlsura.personas.model.niveles;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class EstandaresProceso extends RealmObject {

  /**
   * id : 1
   * estandares : {"id":1,"estandar":"VERIFICACION DE COMPONENTES","nitEmpresa":"800256161"}
   */
  @PrimaryKey private int id;
  private Estandares estandares;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Estandares getEstandares() {
    return estandares;
  }

  public void setEstandares(Estandares estandares) {
    this.estandares = estandares;
  }
}
