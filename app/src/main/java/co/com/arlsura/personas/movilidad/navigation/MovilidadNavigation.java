package co.com.arlsura.personas.movilidad.navigation;

import android.content.Context;
import android.content.Intent;

import co.com.arlsura.personas.App;
import co.com.arlsura.personas.movilidad.MovilidadDashboard;
import co.com.arlsura.personas.movilidad.ui.LoginMovilidad;

public class MovilidadNavigation {


    public static  void startMovilidadLogin(Context context){
        Intent loginIntent = new Intent(context, LoginMovilidad.class);
        context.startActivity(loginIntent);
    }

    public static void startMovilidadDashboard(Context context){
        Intent dashboardIntent = new Intent(context, MovilidadDashboard.class);
        context.startActivity(dashboardIntent);
    }
}
