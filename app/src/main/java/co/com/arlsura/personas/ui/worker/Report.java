package co.com.arlsura.personas.ui.worker;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.dao.WorkerObserverDao;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataContract;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import co.com.arlsura.personas.util.StringUtil;
import co.com.arlsura.personas.view.CustomSpinner;
import com.jakewharton.rxbinding2.widget.RxTextView;
import io.realm.Realm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import timber.log.Timber;

public class Report extends BaseActivity {

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.lblCategoria)
  TextView lblCategoria;
  @BindView(R.id.edtCategoria)
  CustomSpinner edtCategoria;
  @BindView(R.id.lblTipoReporte)
  TextView lblTipoReporte;
  @BindView(R.id.edtTipoReporte)
  CustomSpinner edtTipoReporte;
  @BindView(R.id.lblIncidente)
  TextView lblIncidente;
  @BindView(R.id.edtIncidente)
  EditText edtIncidente;
  @BindView(R.id.btnSiguiente)
  Button btnSiguiente;

  public static Map<String, Object> paramsObserver = new HashMap<>();

  private List<Respuesta> categoriList = new ArrayList<>();
  private List<String> categoriListStr = new ArrayList<>();

  private List<Respuesta> tipoReporteList = new ArrayList<>();
  private List<String> tipoReporteListStr = new ArrayList<>();
  private boolean firstload = true;
    private boolean restored;

    @Override
  public int getLayoutId() {
    return R.layout.activity_report;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(view -> finish());
    isEnabled(btnSiguiente, false);
    initUi();
    restored = true;
    // restoreLocalData();
  }

  @Override
  protected void onPause() {
    super.onPause();
    firstload = true;
    restored = false;
  }

  @Override protected void onResume() {
    super.onResume();
    if(restored){
      restoreLocalData();


    }

  }

  private boolean isObservacionObligatoria(String tipoReporte) {

    return "Incidente".equalsIgnoreCase(tipoReporte) || "Condición insegura".equalsIgnoreCase(
        tipoReporte) || "Condicion insegura".equalsIgnoreCase(tipoReporte);
  }

  private boolean checkCategoria(){
    if((edtCategoria.getSelectedView()) != null){
      if(!((TextView)edtCategoria.getSelectedView()).getText().equals(edtCategoria.getPrompt()))
        return true;
    }
    return false;
  }

  private boolean checkReporte(){

    if((edtTipoReporte.getSelectedView()) != null){
      if(!((TextView)edtTipoReporte.getSelectedView()).getText().equals(edtTipoReporte.getPrompt()))
        return true;
    }
    return false;

  }

  private boolean checkEdts() {
    return checkCategoria() && checkReporte() && checkObservacion();
  }

  private boolean checkObservacion() {

    if(!checkReporte()){
      return false;
    }
    String tipoReporteSeleccionado = ((TextView)edtTipoReporte.getSelectedView()).getText().toString();

    boolean isObligatorio = isObservacionObligatoria(tipoReporteSeleccionado);
    if (isObligatorio) {
      return (edtIncidente.getText().length() > 0);
    } else {
      return !isObligatorio;
    }
  }

  private void initUi() {
    edtCategoria.setOnItemSelectedListener(onItemSelectedListener);
    edtTipoReporte.setOnItemSelectedListener(onItemSelectedListener);

    PreguntasDao preguntasDao = new PreguntasDao();

    List<Respuesta> cList = preguntasDao.findById("edtCategoria ");
    if (cList != null && !cList.isEmpty()) {
      onLoadCategoria(cList);
    }

    List<Respuesta> tList = preguntasDao.findById("edtTipoReporte");
    if (tList != null && !tList.isEmpty()) {
      onLoadTipoReporte(tList);
    }
    RxTextView.textChangeEvents(edtIncidente).subscribe(text -> {
      Timber.e("edtIncidente ".concat(text.text().toString()));
      isEnabled(btnSiguiente, checkEdts());
    });
  }

  private void restoreLocalData() {

    String evento =
        paramsObserver.get("evento") != null ? paramsObserver.get("evento").toString() : null;
    if (evento != null) {

      for (int i = 0; i < categoriListStr.size(); i++) {
        if (categoriListStr.get(i).equalsIgnoreCase(evento)) {
          int fixedPosition = i;

          edtCategoria.setSelection(fixedPosition);
          firstload = false;
        }
      }

    }

    String tipoReporte =
        paramsObserver.get("tipoReporte") != null ? paramsObserver.get("tipoReporte").toString()
            : null;
    if (tipoReporte != null) {
      for (int i = 0; i < tipoReporteListStr.size(); i++) {
        if (tipoReporteListStr.get(i).equalsIgnoreCase(tipoReporte)) {
          int fixedPosition = i;
          edtTipoReporte.setSelection(fixedPosition);
        }
      }
    }
    String descripcionObservacion =
        paramsObserver.get("descripcionObservacion") != null ? paramsObserver.get(
            "descripcionObservacion").toString() : null;
    if (descripcionObservacion != null) {
      edtIncidente.setText(descripcionObservacion);
    }
  }

  private void onLoadCategoria(List<Respuesta> categoriList) {
    this.categoriList.clear();
    this.categoriListStr.clear();

    for (Respuesta item : categoriList) {
      this.categoriList.add(item);
      this.categoriListStr.add(item.getNombre());
    }

    edtCategoria.initializeStringValues(categoriListStr, edtCategoria.getPrompt().toString());
  }

  private void onLoadTipoReporte(List<Respuesta> tipoReporteList) {
    this.tipoReporteList.clear();
    this.tipoReporteListStr.clear();

    for (Respuesta item : tipoReporteList) {
      this.tipoReporteList.add(item);
      this.tipoReporteListStr.add(item.getNombre());
    }

    edtTipoReporte.initializeStringValues(tipoReporteListStr,
        edtTipoReporte.getPrompt().toString());
  }

  private AdapterView.OnItemSelectedListener onItemSelectedListener =
      new AdapterView.OnItemSelectedListener() {
        @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

          int fixedPosition = i ;
          CustomSpinner demo = ((CustomSpinner) findViewById(adapterView.getId()));


          if(demo.getSelectedView() == null)
            return;

          if(((TextView)demo.getSelectedView()).getText().equals(demo.getPrompt()) && !demo.userTouch){
            return;
          }

          if(!demo.userTouch){
            isEnabled(btnSiguiente, checkEdts());
            return;
          }

          switch (adapterView.getId()) {
            case R.id.edtCategoria:
              if (!categoriList.isEmpty() && showNext(edtCategoria.getSelectedItem(),
                  edtCategoria.getPrompt())) {

                if(fixedPosition == categoriList.size())
                  fixedPosition = i-1;

                Timber.e("onItemSelected ".concat(categoriList.get(fixedPosition).getNombre()));
                paramsObserver.put("evento", categoriList.get(fixedPosition).getNombre());
                saveParams();
                isEnabled(btnSiguiente, checkEdts());
              }
              break;
            case R.id.edtTipoReporte:



              if (!tipoReporteList.isEmpty() && showNext(edtTipoReporte.getSelectedItem(),
                  edtTipoReporte.getPrompt())) {

                if(fixedPosition == tipoReporteList.size())
                  fixedPosition = i-1;

                Timber.e("onItemSelected ".concat(tipoReporteList.get(fixedPosition).getNombre()));
                paramsObserver.put("tipoReporte", tipoReporteList.get(fixedPosition).getNombre());
                String txtCompare =
                    StringUtil.removeAccents(tipoReporteList.get(fixedPosition).getNombre().toString());

                if (txtCompare.equalsIgnoreCase(getString(R.string.condicion_incidente))) {
                  lblIncidente.setHint(R.string.condicion_incidente_msg);
                  edtIncidente.setHint(R.string.condicion_incidente_msg);
                } else if (txtCompare.equalsIgnoreCase(getString(R.string.condicion_insegura))) {
                  lblIncidente.setHint(R.string.condicion_insegura_msg);
                  edtIncidente.setHint(R.string.condicion_insegura_msg);
                } else if (txtCompare.equalsIgnoreCase(
                    getString(R.string.condicion_felicitaciones))) {
                  lblIncidente.setHint(R.string.condicion_felicitaciones_msg);
                  edtIncidente.setHint(R.string.condicion_felicitaciones_msg);
                } else if (txtCompare.equalsIgnoreCase(getString(R.string.condicion_intervenir))) {
                  lblIncidente.setHint(R.string.condicion_intervenir_msg);
                  edtIncidente.setHint(R.string.condicion_intervenir_msg);
                }
                saveParams();
                isEnabled(btnSiguiente, checkEdts());
              }
              break;
          }
        }

        @Override public void onNothingSelected(AdapterView<?> adapterView) {

        }
      };

  @OnClick(R.id.btnSiguiente) public void onViewClicked() {
    saveParams();
    Actions.paramsObserver = paramsObserver;

    if(isResponsableNeeded())
      goActv(Actions.class, false);
    else{
      paramsObserver.put("accionRealizada", "No");
      paramsObserver.put("acuerdos", "");
      paramsObserver.put("estado", "Cerrado");
      paramsObserver.put("responsable","1037606403");

      String json = WebServices.getGson().toJson(paramsObserver);
      Timber.e("json Params ".concat(json));

      Status.paramsObserver = paramsObserver;


    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    WorkerObservation observation = workerObserverDao.findFirstPending();
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    observation.setParams(json);
    observation.setStatus(StatusSync.PendingSync.toString());
    observation.setToken(UserDao.token);
    realm.copyToRealmOrUpdate(observation);
    realm.commitTransaction();
    realm.close();

      goActv(Status.class,false);


    }


  }

  private boolean isResponsableNeeded(){
    String tipoReporte = (String)
            paramsObserver.get("tipoReporte");

    return !(tipoReporte.equals(getString(R.string.condicion_felicitaciones)) || tipoReporte.equals(getString(R.string.condicion_intervenir)));
  }
  private void saveParams() {
    paramsObserver.put("descripcionObservacion", edtIncidente.getText().toString());
    String json = WebServices.getGson().toJson(paramsObserver);
    Timber.e("json Params ".concat(json));

    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    WorkerObservation observation = workerObserverDao.findFirstPending();


    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    observation.setParams(json);
    observation.setTipoReporteObligatorio(
        isObservacionObligatoria(edtTipoReporte.getSelectedItem().toString()));
    observation.setStatus(StatusSync.Pending.toString());
    realm.commitTransaction();
    realm.close();
    workerObserverDao.save(observation);
  }
}
