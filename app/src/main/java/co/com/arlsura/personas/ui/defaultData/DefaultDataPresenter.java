package co.com.arlsura.personas.ui.defaultData;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.ApiService;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.configuration.Config;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.RequestError;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import co.com.arlsura.personas.util.Constants;
import com.google.gson.reflect.TypeToken;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by home on 11/7/17.
 */

public class DefaultDataPresenter {
  private defaultDataContract contract;
  private ApiService apiService;
  private PreguntasDao dao;
  private UserDao userDao;

  public DefaultDataPresenter(defaultDataContract contract, ApiService apiService,
      PreguntasDao dao) {
    this.contract = contract;
    this.apiService = apiService;
    this.dao = dao;
    userDao = new UserDao();
    userDao.cleanUsers();
  }

  public void loadPreguntas() {
    if (Config.isUiTest()) {
      contract.setLoading();
      //dao.createAllFromJson(Constants.jsonDatosQuemados);
      Type listType = new TypeToken<ArrayList<Preguntas>>() {
      }.getType();

      List<Preguntas> list = WebServices.getGson().fromJson(Constants.jsonDatosQuemados, listType);
      for (Preguntas item : list) {
        dao.save(item);
      }
      contract.onLoadPreguntas(dao.findAll());
      contract.hideLoading();
    } else {
      apiService.preguntas()
          .doOnSubscribe(disposable -> contract.setLoading())
          .subscribeOn(Schedulers.io())
          .doOnComplete(() -> contract.hideLoading())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(preguntas -> {
            for (Preguntas item : preguntas) {
              dao.save(item);
            }
            contract.onLoadPreguntas(preguntas);
          }, throwable -> {
            if (throwable instanceof HttpException) {
              HttpException error = (HttpException) throwable;
              String errorBody = error.response().errorBody().string();
              Timber.e(errorBody);
              RequestError requestError =
                  WebServices.getGson().fromJson(errorBody, RequestError.class);
              contract.onErr(requestError.getDebugMessage());
            } else {
              contract.onErr(R.string.ha_ocurrindo_un_err);
            }
            contract.hideLoading();
          }, () -> contract.hideLoading());
    }
  }

  public List<Respuesta> findRespuestasById(String id) {
    return dao.findById(id);
  }
}
