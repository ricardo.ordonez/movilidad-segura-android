package co.com.arlsura.personas.ui.observer.history;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.model.Historial;
import java.util.List;

/**
 * Created by home on 11/8/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
  private Context context;
  private List<Historial> list;

  public HistoryAdapter(Context context, List<Historial> list) {
    this.context = context;
    this.list = list;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    final View view = LayoutInflater.from(context).inflate(R.layout.item_historial, parent, false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    Historial item = list.get(position);
    Resources res = context.getResources();

    if (item != null) {
      String numero_personas = String.format(res.getString(R.string.numero_personas),
          String.valueOf(item.getPersonasObservar()));

      String actividad =
          String.format(res.getString(R.string.actividad), String.valueOf(item.getInfoActividad()));

      String area =
          String.format(res.getString(R.string.area), String.valueOf(item.getInfoNivel2Empresa()));

      String estandar =
          String.format(res.getString(R.string.estandar), String.valueOf(item.getInfoEstandar()));

      holder.lblDate.setText(item.getFechaRegistro());
      holder.lblBodega.setText(item.getInfoProceso());

      holder.lblArea.setText(area);
      holder.lblEstandar.setText(estandar);
      holder.lblActividad.setText(actividad);
      holder.lblNumeroPersonas.setText(numero_personas);
    }
  }

  @Override public int getItemCount() {
    return list.size();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.lblBodega) TextView lblBodega;
    @BindView(R.id.lblDate) TextView lblDate;
    @BindView(R.id.lblArea) TextView lblArea;
    @BindView(R.id.lblEstandar) TextView lblEstandar;
    @BindView(R.id.lblActividad) TextView lblActividad;
    @BindView(R.id.lblNumeroPersonas) TextView lblNumeroPersonas;

    ViewHolder(@NonNull View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
