package co.com.arlsura.personas.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by home on 11/8/17.
 */

public class HeaderUtil {
  public static Map<String, String> getHeader(String token) {
    Map<String, String> header = new HashMap<>();
    header.put("X-Auth-Token", token);
    return header;
  }
}
