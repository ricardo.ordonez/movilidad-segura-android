package co.com.arlsura.personas.movilidad.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import co.com.arlsura.personas.R;

public class CompanyVehicleListFragment extends Fragment {

    public CompanyVehicleListFragment(){


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company_vehicle_list, container, false);
    }
}
