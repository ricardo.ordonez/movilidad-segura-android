package co.com.arlsura.personas.ui.worker;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.widget.Toolbar;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.dao.WorkerObserverDao;
import co.com.arlsura.personas.model.Area;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.niveles.EstandaresProceso;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Nivel2;
import co.com.arlsura.personas.model.niveles.Nivel3;
import co.com.arlsura.personas.model.niveles.Nivel4;
import co.com.arlsura.personas.model.niveles.Nivel5;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.Responsable;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.model.niveles.actividadesProceso;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.observer.login.LoginContract;
import co.com.arlsura.personas.ui.observer.login.LoginPresenter;
import co.com.arlsura.personas.ui.worker.ingress.IngressContract;
import co.com.arlsura.personas.ui.worker.ingress.IngressPresenter;
import co.com.arlsura.personas.view.CustomSpinner;
import com.google.gson.reflect.TypeToken;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import io.realm.Realm;
import io.realm.RealmList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import timber.log.Timber;

public class IngressIntern extends BaseActivity implements IngressContract, com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {

  @BindView(R.id.toolbarIntern) Toolbar toolbar;
  @BindView(R.id.lblHola) TextView lblHola;
  @BindView(R.id.edtCargoTrabajador) EditText edtCargoTrabajador;
  @BindView(R.id.lblCargoTrabajador) TextView lblCargoTrabajador;
  @BindView(R.id.lblNmaeEmpresa) TextView lblNmaeEmpresa;
  @BindView(R.id.edtDate) EditText edtDate;
  @BindView(R.id.edtHour) EditText edtHour;
  @BindView(R.id.edtInstalaciones) CustomSpinner edtInstalaciones;
  @BindView(R.id.edtArea) CustomSpinner edtArea;

  @BindView(R.id.lblNiveles) TextView lblNiveles;
  @BindView(R.id.edtNivel1) CustomSpinner edtNivel1;
  @BindView(R.id.edtNivel2) CustomSpinner edtNivel2;
  @BindView(R.id.edtNivel3) CustomSpinner edtNivel3;
  @BindView(R.id.edtNivel4) CustomSpinner edtNivel4;
  @BindView(R.id.edtNivel5) CustomSpinner edtNivel5;
  @BindView(R.id.lblProcesos) TextView lblProcesos;
  @BindView(R.id.edtNivel6) CustomSpinner edtNivel6;
  @BindView(R.id.lblEstadarEvaluar) TextView lblEstadarEvaluar;
  @BindView(R.id.edtNivel7) CustomSpinner edtNivel7;
  @BindView(R.id.lblTarea) TextView lblTarea;
  @BindView(R.id.edtNivel8) CustomSpinner edtNivel8;
  @BindView(R.id.btnContinuar) Button btnContinuar;

  private List<Area> instalacionesList = new ArrayList<>();
  private List<String> instalacionesListStr = new ArrayList<>();

  private List<Area> areasList = new ArrayList<>();
  private List<String> areasListStr = new ArrayList<>();

  private List<Nivel1> nivel1List = new ArrayList<>();
  private List<Nivel2> nivel2List = new ArrayList<>();
  private List<Nivel3> nivel3List = new ArrayList<>();
  private List<Nivel4> nivel4List = new ArrayList<>();
  private List<Nivel5> nivel5List = new ArrayList<>();
  private List<Proceso> procesoList = new ArrayList<>(); // 6
  private List<EstandaresProceso> estandaresProcesoList = new ArrayList<>(); // 7
  private List<actividadesProceso> actividadesProcesoList = new ArrayList<>(); //8
  private Proceso currentProceso;

  private List<String> nivel1ListStr = new ArrayList<>();
  private List<String> nivel2ListStr = new ArrayList<>();
  private List<String> nivel3ListStr = new ArrayList<>();
  private List<String> nivel4ListStr = new ArrayList<>();
  private List<String> nivel5ListStr = new ArrayList<>();

  private List<String> nivel6ListStr = new ArrayList<>();
  private List<String> nivel7ListStr = new ArrayList<>();
  private List<String> nivel8ListStr = new ArrayList<>();

  private RealmList<Responsable> responsables;

  private LoginPresenter loginPresenter;
  private IngressPresenter ingressPresenter;

  private DatePickerDialog datePickerDialog;
  private TimePickerDialog dateTimeDialog;
  SimpleDateFormat formatoDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
  SimpleDateFormat formatoHour = new SimpleDateFormat("HH:mm", Locale.getDefault());

  private Map<String, Object> paramsObserver = new HashMap<>();
  private boolean isLevelsOK;
  private String cargoConstante = "Interno";
  private int selectedLevel;
  private List<Responsable> responsableList;



  @Override public int getLayoutId() {
    return R.layout.activity_ingress_intern;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initPresenter();
    toolbar.inflateMenu(R.menu.menu_salir);
    toolbar.setOnMenuItemClickListener(item -> {
      switch (item.getItemId()) {
        case R.id.action_salir:
          showAlertDialog(R.string.confim_exit, new confirmDialog() {
            @Override public void onPositive() {
              loginPresenter.logOut();
            }

            @Override public void onNegative() {

            }
          });
          break;
      }
      return true;
    });
    initUi();
    ingressPresenter.loadUser();
    ingressPresenter.loadNiveles();
    ingressPresenter.loadInstalaciones();
    ingressPresenter.loadAreasExterno();

    paramsObserver.put("idNivel1Empresa", "");
    paramsObserver.put("idNivel2Empresa", "");
    paramsObserver.put("idNivel3Empresa", "");
    paramsObserver.put("idNivel4Empresa", "");
    paramsObserver.put("idNivel5Empresa", "");
    paramsObserver.put("idProcesos", "");
    paramsObserver.put("idEstandares", "");
    paramsObserver.put("idActividades", "");
  }

  @Override protected void onResume() {
    super.onResume();
    if (checkPending()) {
      showAlertDialogNeutral(R.string.observaciones_pendientes, new confirmDialog() {
        @Override public void onPositive() {
          Report.paramsObserver = paramsObserver;
          goActv(Report.class, false);
        }

        @Override public void onNegative() {

        }
      });
    }

  }

  private boolean checkPending() {
    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    WorkerObservation workerObservation = workerObserverDao.findFirstPending();
    if (workerObservation != null) {
      Timber.e("checkPending " + workerObservation.getParams());
      paramsObserver = WebServices.getGson()
          .fromJson(workerObservation.getParams(), new TypeToken<HashMap<String, Object>>() {
          }.getType());
    }
    return workerObservation != null;
  }

  private DatePickerDialog.OnDateSetListener onDateSetListener =
      (view, year, monthOfYear, dayOfMonth) -> {
        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear, dayOfMonth);
        try {
          edtDate.setText(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
          Timber.e(e);
        }
      };

  private TimePickerDialog.OnTimeSetListener onTimeSetListener =
      (view, hourOfDay, minute, second) -> {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE,minute);
        edtHour.setText(formatoHour.format(calendar.getTime()));
      };

  private void initPresenter() {
    loginPresenter = new LoginPresenter(loginContract, WebServices.getApi(), new UserDao());
    ingressPresenter = new IngressPresenter(this, WebServices.getApi());
  }

  private LoginContract loginContract = new LoginContract() {
    @Override public void onLoadUser(User user) {

    }

    @Override public void onSuccesLogOut() {
      goActv(SelectProfile.class, true);
    }

    @Override public void setLoading() {
      Timber.d("setLoading");
      showLoading(getString(R.string.loading));
    }

    @Override public void hideLoading() {
      Timber.d("hideLoading");
      dissmisLoading();
    }

    @Override public void onErr(String err) {
      Timber.d("onErr");
      showAlertDialog(err);
    }

    @Override public void onErr(int err) {
      Timber.d(getString(err));
      showAlertDialog(err);
    }
  };

  @OnClick({ R.id.edtDate, R.id.edtHour }) public void onViewClicked(View view) {
    switch (view.getId()) {
      case R.id.edtDate:
        String[] fecha = ((TextView) findViewById(R.id.edtDate)).getText().toString().split("-");

        int year = Integer.parseInt(fecha[0]);
        int month = Integer.parseInt(fecha[1]);
        int day = Integer.parseInt(fecha[2]);

        builder.defaultDate(year,month-1,day);
        builder.build().show();

        break;
      case R.id.edtHour:
        dateTimeDialog.show(getFragmentManager(), "Timepickerdialog");
        break;
    }
  }

  private void fillResponsables(RealmList<Responsable> responsables){
      if(responsables.isEmpty())
          return;

      responsables.clear();
      this.responsableList.addAll(responsables);
  }

  private boolean levelHasResponsable(Object nivel){

      if(nivel instanceof Nivel1){
          fillResponsables(((Nivel1) nivel).getResponsables());
      }
      if(nivel instanceof Nivel2){
          fillResponsables(((Nivel2) nivel).getResponsables());
      }

      if(nivel instanceof Nivel3){
          fillResponsables(((Nivel3) nivel).getResponsables());
      }

      if(nivel instanceof Nivel4){
          fillResponsables(((Nivel4) nivel).getResponsables());
      }

      if(nivel instanceof Nivel5){
          fillResponsables(((Nivel5) nivel).getResponsables());
      }

      return !responsableList.isEmpty();

  }

  public boolean checkEdts() {

    return edtDate.getText().length() > 0
            && edtHour.getText().length() > 0
            && checkArea()
            && checkInstalaciones()
            && edtCargoTrabajador.getText().length() > 0
            && isLevelsOK;
  }



  private boolean checkArea(){
    if(edtArea.getSelectedView() == null){
      return false;
    }

    return !((TextView)edtArea.getSelectedView()).getText().toString().equals(edtArea.getPrompt());
  }

  private boolean checkInstalaciones() {
    if (edtInstalaciones.getSelectedView() == null) {
      return false;
    }
    return !((TextView)edtInstalaciones.getSelectedView()).getText().toString().equals(edtInstalaciones.getPrompt());

  }


    public void validateForm() {



        if (edtDate.getText().toString().isEmpty()) {
            showAlertDialog("Para continuar debes ingresar la fecha de observacion");
            return;
        }

        if (edtHour.getText().toString().isEmpty()) {
            showAlertDialog("Para continuar debes ingresar la hora de observacion");
            return;
        }


        if (edtCargoTrabajador.getText().toString().isEmpty()) {
            showAlertDialog("Para continuar debes ingresar su cargo en la empresa");
            return;
        }




        if (!checkInstalaciones() ) {
                showAlertDialog("Para continuar debes seleccionar una instalacion");
        }
    }


  private void initUi() {
    RxTextView.textChangeEvents(edtCargoTrabajador).subscribe(text -> {
      isEnabled(btnContinuar, checkEdts());
    });


    createDatePickerDialog();
    Calendar now = Calendar.getInstance();
    datePickerDialog = DatePickerDialog.newInstance(onDateSetListener, now.get(Calendar.YEAR),
        now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));

    dateTimeDialog = TimePickerDialog.newInstance(onTimeSetListener, true);

    edtDate.setText(formatoDate.format(now.getTime()));
    edtHour.setText(formatoHour.format(now.getTime()));

    edtInstalaciones.setOnItemSelectedListener(onItemSelectedListener);
    edtArea.setOnItemSelectedListener(onItemSelectedListener);

    edtNivel1.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel2.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel3.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel4.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel5.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel6.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel7.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel8.setOnItemSelectedListener(onItemSelectedListener);



  }

  private void updateItems(List<Nivel1> nivel1List) {

    for (Nivel1 item1 : nivel1List) {
      nivel1ListStr.add(item1.getNombreNivel());

      for (Nivel2 item2 : item1.getNiveles2()) {
        nivel2List.add(item2);
        nivel2ListStr.add(item2.getNombreNivel());
        for (Nivel3 item3 : item2.getNiveles3()) {
          nivel3List.add(item3);
          nivel3ListStr.add(item3.getNombreNivel());
          for (Nivel4 item4 : item3.getNiveles4()) {
            nivel4List.add(item4);
            nivel4ListStr.add(item4.getNombreNivel());
            for (Nivel5 item5 : item4.getNiveles5()) {
              nivel5List.add(item5);
              nivel5ListStr.add(item5.getNombreNivel());
            }
          }
        }
      }
    }

    edtNivel1.initializeStringValues(nivel1ListStr, edtNivel1.getPrompt().toString());
    edtNivel2.initializeStringValues(nivel2ListStr, edtNivel2.getPrompt().toString());
    edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems2(List<Nivel2> list) {
    this.nivel2List.clear();
    this.nivel2ListStr.clear();
    this.nivel3List.clear();
    this.nivel3ListStr.clear();
    this.nivel4List.clear();
    this.nivel4ListStr.clear();
    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel2 item2 : list) {
      nivel2List.add(item2);
      nivel2ListStr.add(item2.getNombreNivel());
      for (Nivel3 item3 : item2.getNiveles3()) {
        nivel3List.add(item3);
        nivel3ListStr.add(item3.getNombreNivel());
        for (Nivel4 item4 : item3.getNiveles4()) {
          nivel4List.add(item4);
          nivel4ListStr.add(item4.getNombreNivel());
          for (Nivel5 item5 : item4.getNiveles5()) {
            nivel5List.add(item5);
            nivel5ListStr.add(item5.getNombreNivel());
          }
        }
      }
    }

    edtNivel2.initializeStringValues(nivel2ListStr, edtNivel2.getPrompt().toString());
    edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems3(List<Nivel3> list) {
    this.nivel3List.clear();
    this.nivel3ListStr.clear();
    this.nivel4List.clear();
    this.nivel4ListStr.clear();
    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel3 item3 : list) {
      nivel3List.add(item3);
      nivel3ListStr.add(item3.getNombreNivel());
      for (Nivel4 item4 : item3.getNiveles4()) {
        nivel4List.add(item4);
        nivel4ListStr.add(item4.getNombreNivel());
        for (Nivel5 item5 : item4.getNiveles5()) {
          nivel5List.add(item5);
          nivel5ListStr.add(item5.getNombreNivel());
        }
      }
    }

    edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems4(List<Nivel4> list) {

    this.nivel4List.clear();
    this.nivel4ListStr.clear();
    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel4 item4 : list) {
      nivel4List.add(item4);
      nivel4ListStr.add(item4.getNombreNivel());
      for (Nivel5 item5 : item4.getNiveles5()) {
        nivel5List.add(item5);
        nivel5ListStr.add(item5.getNombreNivel());
      }
    }

    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems5(List<Nivel5> list) {

    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel5 item5 : list) {
      nivel5List.add(item5);
      nivel5ListStr.add(item5.getNombreNivel());
    }
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateProcesos(List<Proceso> list) {
    procesoList.clear();
    nivel6ListStr.clear();
    estandaresProcesoList.clear();
    nivel7ListStr.clear();
    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (Proceso itemProceso : list) {
      procesoList.add(itemProceso);
      nivel6ListStr.add(itemProceso.getProceso());
      for (EstandaresProceso itemEstandaresProceso : itemProceso.getEstandaresProcesos()) {
        nivel7ListStr.add(itemEstandaresProceso.getEstandares().getEstandar());
        estandaresProcesoList.add(itemEstandaresProceso);
      }
      for (actividadesProceso itemActividadesProceso : itemProceso.getActividadesProcesos()) {
        nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
        actividadesProcesoList.add(itemActividadesProceso);
      }
    }
    edtNivel6.initializeStringValues(nivel6ListStr, edtNivel6.getPrompt().toString());
    edtNivel7.initializeStringValues(nivel7ListStr, edtNivel7.getPrompt().toString());
    edtNivel8.initializeStringValues(nivel8ListStr, edtNivel8.getPrompt().toString());
  }

  private void updateProcesos7(Proceso itemProceso) {
    estandaresProcesoList.clear();
    nivel7ListStr.clear();

    for (EstandaresProceso itemEstandaresProceso : itemProceso.getEstandaresProcesos()) {
      nivel7ListStr.add(itemEstandaresProceso.getEstandares().getEstandar());
      estandaresProcesoList.add(itemEstandaresProceso);
    }

    edtNivel7.initializeStringValues(nivel7ListStr, edtNivel7.getPrompt().toString());
  }

  private void updateProcesos8(Proceso itemProceso) {
    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (actividadesProceso itemActividadesProceso : itemProceso.getActividadesProcesos()) {
      nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
      actividadesProcesoList.add(itemActividadesProceso);
    }

    edtNivel8.initializeStringValues(nivel8ListStr, edtNivel8.getPrompt().toString());
  }

  private boolean userTouch;
  private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      userTouch = true;
      return false;
    }
  };



  private CustomSpinner.OnItemSelectedListener onItemSelectedListener2 =
          new CustomSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


              int fixedPosition = position;

              switch (parent.getId()) {
                case R.id.edtInstalaciones:
                  if (!instalacionesList.isEmpty() && showNext(edtInstalaciones.getSelectedItem(),
                          edtInstalaciones.getPrompt())) {

                    paramsObserver.put("idArea", null);
                    boolean isEmpresa = "EMPRESA".equalsIgnoreCase(
                            instalacionesList.get(fixedPosition).getNombreUbicacion());
                    if (!isEmpresa) {
                      ingressPresenter.loadAreasExterno();
                    } else {
                      ingressPresenter.loadAreas();
                    }
                  }
                  break;
                case R.id.edtArea:
                  if (!areasList.isEmpty() && showNext(areasList.get(fixedPosition).getNombreUbicacion(),
                          edtArea.getPrompt())) {
                    paramsObserver.put("idArea", areasList.get(fixedPosition).getId());

                  }
                  break;
                case R.id.edtNivel1:
                  if (!nivel1List.isEmpty()) {
                    updateItems2(nivel1List.get(fixedPosition).getNiveles2());
                    Timber.d("onItemSelected Nivel 1 ".concat(
                            nivel1List.get(fixedPosition).getNombreNivel()));
                    Timber.d("showNext Nivel 2 ".concat(
                            String.valueOf(showNext(edtNivel1.getSelectedItem(), edtNivel1.getPrompt()))));
                    paramsObserver.put("idNivel1Empresa", nivel1List.get(fixedPosition).getId());

                    edtNivel2.setVisibility(
                            showNext(edtNivel1.getSelectedItem(), edtNivel1.getPrompt()) ? View.VISIBLE
                                    : View.GONE);
                    if (showNext(edtNivel1.getSelectedItem(), edtNivel1.getPrompt())) {
                      responsables = nivel1List.get(fixedPosition).getResponsables();
                      Timber.e("responsables size ".concat(String.valueOf(responsables.size())));
                    }
                    edtNivel3.setVisibility(View.GONE);
                    edtNivel4.setVisibility(View.GONE);
                    edtNivel5.setVisibility(View.GONE);
                    isEnabled(btnContinuar, false);
                    if (showSubLevls(nivel1List.get(fixedPosition).getProcesos()) & !nivel1List.get(
                            fixedPosition).getResponsables().isEmpty()) {
                      updateProcesos(nivel1List.get(fixedPosition).getProcesos());

                      edtNivel6.setVisibility(
                              showSubLevls(nivel1List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                      lblProcesos.setVisibility(
                              showSubLevls(nivel1List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                    }
                  }

                  break;
                case R.id.edtNivel2:
                  if (!nivel2List.isEmpty() && edtNivel2.getVisibility() == View.VISIBLE) {
                    paramsObserver.put("idNivel2Empresa", nivel2List.get(fixedPosition).getId());
                    Timber.d("onItemSelected Nivel 2 ".concat(
                            nivel2List.get(fixedPosition).getNombreNivel()));

                     updateItems3(nivel2List.get(fixedPosition).getNiveles3());

                    edtNivel3.setVisibility(
                            showNext(edtNivel2.getSelectedItem(), edtNivel2.getPrompt()) ? View.VISIBLE
                                    : View.GONE);

                    edtNivel4.setVisibility(View.GONE);
                    edtNivel5.setVisibility(View.GONE);
                    if (showNext(edtNivel2.getSelectedItem(), edtNivel2.getPrompt()) && !nivel2List.get(
                            fixedPosition).getResponsables().isEmpty()) {
                      responsables = nivel2List.get(fixedPosition).getResponsables();
                      Timber.e("responsables size ".concat(String.valueOf(responsables.size())));
                    }
                    isEnabled(btnContinuar, false);
                    if (showSubLevls(nivel2List.get(fixedPosition).getProcesos())) {

                      updateProcesos(nivel2List.get(fixedPosition).getProcesos());
                      edtNivel6.setVisibility(
                              showSubLevls(nivel2List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                      lblProcesos.setVisibility(
                              showSubLevls(nivel2List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                    }
                  }

                  break;
                case R.id.edtNivel3:
                  if (!nivel3List.isEmpty() && edtNivel3.getVisibility() == View.VISIBLE) {
                    paramsObserver.put("idNivel3Empresa", nivel3List.get(fixedPosition).getId());
                    Timber.d(
                            "onItemSelected Nivel 3 ".concat(nivel3List.get(fixedPosition).toString()));
                    updateItems4(nivel3List.get(fixedPosition).getNiveles4());

                    edtNivel4.setVisibility(
                            showNext(edtNivel3.getSelectedItem(), edtNivel3.getPrompt()) ? View.VISIBLE
                                    : View.GONE);
                    if (showNext(edtNivel3.getSelectedItem(), edtNivel3.getPrompt()) & !nivel3List.get(
                            fixedPosition).getResponsables().isEmpty()) {
                      responsables = nivel3List.get(fixedPosition).getResponsables();
                    }

                    edtNivel5.setVisibility(View.GONE);
                    isEnabled(btnContinuar, false);
                    if (showSubLevls(nivel3List.get(fixedPosition).getProcesos())) {

                      updateProcesos(nivel3List.get(fixedPosition).getProcesos());
                      edtNivel6.setVisibility(
                              showSubLevls(nivel3List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                      lblProcesos.setVisibility(
                              showSubLevls(nivel3List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                    }
                  }

                  break;
                case R.id.edtNivel4:
                  if (!nivel4List.isEmpty() && edtNivel4.getVisibility() == View.VISIBLE) {
                    paramsObserver.put("idNivel4Empresa", nivel4List.get(fixedPosition).getId());
                    Timber.d("onItemSelected Nivel 4 ".concat(
                            nivel4List.get(fixedPosition).getNombreNivel()));

                    updateItems5(nivel4List.get(fixedPosition).getNiveles5());

                    edtNivel5.setVisibility(
                            showNext(edtNivel4.getSelectedItem(), edtNivel4.getPrompt()) ? View.VISIBLE
                                    : View.GONE);
                    isEnabled(btnContinuar, false);
                    if (showNext(edtNivel4.getSelectedItem(), edtNivel4.getPrompt()) & !nivel4List.get(
                            fixedPosition).getResponsables().isEmpty()) {
                      responsables = nivel4List.get(fixedPosition).getResponsables();
                    }
                    if (showSubLevls(nivel4List.get(fixedPosition).getProcesos())) {
                      updateProcesos(nivel4List.get(fixedPosition).getProcesos());
                      edtNivel6.setVisibility(
                              showSubLevls(nivel4List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                      lblProcesos.setVisibility(
                              showSubLevls(nivel4List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                    }
                  }

                  break;
                case R.id.edtNivel5:
                    if (!nivel5List.isEmpty() && edtNivel5.getVisibility() == View.VISIBLE) {
                    paramsObserver.put("idNivel5Empresa", nivel5List.get(fixedPosition).getId());
                    Timber.d("onItemSelected Nivel 5 ".concat(
                            nivel5List.get(fixedPosition).getNombreNivel()));
                    isEnabled(btnContinuar, false);
                    if (showNext(edtNivel5.getSelectedItem(), edtNivel5.getPrompt()) & !nivel5List.get(
                            fixedPosition).getResponsables().isEmpty()) {
                      responsables = nivel5List.get(fixedPosition).getResponsables();
                    }
                    if (showSubLevls(nivel5List.get(fixedPosition).getProcesos())) {
                      updateProcesos(nivel5List.get(fixedPosition).getProcesos());

                      edtNivel6.setVisibility(
                              showSubLevls(nivel5List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                      lblProcesos.setVisibility(
                              showSubLevls(nivel5List.get(fixedPosition).getProcesos()) ? View.VISIBLE
                                      : View.GONE);
                    }
                  }

                  break;
                case R.id.edtNivel6:
                  if (!procesoList.isEmpty() && edtNivel6.getVisibility() == View.VISIBLE) {

                    Timber.d(
                            "onItemSelected Nivel 6 ".concat(procesoList.get(fixedPosition).getProceso()));
                    currentProceso = procesoList.get(fixedPosition);
                    paramsObserver.put("idProcesos", currentProceso.getId());
                    updateProcesos7(currentProceso);
                    updateProcesos8(currentProceso);
                    lblEstadarEvaluar.setVisibility(
                            estandaresProcesoList.isEmpty() ? View.GONE : View.VISIBLE);
                    edtNivel7.setVisibility(estandaresProcesoList.isEmpty() ? View.GONE : View.VISIBLE);
                    lblTarea.setVisibility(actividadesProcesoList.isEmpty() ? View.GONE : View.VISIBLE);
                    edtNivel8.setVisibility(
                            actividadesProcesoList.isEmpty() ? View.GONE : View.VISIBLE);
                    isEnabled(btnContinuar, false);
                  }
                  break;
                case R.id.edtNivel7:
                  if (!estandaresProcesoList.isEmpty() && edtNivel7.getVisibility() == View.VISIBLE) {
                    paramsObserver.put("idEstandares",
                            estandaresProcesoList.get(fixedPosition).getEstandares().getId());
                    Timber.d("onItemSelected Nivel 7 ".concat(
                            estandaresProcesoList.get(fixedPosition).getEstandares().getEstandar()));
                    updateProcesos8(currentProceso);
                    lblTarea.setVisibility(actividadesProcesoList.isEmpty() ? View.GONE : View.VISIBLE);
                    edtNivel8.setVisibility(
                            actividadesProcesoList.isEmpty() ? View.GONE : View.VISIBLE);
                    isEnabled(btnContinuar, false);
                  }

                  break;
                case R.id.edtNivel8:
                  if (!actividadesProcesoList.isEmpty() && edtNivel8.getVisibility() == View.VISIBLE) {
                    paramsObserver.put("idActividades",
                            actividadesProcesoList.get(fixedPosition).getActividades().getId());
                    Timber.d("onItemSelected Nivel 8 ".concat(
                            actividadesProcesoList.get(fixedPosition).getActividades().getActividad()));
                    isLevelsOK = showNext(edtNivel8.getSelectedItem(), edtNivel8.getPrompt());
                    isEnabled(btnContinuar, checkEdts());
                    if (!checkEdts() && showNext(edtNivel8.getSelectedItem(), edtNivel8.getPrompt())) {
                      showAlertDialog(R.string.campos_pendientes);
                    }
                  }

                  break;
              }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

            @Override
            public void onAfterItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }
          };

  @Override public void setLoading() {
    Timber.d("setLoading");
    showLoading(getString(R.string.loading));
  }

  @Override public void hideLoading() {
    Timber.d("hideLoading");
    dissmisLoading();
  }

  @Override public void onErr(String err) {
    Timber.d("onErr");
    showAlertDialog(err);
  }

  @Override public void onErr(int err) {
    Timber.d(getString(err));
    showAlertDialog(err);
  }

  @Override public void onLoadUser(User user) {
    Resources res = getResources();
    String hello = String.format(res.getString(R.string.hola_usuario), user.name);
    runOnUiThread(() -> {
      lblHola.setText(hello);
      lblNmaeEmpresa.setText(user.companyName);
    });
  }

  private CustomSpinner.OnItemSelectedListener onItemSelectedListener = new CustomSpinner.OnItemSelectedListener() {

    @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
      //i = index;

    }

    @Override public void onNothingSelected(AdapterView<?> adapterView) {
      Timber.d("tipoDoumento onNothingSelected ");
    }

    @Override
    public void onAfterItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


      int fixedPosition = i;

      Timber.d("fixedPosition ".concat(String.valueOf(fixedPosition)));

      Timber.d("fixedPosition ".concat(String.valueOf(fixedPosition)));
      CustomSpinner demo = ((CustomSpinner) findViewById(adapterView.getId()));

      if(((TextView)demo.getSelectedView()).getText().equals(demo.getPrompt())){
        return;
      }

      switch (adapterView.getId()) {

        case R.id.edtInstalaciones:
          if (!instalacionesList.isEmpty() && showNext(edtInstalaciones.getSelectedItem(),
                  edtInstalaciones.getPrompt())) {

            paramsObserver.put("idArea", null);
            boolean isEmpresa = "EMPRESA".equalsIgnoreCase(
                    instalacionesList.get(fixedPosition).getNombreUbicacion());
            if (!isEmpresa) {
              ingressPresenter.loadAreasExterno();
            } else {
              ingressPresenter.loadAreas();
            }
          }
          break;
        case R.id.edtArea:
          if (!areasList.isEmpty() && showNext(edtArea.getSelectedItem(),
                  edtArea.getPrompt())) {
            paramsObserver.put("idArea", areasList.get(fixedPosition).getId());


          }
          break;
        case R.id.edtNivel1:
          paramsObserver.put("idNivel1Empresa", nivel1List.get(fixedPosition).getId());
          handleFormUIChanges(nivel1List.get(fixedPosition));
          break;
        case R.id.edtNivel2:
          handleFormUIChanges(nivel2List.get(fixedPosition));
          paramsObserver.put("idNivel2Empresa", nivel2List.get(fixedPosition).getId());
          break;
        case R.id.edtNivel3:
          handleFormUIChanges(nivel3List.get(fixedPosition));
          paramsObserver.put("idNivel3Empresa", nivel3List.get(fixedPosition).getId());
          break;
        case R.id.edtNivel4:
          handleFormUIChanges(nivel4List.get(fixedPosition));
          paramsObserver.put("idNivel4Empresa", nivel4List.get(fixedPosition).getId());
          break;
        case R.id.edtNivel5:
          handleFormUIChanges(nivel5List.get(fixedPosition));
          paramsObserver.put("idNivel5Empresa", nivel5List.get(fixedPosition).getId());

          break;
        case R.id.edtNivel6:
          currentProceso = procesoList.get(fixedPosition);



          handleFormUIChanges(currentProceso);
          paramsObserver.put("idProcesos", currentProceso.getId());
          break;
        case R.id.edtNivel7:

          paramsObserver.put("idEstandares",
                  estandaresProcesoList.get(fixedPosition).getEstandares().getId());

          break;
        case R.id.edtNivel8:
          paramsObserver.put("idActividades",
                  actividadesProcesoList.get(fixedPosition).getActividades().getId());
          break;
      }
      validateForm();
      isEnabled(btnContinuar,checkEdts());
    }
  };

  @Override public void onLoadNiveles(List<Nivel1> list) {
    Timber.d("onLoadNiveles ".concat(String.valueOf(list.size())));
    this.nivel1List = list;
    updateItems(list);
  }

  @Override public void onLoadInstalaciones(List<Area> areas) {
    runOnUiThread(() -> {

      instalacionesList.clear();
      instalacionesListStr.clear();

      for (Area item : areas) {
//        if("EMPRESA".equalsIgnoreCase(item.getNombreUbicacion()))
//          instalacionesListStr.add("VISITANTE");
//        else{
//          instalacionesListStr.add(item.getNombreUbicacion());
//
        instalacionesListStr.add(item.getNombreUbicacion());
        instalacionesList.add(item);

      }

      edtInstalaciones.initializeStringValues(instalacionesListStr,
          edtInstalaciones.getPrompt().toString());
    });
  }

  @Override public void onLoadAreas(List<Area> areas) {
    runOnUiThread(() -> {
      areasList.clear();
      areasListStr.clear();

      for (Area item : areas) {
        areasListStr.add(item.getNombreUbicacion());
        areasList.add(item);
      }

      edtArea.initializeStringValues(areasListStr, edtArea.getPrompt().toString());

    });
    edtArea.setVisibility(View.VISIBLE);
    findViewById(R.id.lblAreaEmpresa).setVisibility(View.VISIBLE);
  }

  @Override
  public void onClose() {
    try{

      final Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          //Do something after 100ms
          loginPresenter.logOut();
        }
      }, 1000);
    }catch (Exception e){
      Timber.d("Error starting over");
    }
  }

  @OnClick(R.id.btnContinuar) public void onViewClicked() {

    if(responsableList.isEmpty()){
      showAlertDialog("Recuerde seleccionar niveles necesarios");
      return;
    }


    if(edtArea.getSelectedItem() == null){
      showAlertDialog("Recuerde seleccionar el area");
      return;
    }
    paramsObserver.put("fechaRegistro", edtDate.getText().toString());
    paramsObserver.put("horaRegistro", edtHour.getText().toString());
    User user = new UserDao().findFirst();
    paramsObserver.put("nitEmpresa", user.documentCompany);
    paramsObserver.put("cedulaTrabajador", user.document);
    paramsObserver.put("nombreTrabajador", user.name);
    paramsObserver.put("cargoTrabajador", edtCargoTrabajador.getText().toString());
    paramsObserver.put("nombreEmpresa", user.companyName);
    paramsObserver.put("nombreEmpresaExterno", user.companyName);
    paramsObserver.put("personasObservar", 1);
    paramsObserver.put("instalaciones", edtInstalaciones.getSelectedItem().toString());
    String area = ((TextView) edtArea.getSelectedView()).getText().toString();

    paramsObserver.put("ubicacion", area);
    paramsObserver.put("lugar", "");

    String json = WebServices.getGson().toJson(paramsObserver);
    WorkerObservation observation = new WorkerObservation();

    RealmList<Responsable> responsables = new RealmList<>();

    for(Responsable responsable : responsableList){
      responsables.add(responsable);
    }


    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    observation.setParams(json);
    observation.setStatus(StatusSync.Pending.toString());
    observation.setResponsables(responsables);
    realm.commitTransaction();
    realm.close();
    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    workerObserverDao.save(observation);
    Timber.e(json);
    Report.paramsObserver = paramsObserver;
    goActv(Report.class, false);
  }


  ////////////////////////////////////////////////////////////////////////////////////////////
  //TODO CREATE A PRESENTER with the code below
  ///////////////////////////////////////////////////////////////////////////////////////////

  /*
  handleFormUIChanges
updateData
updateSpinnersVisibility
updateProcessSpinnersVisibility
populateProcessSpinners
populateLevelSpinners
updateLevelProcess
searchLevelProcess
fillNextLevelLists
updateProcessData
updateSubProcessData
clearLevelsFrom
getLevelNumber
isProcessSet
searchParentProcess
   */

  private boolean isProcessSet(){

    boolean isProcessSet;
    boolean isActivitySet;
    boolean isStandartSet;

    isProcessSet = paramsObserver.containsKey("idProcesos") &&  numberIsSet(paramsObserver.get("idProcesos"));
    isActivitySet = paramsObserver.containsKey("idActividades")  && numberIsSet(paramsObserver.get("idActividades"));
    isStandartSet = paramsObserver.containsKey("idEstandares")  && numberIsSet(paramsObserver.get("idEstandares"));

    return isProcessSet || isActivitySet || isStandartSet;
  }

  private boolean numberIsSet(Object object){
    if(object instanceof  String)
      return !((String) object).isEmpty();
    else if(object instanceof Integer)
      return ((Integer) object) != 0;
    return false;
  }

  private void handleFormUIChanges(Object nivel){

    updateData(nivel);

    populateLevelSpinners(selectedLevel + 1);
    updateSpinnersVisibility(selectedLevel + 1);
    isLevelsOK = !responsableList.isEmpty();//levelHasResponsable(nivel);

  }

  private boolean isProcessEmpty(){

    boolean resultado;
    switch (selectedLevel){
      case 1:
        resultado = nivel2List.isEmpty() && procesoList.isEmpty();
        break;
      case 2:
        resultado =nivel3List.isEmpty() && procesoList.isEmpty();
        break;
      case 3:
        resultado =nivel4List.isEmpty() && procesoList.isEmpty();
        break;
      case 4:
        resultado =nivel5List.isEmpty() && procesoList.isEmpty();
        break;
      case 5:
        resultado = procesoList.isEmpty();
        break;
      default:
        resultado =false;
        break;
    }
    return resultado;
  }


  private void updateData(Object nivel){
    selectedLevel = getLevelNumber(nivel);

    if(selectedLevel > 0 && selectedLevel < 6){

      clearLevelsFrom(selectedLevel);
      fillNextLevelLists(nivel);
      updateLevelProcess(nivel);
      updateLevelResponsable(nivel);



    }else if(nivel instanceof Proceso){
      updateSubProcessData((Proceso) nivel);
    }

  }
  private void updateSpinnersVisibility(int newLevel){

    edtNivel2.setVisibility((newLevel > 1 && !nivel2ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
    edtNivel3.setVisibility((newLevel > 2 && !nivel3ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
    edtNivel4.setVisibility((newLevel > 3 && !nivel4ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
    edtNivel5.setVisibility((newLevel > 4 && !nivel5ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
  }


  private void updateProcessSpinnersVisibility(){

    lblProcesos.setVisibility(procesoList.isEmpty() ? View.GONE : View.VISIBLE);
    edtNivel6.setVisibility(procesoList.isEmpty() ? View.GONE : View.VISIBLE);

    if(currentProceso == null){
      lblEstadarEvaluar.setVisibility(View.GONE);
      lblTarea.setVisibility(View.GONE);
      edtNivel7.setVisibility(View.GONE);
      edtNivel8.setVisibility(View.GONE);
      return;
    }

    lblEstadarEvaluar.setVisibility(currentProceso.getEstandaresProcesos().isEmpty() ? View.GONE : View.VISIBLE);
    lblTarea.setVisibility(currentProceso.getActividadesProcesos().isEmpty() ? View.GONE : View.VISIBLE);
    edtNivel7.setVisibility(currentProceso.getEstandaresProcesos().isEmpty() ? View.GONE : View.VISIBLE);
    edtNivel8.setVisibility(currentProceso.getActividadesProcesos().isEmpty() ? View.GONE : View.VISIBLE);
  }

  private void populateProcessSpinners(){
    if(selectedLevel != 6)
      edtNivel6.initializeStringValues(nivel6ListStr, edtNivel6.getPrompt().toString());
    edtNivel7.initializeStringValues(nivel7ListStr, edtNivel7.getPrompt().toString());
    edtNivel8.initializeStringValues(nivel8ListStr, edtNivel8.getPrompt().toString());
  }
  private void populateLevelSpinners(int nivel){

    switch (nivel){
      case 1:
        edtNivel1.initializeStringValues(nivel1ListStr, edtNivel1.getPrompt().toString());
        break;
      case 2:
        edtNivel2.initializeStringValues(nivel2ListStr, edtNivel2.getPrompt().toString());
        break;
      case 3:
        edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
        break;
      case 4:
        edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
        break;
      case 5:
        edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
        break;
    }


  }




  private void updateLevelProcess(Object nivel){

    int levelNumber = getLevelNumber(nivel);
    List<Proceso> procesos = new ArrayList<>();

    switch (levelNumber){
      case 1:
        procesos = ((Nivel1) nivel).getProcesos();
        break;
      case 2:
        procesos = ((Nivel2) nivel).getProcesos();
        break;
      case 3:
        procesos = ((Nivel3) nivel).getProcesos();
        break;
      case 4:
        procesos = ((Nivel4) nivel).getProcesos();
        break;
      case 5:
        procesos = ((Nivel5) nivel).getProcesos();
        break;
    }


    if(!isListSet(procesos)){

      for (int parentNumber = (levelNumber - 1); parentNumber>0; parentNumber--){

        procesos = searchLevelProcess(parentNumber);
        if(isListSet(procesos)){
          break;
        }

      }
    }

    procesoList = new ArrayList<>(procesos);
    updateProcesosStringList();
  }



  private List<Proceso> searchLevelProcess(int levelNumber){
    List<Proceso> procesos = new ArrayList<>();
    String levelKey = String.format(Locale.US,"idNivel%dEmpresa",levelNumber);

    int levelId = (Integer) paramsObserver.get(levelKey);

    switch (levelNumber){
      case 1:
        for(Nivel1 nivel1 : nivel1List){
          if(nivel1.getId() == levelId && !nivel1.getProcesos().isEmpty())
            procesos = nivel1.getProcesos();
        }
        break;
      case 2:
        for(Nivel2 nivel2 : nivel2List){
          if(nivel2.getId() == levelId && !nivel2.getProcesos().isEmpty())
            procesos = nivel2.getProcesos();
        }
        break;
      case 3:
        for(Nivel3 nivel3 : nivel3List){
          if(nivel3.getId() == levelId && !nivel3.getProcesos().isEmpty())
            procesos = nivel3.getProcesos();
        }
        break;
      case 4:
        for(Nivel4 nivel4 : nivel4List){
          if(nivel4.getId() == levelId && !nivel4.getProcesos().isEmpty())
            procesos = nivel4.getProcesos();
        }
        break;

      case 5:
        for(Nivel5 nivel5 : nivel5List){
          if(nivel5.getId() == levelId && !nivel5.getProcesos().isEmpty())
            procesos = nivel5.getProcesos();
        }
        break;
    }



    return procesos;
  }






  private void updateLevelResponsable(Object nivel){

    int levelNumber = getLevelNumber(nivel);
    List<Responsable> responsables = new ArrayList<>();

    switch (levelNumber){
      case 1:
        responsables = ((Nivel1) nivel).getResponsables();
        break;
      case 2:
        responsables = ((Nivel2) nivel).getResponsables();
        break;
      case 3:
        responsables = ((Nivel3) nivel).getResponsables();
        break;
      case 4:
        responsables = ((Nivel4) nivel).getResponsables();
        break;
      case 5:
        responsables = ((Nivel5) nivel).getResponsables();
        break;
    }


    if(!isListSet(responsables)){

      for (int parentNumber = (levelNumber - 1); parentNumber>0; parentNumber--){

        responsables = searchLevelResponsable(parentNumber);
        if(isListSet(responsables)){
          break;
        }

      }
    }

    responsableList = new ArrayList<>(responsables);
  }



  private List<Responsable> searchLevelResponsable(int levelNumber){
    List<Responsable> responsables = new ArrayList<>();
    String levelKey = String.format(Locale.US,"idNivel%dEmpresa",levelNumber);

    int levelId = (Integer) paramsObserver.get(levelKey);

    switch (levelNumber){
      case 1:
        for(Nivel1 nivel1 : nivel1List){
          if(nivel1.getId() == levelId && !nivel1.getResponsables().isEmpty())
            responsables = nivel1.getResponsables();
        }
        break;
      case 2:
        for(Nivel2 nivel2 : nivel2List){
          if(nivel2.getId() == levelId && !nivel2.getResponsables().isEmpty())
            responsables = nivel2.getResponsables();
        }
        break;
      case 3:
        for(Nivel3 nivel3 : nivel3List){
          if(nivel3.getId() == levelId && !nivel3.getResponsables().isEmpty())
            responsables = nivel3.getResponsables();
        }
        break;
      case 4:
        for(Nivel4 nivel4 : nivel4List){
          if(nivel4.getId() == levelId && !nivel4.getResponsables().isEmpty())
            responsables = nivel4.getResponsables();
        }
        break;

      case 5:
        for(Nivel5 nivel5 : nivel5List){
          if(nivel5.getId() == levelId && !nivel5.getResponsables().isEmpty())
            responsables = nivel5.getResponsables();
        }
        break;
    }



    return responsables;
  }





  private void updateProcesosStringList() {
    if(procesoList.isEmpty())
      return;

    estandaresProcesoList.clear();
    nivel7ListStr.clear();
    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (Proceso itemProceso : procesoList) {
      nivel6ListStr.add(itemProceso.getProceso());
      for (EstandaresProceso itemEstandaresProceso : itemProceso.getEstandaresProcesos()) {
        nivel7ListStr.add(itemEstandaresProceso.getEstandares().getEstandar());
        estandaresProcesoList.add(itemEstandaresProceso);
      }
      for (actividadesProceso itemActividadesProceso : itemProceso.getActividadesProcesos()) {
        nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
        actividadesProcesoList.add(itemActividadesProceso);
      }
    }

  }


  private void fillNextLevelLists(Object level){
    int actual = selectedLevel;
    switch (actual){
      case 1:
        Nivel1 nivel1 = (Nivel1) level;
        nivel2List.addAll(nivel1.getNiveles2());
        for(int i = 0; i<nivel1.getNiveles2().size(); i++){
          nivel2ListStr.add(nivel2List.get(i).getNombreNivel());
        }

        break;
      case 2:
        Nivel2 nivel2 = (Nivel2) level;
        nivel3List.addAll(nivel2.getNiveles3());
        for(int i = 0; i<nivel2.getNiveles3().size(); i++){
          nivel3ListStr.add(nivel3List.get(i).getNombreNivel());
        }

        break;
      case 3:
        Nivel3 nivel3 = (Nivel3) level;
        nivel4List.addAll(nivel3.getNiveles4());
        for(int i = 0; i<nivel3.getNiveles4().size(); i++){
          nivel4ListStr.add(nivel4List.get(i).getNombreNivel());
        }

        break;
      case 4:
        Nivel4 nivel4 = (Nivel4) level;
        nivel5List.addAll(nivel4.getNiveles5());
        for(int i = 0; i<nivel4.getNiveles5().size(); i++){
          nivel5ListStr.add(nivel5List.get(i).getNombreNivel());
        }
        break;
    }

  }



  private void updateProcessData(List<Proceso> procesos){

    if(procesos.isEmpty()){
      return;
    }


    procesoList.clear();
    nivel6ListStr.clear();
    nivel7ListStr.clear();
    nivel8ListStr.clear();


    // Populate Procesos
    procesoList.addAll(procesos);
    for(Proceso proceso : procesos){
      nivel6ListStr.add(proceso.getProceso());
    }
  }

  private void updateSubProcessData(Proceso proceso){

    estandaresProcesoList.clear();
    nivel7ListStr.clear();

    for(EstandaresProceso estandares : proceso.getEstandaresProcesos()){
      nivel7ListStr.add(estandares.getEstandares().getEstandar());
      estandaresProcesoList.add(estandares);
    }


    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (actividadesProceso itemActividadesProceso : proceso.getActividadesProcesos()) {
      nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
      actividadesProcesoList.add(itemActividadesProceso);
    }



  }

  private void clearLevelsFrom(int level){

    if(level < 2){
      nivel2List.clear();
      nivel2ListStr.clear();
    }


    if(level < 3){
      nivel3List.clear();
      nivel3ListStr.clear();
    }

    if(level < 4){
      nivel4List.clear();
      nivel4ListStr.clear();
    }

    if(level < 5){
      nivel5List.clear();
      nivel5ListStr.clear();
    }

    if(level < 2){
      currentProceso = null;
      procesoList.clear();
      estandaresProcesoList.clear();
      actividadesProcesoList.clear();
    }


    nivel6ListStr.clear();
    nivel7ListStr.clear();
    nivel8ListStr.clear();

    if(paramsObserver.containsKey("idProcesos")){paramsObserver.remove("idProcesos");}
    if(paramsObserver.containsKey("idActividades")){paramsObserver.remove("idActividades");}
    if(paramsObserver.containsKey("idEstandares")){paramsObserver.remove("idEstandares");}

  }


  // fix
  private int getLevelNumber(Object level){

    if(level instanceof Nivel1)
      return 1;

    if(level instanceof Nivel2)
      return 2;

    if(level instanceof Nivel3)
    if(level instanceof Nivel3)
      return 3;

    if(level instanceof Nivel4)
      return 4;

    if(level instanceof Nivel5)
      return 5;

    if(level instanceof Proceso)
      return 6;

    return -1;
  }

  SpinnerDatePickerDialogBuilder builder;
  private void createDatePickerDialog(){
    Calendar calendar = Calendar.getInstance();

    builder = new SpinnerDatePickerDialogBuilder()
            .context(IngressIntern.this)
            .callback(IngressIntern.this)
            .showTitle(true)
            .defaultDate(calendar.get(Calendar.YEAR), 0, 1)
            .maxDate(calendar.get(Calendar.YEAR) + 1, 0, 1)
            .minDate(calendar.get(Calendar.YEAR) - 1, 0, 1);
  }

  @Override
  public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear + 1 , dayOfMonth);
    try {
      edtDate.setText(formatoDate.format(formatoDate.parse(date)));
    } catch (ParseException e) {
      Timber.e(e);
    }
  }
}
