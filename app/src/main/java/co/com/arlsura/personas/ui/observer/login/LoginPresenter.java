package co.com.arlsura.personas.ui.observer.login;

import com.google.gson.reflect.TypeToken;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.ApiService;
import co.com.arlsura.personas.api.SuraLoginService;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.configuration.Config;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.RequestError;
import co.com.arlsura.personas.model.TipoDocumento;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import co.com.arlsura.personas.util.Constants;
import co.com.arlsura.personas.util.HeaderUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by home on 11/7/17.
 */

public class LoginPresenter {
  private LoginContract contract;
  private ApiService apiService;
  private SuraLoginService suraLoginService;
  private UserDao userDao;

  public LoginPresenter(LoginContract contract, ApiService apiService, UserDao userDao) {
    this.contract = contract;
    this.apiService = apiService;
    this.userDao = userDao;
    Timber.tag("LoginPresenter");
  }

  public LoginPresenter(LoginContract contract, SuraLoginService suraLoginService, UserDao userDao){

    this.contract = contract;
    this.suraLoginService = suraLoginService;
    this.userDao = userDao;
    Timber.tag("LoginPresenter");
  }

  public void loginObserver(Map<String, Object> param) {

    if (Config.isUiTest()) {
      contract.setLoading();
      User user = WebServices.getGson().fromJson(Constants.jsonLoginObserver, User.class);
      userDao.save(user);
      contract.onLoadUser(user);
      contract.hideLoading();
    } else {

      apiService.login(param)
          .doOnSubscribe(disposable -> contract.setLoading())
          .subscribeOn(Schedulers.io())
          .doOnComplete(() -> contract.hideLoading())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(user -> {
            user.document = param.get("username").toString();
            user.documentCompany = param.get("document").toString();

            User old = userDao.findFirst();

            if(old != null)
              userDao.delete(old);

            if (user.isObservador()) {
              userDao.save(user);

              contract.onLoadUser(user);
            } else {
              contract.hideLoading();
              contract.onErr(R.string.usuario_no_autorizado);
            }
          }, throwable -> {
            Timber.e(throwable);

            if (throwable instanceof HttpException) {
              HttpException error = (HttpException) throwable;
              String errorBody = error.response().errorBody().string();
              Timber.e(errorBody);
              RequestError requestError =
                  WebServices.getGson().fromJson(errorBody, RequestError.class);

              if(requestError.getStatus().equals("NO")){
                contract.onErr("No tienes acceso a esta empresa como observador, comunícate con el administrador o ingresa como trabajador");
              }else  if(requestError.getDebugMessage() == null)
                contract.onErr(requestError.getMessage());
              else
                contract.onErr(requestError.getDebugMessage());
            } else {
              contract.onErr(R.string.ha_ocurrindo_un_err);
            }

            contract.hideLoading();
          }, () -> contract.hideLoading());
    }
  }


  public void loginWorker(Map<String, Object> param) {


    if (Config.isUiTest()) {
      contract.setLoading();
      boolean isIntern = param.get("username").toString().equalsIgnoreCase("1152198279");


      User user = WebServices.getGson()
          .fromJson(isIntern ? Constants.jsonLoginWorker : Constants.jsonLoginWorkerExterno,
              User.class);

      if(!isSameUser(user)){
        clearWorkObservations(user);
      }

      user.document = param.get("username").toString();
      user.documentCompany = param.get("document").toString();
      userDao.save(user);

      contract.onLoadUser(user);
      contract.hideLoading();
    } else {

      apiService.loginWorker(param)
          .doOnSubscribe(disposable -> contract.setLoading())
          .subscribeOn(Schedulers.io())
          .doOnComplete(() -> contract.hideLoading())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(user -> {

            if(!isSameUser(user)){
              clearWorkObservations(user);
            }

            user.document = param.get("username").toString();
            user.documentCompany = param.get("document").toString();

            UserDao.token = user.token;

            userDao.save(user);
            contract.onLoadUser(user);
          }, throwable -> {
            Timber.e(throwable);
            if (throwable instanceof HttpException) {
              HttpException error = (HttpException) throwable;
              String errorBody = error.response().errorBody().string();
              Timber.e(errorBody);
              RequestError requestError =
                  WebServices.getGson().fromJson(errorBody, RequestError.class);

              if(requestError.getStatus().equals("NO")){
                contract.onErr("No tienes acceso a esta empresa como observador, comunícate con el administrador o ingresa como trabajador");
              }else  if(requestError.getDebugMessage() == null)
                contract.onErr(requestError.getMessage());
              else
                contract.onErr(requestError.getDebugMessage());

            } else {
              contract.onErr(R.string.ha_ocurrindo_un_err);
            }
            contract.hideLoading();
          }, () -> contract.hideLoading());
    }
  }

  private boolean isSameUser(User user){

    User userdb = userDao.findFirst();

    if( userdb != null && user != null)
      return (userdb.document.equals(user.document));

    return false;
  }
  public void logOut() {

    UserDao dao = new UserDao();
    User user = dao.findFirst();
    if (user != null) {
      String token = user.token;
      contract.setLoading();
      if (Config.isUiTest()) {
        closeSession();
        if(userDao.findFirst() != null)
          userDao.delete(user);

        contract.onSuccesLogOut();
        contract.hideLoading();
      } else {
        apiService.logOut(HeaderUtil.getHeader(token))
            .doOnSubscribe(disposable -> contract.setLoading())
            .subscribeOn(Schedulers.io())
            .doOnComplete(() -> contract.hideLoading())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(u -> {
              userDao.delete(user);
              contract.onSuccesLogOut();
              contract.hideLoading();
            }, throwable -> {
              Timber.e(throwable);

              if (throwable instanceof HttpException) {
                HttpException error = (HttpException) throwable;
                String errorBody = error.response().errorBody().string();
                Timber.e(errorBody);
                RequestError requestError =
                    WebServices.getGson().fromJson(errorBody, RequestError.class);
                contract.onErr(requestError.getDebugMessage());





              } else {
                contract.onErr(R.string.ha_ocurrindo_un_err);
              }

              contract.hideLoading();
            }, () -> contract.hideLoading());
      }
    }
  }

  private void clearWorkObservations(User user){
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.commitTransaction();
  }


  private void clearDatabase(){
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.deleteAll();
    realm.commitTransaction();

  }
  private void closeSession() {
    User user = userDao.findFirst();
    if (user != null) {
      userDao.delete(user);
    }
  }

  public List<Respuesta> findRespuestasById(String id) {
    PreguntasDao dao = new PreguntasDao();
    return dao.findById(id);
  }

  public String getTipos(){
    String tiposDocumento =
            "[\n" +
                    "    {\"value\":\"C\",\"cdTipoDocumentoGobierno\":\"CC\",\"text\":\"Cédula de ciudadanía\",\"minimo\":3,\"maximo\":11,\"patron\":\"^[0-9]{3,11}$\",\"type\":\"numeric\",\"minAge\":18,\"maxAge\":120},\n" +
                    "\t{\"value\":\"N\",\"cdTipoDocumentoGobierno\":\"NI\",\"text\":\"NIT\",\"minimo\":8,\"maximo\":15,\"patron\":\"^[0-9]{8,15}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "    {\"value\":\"E\",\"cdTipoDocumentoGobierno\":\"CE\",\"text\":\"Cédula de extranjería\",\"minimo\":3,\"maximo\":6,\"patron\":\"^[0-9]{3,6}$\",\"type\":\"alphanumeric\",\"minAge\":18,\"maxAge\":120},\n" +
                    "\t{\"value\":\"T\",\"cdTipoDocumentoGobierno\":\"TI\",\"text\":\"Tarjeta de identidad\",\"minimo\":8,\"maximo\":11,\"patron\":\"^[0-9]{8,11}$\",\"type\":\"numeric\",\"minAge\":7,\"maxAge\":19},\n" +
                    "    {\"value\":\"D\",\"cdTipoDocumentoGobierno\":\"CD\",\"text\":\"Carné diplomático\",\"minimo\":3,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{3,11}$\",\"type\":\"alphanumeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "\t{\"value\":\"U\",\"cdTipoDocumentoGobierno\":\"NU\",\"text\":\"N.U.I.P\",\"minimo\":9,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{9,11}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "\t{\"value\":\"P\",\"cdTipoDocumentoGobierno\":\"PA\",\"text\":\"Pasaporte\",\"minimo\":6,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{6,11}$\",\"type\":\"alphanumeric\",\"minAge\":0,\"maxAge\":18},\n" +
                    "\t{\"value\":\"Z\",\"cdTipoDocumentoGobierno\":\"PE\",\"text\":\"Permiso especial\",\"minimo\":3,\"maximo\":16,\"patron\":\"^[0-9]{3,16}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "    {\"value\":\"I\",\"cdTipoDocumentoGobierno\":\"RC\",\"text\":\"Registro civil\",\"minimo\":10,\"maximo\":11,\"patron\":\"^[0-9]{10,11}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":18},\n" +
                    "    {\"value\":\"S\",\"cdTipoDocumentoGobierno\":\"SC\",\"text\":\"Salvoconducto de permanencia\",\"minimo\":9,\"maximo\":9,\"patron\":\"^[0-9]{9}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120}\n" +
                    "]";

    return tiposDocumento;
  }
  public List<String> getTiposArray(){
    String[] tipos = new String[]{"Cédula de ciudadanía","NIT","Cédula de extranjería","Tarjeta de identidad",
            "Carné diplomático","N.U.I.P","Pasaporte",
            "Permiso especial","Registro civil","Salvoconducto de permanencia"};

    ArrayList<String> tiposArray =  new ArrayList<>();
    for (String tipo: tipos
         ) {
      tiposArray.add(tipo);
    }

    return tiposArray;
  }
  public List<TipoDocumento> getTiposDocumentos() {

    Type listType = new TypeToken<ArrayList<TipoDocumento>>() {
    }.getType();

    String tiposDocumento =
            "[\n" +
                    "    {\"value\":\"C\",\"cdTipoDocumentoGobierno\":\"CC\",\"text\":\"Cédula de ciudadanía\",\"minimo\":3,\"maximo\":11,\"patron\":\"^[0-9]{3,11}$\",\"type\":\"numeric\",\"minAge\":18,\"maxAge\":120},\n" +
                    "\t{\"value\":\"N\",\"cdTipoDocumentoGobierno\":\"NI\",\"text\":\"NIT\",\"minimo\":8,\"maximo\":15,\"patron\":\"^[0-9]{8,15}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "    {\"value\":\"E\",\"cdTipoDocumentoGobierno\":\"CE\",\"text\":\"Cédula de extranjería\",\"minimo\":3,\"maximo\":6,\"patron\":\"^[0-9]{3,6}$\",\"type\":\"alphanumeric\",\"minAge\":18,\"maxAge\":120},\n" +
                    "\t{\"value\":\"T\",\"cdTipoDocumentoGobierno\":\"TI\",\"text\":\"Tarjeta de identidad\",\"minimo\":8,\"maximo\":11,\"patron\":\"^[0-9]{8,11}$\",\"type\":\"numeric\",\"minAge\":7,\"maxAge\":19},\n" +
                    "    {\"value\":\"D\",\"cdTipoDocumentoGobierno\":\"CD\",\"text\":\"Carné diplomático\",\"minimo\":3,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{3,11}$\",\"type\":\"alphanumeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "\t{\"value\":\"U\",\"cdTipoDocumentoGobierno\":\"NU\",\"text\":\"N.U.I.P\",\"minimo\":9,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{9,11}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "\t{\"value\":\"P\",\"cdTipoDocumentoGobierno\":\"PA\",\"text\":\"Pasaporte\",\"minimo\":6,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{6,11}$\",\"type\":\"alphanumeric\",\"minAge\":0,\"maxAge\":18},\n" +
                    "\t{\"value\":\"Z\",\"cdTipoDocumentoGobierno\":\"PE\",\"text\":\"Permiso especial\",\"minimo\":3,\"maximo\":16,\"patron\":\"^[0-9]{3,16}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "    {\"value\":\"I\",\"cdTipoDocumentoGobierno\":\"RC\",\"text\":\"Registro civil\",\"minimo\":10,\"maximo\":11,\"patron\":\"^[0-9]{10,11}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":18},\n" +
                    "    {\"value\":\"S\",\"cdTipoDocumentoGobierno\":\"SC\",\"text\":\"Salvoconducto de permanencia\",\"minimo\":9,\"maximo\":9,\"patron\":\"^[0-9]{9}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120}\n" +
                    "]";

    Timber.i(tiposDocumento);

    Timber.i(Constants.tiposDocumento);
    List<TipoDocumento> listaDocumentos = WebServices.getGson().fromJson(tiposDocumento, listType);
    for ( TipoDocumento documento: listaDocumentos) {
      try {
        Timber.i(documento.toString());

      }catch (Exception e){
        Timber.i("DOCUMENTO NULL");

      }

    }

    return listaDocumentos;
  }



}
