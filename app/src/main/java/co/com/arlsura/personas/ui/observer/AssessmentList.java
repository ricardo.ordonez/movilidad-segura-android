package co.com.arlsura.personas.ui.observer;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.ComportamientosProcesos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.StatusComportamiento;
import co.com.arlsura.personas.ui.observer.assesment.AssesmentContract;
import co.com.arlsura.personas.ui.observer.assesment.AssesmentListAdapter;
import co.com.arlsura.personas.ui.observer.assesment.FragmentStatusAsessment;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataContract;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import timber.log.Timber;

public class AssessmentList extends BaseActivity implements BasicDataContract, AssesmentContract {
  public static Map<String, Object> paramsObserver = new HashMap<>();
  public static Proceso procesoRoot;
  public static List<Comportamientos> list;
  @BindView(R.id.toolbarValoracionComportamientos) Toolbar toolbarValoracionComportamientos;
  @BindView(R.id.listAssesment) RecyclerView listAssesment;
  private AssesmentListAdapter adapter;
  private BasicDataPresenter presenter;
  public static AssesmentContract listenerAssesment;

  @Override public int getLayoutId() {
    return R.layout.activity_assessment_list;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbarValoracionComportamientos);
    toolbarValoracionComportamientos.setNavigationOnClickListener(view -> finish());
    initUi();
    initPresenter();
  }

  @Override protected void onResume() {
    super.onResume();
    if (procesoRoot != null) presenter.loadAssesmetListById(procesoRoot.getId());
  }

  private void initPresenter() {
    presenter = new BasicDataPresenter(WebServices.getApi(), new ProcesoDao(), this);
    if (procesoRoot == null) {
      presenter.loadAssesmetList();
    } else {
      List<Comportamientos> listOut = new ArrayList<>();
      for (ComportamientosProcesos item : procesoRoot.getComportamientosProcesos()) {
        listOut.add(item.getComportamientos());
      }
      onLoad(listOut);
    }
  }

  private void initUi() {
    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    listAssesment.setLayoutManager(mLayoutManager);
  }

  /*
  *   func checkAllItems(data: List<Comportamientos>) -> Bool {
    var out = false
    var conutOk: Int = 0;

    data.forEach { (item) in
      //Log.Log(msg: "status item \(item.comportamientos?.estado) \(item.id)")
      out = item.comportamientos?.estado == TypeState.Succes.rawValue
      if(out){
        conutOk = conutOk + 1
      }
      if(!out){
        return
      }
    }
    Log.Log(msg: "data size \(data.count) countOK \(conutOk)")
    return conutOk == data.count
  }
  * */

  private boolean checkAllItems(List<Comportamientos> list) {
    boolean out;
    int coutOk = 0;
    for (Comportamientos item : list) {
      Timber.e("EL ESTADO ES: " + item.getEstado());
      out = (item.getEstado() != null) && item.getEstado().equalsIgnoreCase(StatusComportamiento.Succes.toString());
      if (out) {
        coutOk++;
      }
      if (!out) {
        break;
      }
    }
    return coutOk == list.size();
  }

  @Override public void setLoading() {
    Timber.d("setLoading");
    showLoading(getString(R.string.loading));
  }

  @Override public void hideLoading() {
    Timber.d("hideLoading");
    dissmisLoading();
  }

  @Override public void onErr(String err) {
    Timber.d("onErr");
    showAlertDialog(err);
  }

  @Override public void onErr(int err) {
    Timber.d(getString(err));
    showAlertDialog(err);
  }

  @Override public void onLoadNiveles(List<Nivel1> list) {

  }

  @Override public void onLoad(List<Comportamientos> list) {
    AssessmentList.list = list;
    boolean isOk = checkAllItems(AssessmentList.list);
    FragmentStatusAsessment.isOK = isOk;
    if (listenerAssesment != null) {
      Timber.e("onLoad isOK ".concat(String.valueOf(isOk)));
      listenerAssesment.onListOk(isOk);
    }
    adapter = new AssesmentListAdapter(this, list);
    adapter.getPositionClicks().subscribe(position -> {
      Timber.d("adapter click on ".concat(String.valueOf(position)));
      AssesmentDetail.list = AssessmentList.list;
      AssesmentDetail.currentPosition = position;
      AssesmentDetail.listenerAssesment = this;
      goActv(AssesmentDetail.class, false);
    });
    runOnUiThread(() -> listAssesment.setAdapter(adapter));
  }

  @Override public void onLoadRootProceso(Proceso procesoRoot) {
    AssessmentList.procesoRoot = procesoRoot;
    FragmentStatusAsessment.procesoRootId = AssessmentList.procesoRoot.getId();
  }

  @Override public void onItemChange(Comportamientos item) {
    Timber.e("onItemChange List");
    presenter.loadAssesmetListById(procesoRoot.getId());

    /*
    Comportamientos currentItem = list.get(item.getPosition());

    Timber.e("onItemChange BeforeUpdate ".concat(String.valueOf(item.getPosition())));
    Timber.e("onItemChange Position ".concat(currentItem.getValoracion()));
    Realm realm = Realm.getDefaultInstance();
    realm.executeTransaction(realm1 -> {

    });
    currentItem = item;
    realm.beginTransaction();
    Timber.e("onItemChange Update Item ".concat(currentItem.getValoracion()));
    */
  }

  @Override public void onListOk(boolean isOk) {

  }
}
