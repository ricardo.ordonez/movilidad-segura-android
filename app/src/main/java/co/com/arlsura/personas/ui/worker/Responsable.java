package co.com.arlsura.personas.ui.worker;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.dao.WorkerObserverDao;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.view.CustomSpinner;
import io.realm.Realm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import timber.log.Timber;

public class Responsable extends BaseActivity {
  public static Map<String, Object> paramsObserver = new HashMap<>();
  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.edtResponsable) CustomSpinner edtResponsable;
  @BindView(R.id.btnSiguiente) Button btnSiguiente;
  private List<co.com.arlsura.personas.model.niveles.Responsable> responsableList =
      new ArrayList<>();
  private List<String> responsableListStr = new ArrayList<>();

  @Override public int getLayoutId() {
    return R.layout.activity_responsable;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(view -> finish());
    isEnabled(btnSiguiente, false);
    initUi();
  }

  private void initUi() {
    edtResponsable.setOnItemSelectedListener(onItemSelectedListener);
    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    WorkerObservation observation = workerObserverDao.findFirstPending();
    if (observation != null && !observation.getResponsables().isEmpty()) {
      responsableList.clear();
      responsableListStr.clear();
      for (co.com.arlsura.personas.model.niveles.Responsable item : observation.getResponsables()) {
        responsableList.add(item);
        responsableListStr.add(item.getNombre());
      }
      edtResponsable.initializeStringValues(responsableListStr,
          edtResponsable.getPrompt().toString());
    }
  }

  private CustomSpinner.OnItemSelectedListener onItemSelectedListener =
      new CustomSpinner.OnItemSelectedListener() {
        @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        }

        @Override public void onNothingSelected(AdapterView<?> adapterView) {

        }

        @Override
        public void onAfterItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
          int fixedPosition = i;

          switch (adapterView.getId()) {
            case R.id.edtResponsable:

              if (!responsableList.isEmpty() && showNext(responsableList.get(fixedPosition).getNombre(),
                      edtResponsable.getPrompt())) {

                Timber.e("onItemSelected ".concat(responsableList.get(fixedPosition).getNombre()));
                isEnabled(btnSiguiente, true);
                paramsObserver.put("responsable", responsableList.get(fixedPosition).getCedula());
              }
              break;
          }
        }
      };

  @OnClick(R.id.btnSiguiente) public void onViewClicked() {
    String json = WebServices.getGson().toJson(paramsObserver);
    Timber.e("json Params ".concat(json));

    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    WorkerObservation observation = workerObserverDao.findFirstPending();
    String token = new UserDao().findFirst().token;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    observation.setParams(json);
    observation.setToken(token);
    observation.setStatus(StatusSync.PendingSync.toString());
    realm.commitTransaction();
    realm.close();

    workerObserverDao.save(observation);

    Status.paramsObserver = paramsObserver;
    goActv(Status.class, false);
  }
}
