package co.com.arlsura.personas.api;

import androidx.annotation.NonNull;
import co.com.arlsura.personas.model.Area;
import co.com.arlsura.personas.model.Historial;
import co.com.arlsura.personas.model.ResponseSync;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import co.com.arlsura.personas.model.niveles.RootNivel1;
import io.reactivex.Observable;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by home on 11/7/17.
 */

public interface ApiService {
  @NonNull @POST("services/auth") Observable<User> login(@Body Map<String, Object> param);

  @NonNull @POST("services/authtrabajador") Observable<User> loginWorker(
      @Body Map<String, Object> param);

  @NonNull @GET("services/preguntas") Observable<List<Preguntas>> preguntas();

  @NonNull @GET("services/valoraciones") Observable<List<Historial>> historial(
      @HeaderMap Map<String, String> headers);

  @NonNull @GET("services/niveles") Observable<RootNivel1> niveles(@Query("empresa") String empresa,
      @HeaderMap Map<String, String> headers);

  @Nonnull @POST("services/valoraciones") Observable<ResponseSync>
  syncValoraciones(
      @Body Map<String, Object> param, @HeaderMap Map<String, String> headers);

  @Nonnull @POST("services/valoraciones/trabajador") Observable<ResponseSync> syncWorker(
      @Body Map<String, Object> param, @HeaderMap Map<String, String> headers);

  @NonNull @GET("services/areas") Observable<List<Area>> areas(@Query("empresa") String empresa,
      @HeaderMap Map<String, String> headers);

  // TODO Change the service path to remove the token
  @Nonnull @GET("services/auth/logout") Observable<ResponseBody> logOut(@HeaderMap Map<String, String> headers);
}
