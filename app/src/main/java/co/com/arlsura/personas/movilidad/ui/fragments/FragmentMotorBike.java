package co.com.arlsura.personas.movilidad.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import co.com.arlsura.personas.movilidad.ui.VehicleMovilidad;
import co.com.arlsura.personas.R;
import timber.log.Timber;


public class FragmentMotorBike extends Fragment implements com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private SpinnerDatePickerDialogBuilder builder;


    private EditText etFechaMatricula, etFechaSoat, etFechaVenSeguro, etFechaCambioAceite, etFechaVenTenomecanico;
    private EditText etPlaca, etMarca, etModelo, etLinea, etServicio;
    private Button btnGuardarVehiculo;
    private int opcionFecha = 0;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog dateTimeDialog;
    private SimpleDateFormat formatoDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public FragmentMotorBike() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_motor_bike, container, false);

        return initUi(view);
    }

    private View initUi(View view) {

        etFechaMatricula = view.findViewById(R.id.etFechaMatricula);
        etFechaSoat = view.findViewById(R.id.etFechaSoat);
        etFechaVenSeguro = view.findViewById(R.id.etFechaVenSeguro);
        etFechaCambioAceite = view.findViewById(R.id.etFechaCambioAceite);
        etFechaVenTenomecanico = view.findViewById(R.id.etFechaVenTenomecanico);
        etPlaca = view.findViewById(R.id.etPlaca);
        etMarca = view.findViewById(R.id.etMarca);
        etModelo = view.findViewById(R.id.etModelo);
        etLinea = view.findViewById(R.id.etLinea);
        etServicio = view.findViewById(R.id.etServicio);

        btnGuardarVehiculo = view.findViewById(R.id.btnGuardarVehiculo);

        etFechaMatricula.setOnClickListener(this);
        etFechaSoat.setOnClickListener(this);
        etFechaVenSeguro.setOnClickListener(this);
        etFechaCambioAceite.setOnClickListener(this);
        etFechaVenTenomecanico.setOnClickListener(this);
        btnGuardarVehiculo.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        opcionFecha = view.getId();
        switch (opcionFecha) {
            case R.id.etFechaMatricula:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaSoat:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaVenSeguro:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaCambioAceite:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaVenTenomecanico:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.btnGuardarVehiculo:
                if (validateFields()) {
                    Intent vehicleMovilidad = new Intent(getContext(), VehicleMovilidad.class);
                    vehicleMovilidad.putExtra("llave", "FragmentVehicle");
                    startActivity(vehicleMovilidad);
                }
                break;
        }
    }

    private boolean validateFields() {

        boolean validacion = true;

        if (!(!etPlaca.getText().toString().trim().isEmpty() && etPlaca.getText().toString().length() == 6)) {
            validacion = false;
            etPlaca.setError("La placa no se ha diligenciado correctamente");
        }
        if (!(!etMarca.getText().toString().trim().isEmpty() && etMarca.getText().toString().length() >= 3)) {
            validacion = false;
            etMarca.setError("La marca no se ha diligenciado correctamente");
        }
        if (!(!etModelo.getText().toString().trim().isEmpty() && etModelo.getText().toString().length() >= 4)) {
            validacion = false;
            etModelo.setError("El modelo no se ha diligenciado correctamente");
        }
        if (!(!etLinea.getText().toString().trim().isEmpty() && etLinea.getText().toString().length() >= 1)) {
            validacion = false;
            etLinea.setError("La linea no se ha diligenciado correctamente");
        }
        if (!(!etServicio.getText().toString().trim().isEmpty() && etServicio.getText().toString().length() >= 7)) {
            validacion = false;
            etServicio.setError("el servicio no se ha diligenciado correctamente");
        }
        if (!(!etFechaMatricula.getText().toString().trim().isEmpty() && etFechaMatricula.getText().toString().length() == 10)) {
            validacion = false;
            etFechaMatricula.setError("La fecha de matricula no se ha diligenciado correctamente");
        }
        if (!(!etFechaSoat.getText().toString().trim().isEmpty() && etFechaSoat.getText().toString().length() == 10)) {
            validacion = false;
            etFechaSoat.setError("La fecha de vencimiento del soat no se ha diligenciado correctamente");
        }
        if (!(!etFechaVenSeguro.getText().toString().trim().isEmpty() && etFechaVenSeguro.getText().toString().length() == 10)) {
            validacion = false;
            etFechaVenSeguro.setError("La fecha de vencimiento del seguro no se ha diligenciado correctamente");
        }
        if (!(!etFechaCambioAceite.getText().toString().trim().isEmpty() && etFechaCambioAceite.getText().toString().length() == 10)) {
            validacion = false;
            etFechaCambioAceite.setError("La fecha de cambio de aceite no se ha diligenciado correctamente");
        }
        if (!(!etFechaVenTenomecanico.getText().toString().trim().isEmpty() && etFechaVenTenomecanico.getText().toString().length() == 10)) {
            validacion = false;
            etFechaVenTenomecanico.setError("La fecha de revisión técnomecanica del no se ha diligenciado correctamente");
        }

        if (validacion) {
            return validacion;
        } else {
            return validacion;
        }
    }

    private void createDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        builder = new SpinnerDatePickerDialogBuilder()
                .context(getContext())
                .callback(FragmentMotorBike.this)
                .showTitle(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .maxDate(calendar.get(Calendar.YEAR) + 1, 0, 1)
                .minDate(calendar.get(Calendar.YEAR) - 1, 0, 1);
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener =
            (view, year, monthOfYear, dayOfMonth) -> {
                String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear, dayOfMonth);
                try {
                    optionChangeDate(formatoDate.format(formatoDate.parse(date)));
                } catch (ParseException e) {
                    Timber.e(e);
                }
            };




    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear + 1, dayOfMonth);
        try {
            optionChangeDate(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
            Timber.e(e);
        }
    }

    private void optionChangeDate(String formatoDate) {
        switch (opcionFecha) {
            case R.id.etFechaMatricula:
                etFechaMatricula.setText(formatoDate);
                break;

            case R.id.etFechaSoat:
                etFechaSoat.setText(formatoDate);
                break;

            case R.id.etFechaVenSeguro:
                etFechaVenSeguro.setText(formatoDate);
                break;

            case R.id.etFechaCambioAceite:
                etFechaCambioAceite.setText(formatoDate);
                break;

            case R.id.etFechaVenTenomecanico:
                etFechaVenTenomecanico.setText(formatoDate);
                break;
        }
    }


}
