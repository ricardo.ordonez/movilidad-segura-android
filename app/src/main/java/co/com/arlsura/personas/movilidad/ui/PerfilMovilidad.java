package co.com.arlsura.personas.movilidad.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import co.com.arlsura.personas.R;

public class PerfilMovilidad extends AppCompatActivity implements View.OnClickListener {

    Button btnAceptar;
    ImageView arrowBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_perfil_movilidad);
        setContentView(R.layout.fragment_show_data_perfil_movilidad);

        initUi();
    }

    private void initUi() {
        btnAceptar = (Button) findViewById(R.id.btnAceptar);
        arrowBack = (ImageView) findViewById(R.id.arrowBack);

        btnAceptar.setOnClickListener(this);
        arrowBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAceptar:
                Intent vehiculoIntent = new Intent(PerfilMovilidad.this, VehicleMovilidad.class);
                startActivity(vehiculoIntent);
                break;
            case R.id.arrowBack:
                Intent intent = new Intent(PerfilMovilidad.this, VehicleMovilidad.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent vehiculoIntent = new Intent(PerfilMovilidad.this, VehicleMovilidad.class);
        startActivity(vehiculoIntent);
    }
}
