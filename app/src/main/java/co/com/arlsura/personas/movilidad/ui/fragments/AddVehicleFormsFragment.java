package co.com.arlsura.personas.movilidad.ui.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;


public class AddVehicleFormsFragment extends BaseFragment {


    @BindView(R.id.vpAddForms) ViewPager vpAddForms;
    @BindView(R.id.tabsFroms) TabLayout tabsForms;

    public AddVehicleFormsFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_vehicle_forms;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vpAddForms.setAdapter(new FormFragmentsAdapter(getChildFragmentManager()));

        tabsForms.setTabMode(TabLayout.MODE_FIXED);
        tabsForms.setupWithViewPager(vpAddForms);
        createCustomTabs();
    }



    private void createCustomTabs(){
        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tabview,null);
        ImageView tabOneImage = tabOne.findViewById(R.id.img_tab);
        TextView tabOneText = tabOne.findViewById(R.id.tv_tab);

        tabOneImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.icon_carro_o,getActivity().getTheme()));
        tabOneText.setText("Automovil");

        tabsForms.getTabAt(0).setCustomView(tabOne);

        // Second tab

        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tabview,null);
        ImageView tabTwoImage = tabTwo.findViewById(R.id.img_tab);
        TextView tabTwoText = tabTwo.findViewById(R.id.tv_tab) ;

        tabTwoImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.icon_moto,getActivity().getTheme()));
        tabTwoText.setText("Motocicleta");

        tabsForms.getTabAt(1).setCustomView(tabTwo);

        // Third tab

        LinearLayout tabThree = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tabview,null);
        ImageView tabThreeImage = tabThree.findViewById(R.id.img_tab);
        TextView tabThreeText = tabThree.findViewById(R.id.tv_tab);

        tabThreeImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.icon_bici,getActivity().getTheme()));
        tabThreeText.setText("Bicicleta");

        tabsForms.getTabAt(2).setCustomView(tabThree);

    }
    public static class FormFragmentsAdapter extends FragmentPagerAdapter{


        public FormFragmentsAdapter(FragmentManager fragmentManager){
            super(fragmentManager);

        }
        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    return new FragmentVehicle();
                case 1:
                    return new FragmentMotorBike();
                case 2:
                    return new FragmentBicycle();
                default:
                    return null;

            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Automovil";
                case 1:
                    return "Motocicleta";
                case 2:
                    return "Bicicleta";
            }

            return null;
        }

    }
}
