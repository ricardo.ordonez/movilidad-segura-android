package co.com.arlsura.personas.movilidad.view;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import co.com.arlsura.personas.movilidad.ui.CreateVehicleEnterprice;
import co.com.arlsura.personas.R;

/**
 * Created by xkiRox on 17/07/18.
 */
public class EnterpriceCustomDialog extends DialogFragment {

    private LayoutInflater inflater;
    private View customView;
    private Button btnComenzar;
    private TextView tvPlacas;

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        inflater = getActivity().getLayoutInflater();
        customView = inflater.inflate(R.layout.custom_dialog_enterprice_movilidad, null);
        btnComenzar = customView.findViewById(R.id.btnComezar);
        tvPlacas = customView.findViewById(R.id.tvPlacas);

        tvPlacas.setText("\nABDC123 \nBCD234 \nCDE345 ");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        btnComenzar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("LoginCustomDialog", "Iniciando vehiculos");
                Intent vehiculoIntent = new Intent(customView.getContext(), CreateVehicleEnterprice.class);
                startActivity(vehiculoIntent);
            }
        });

        builder.setView(customView);

        return builder.create();
    }
}
