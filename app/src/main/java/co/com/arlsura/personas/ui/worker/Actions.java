package co.com.arlsura.personas.ui.worker;

import android.os.Bundle;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.WorkerObserverDao;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.util.Constants;
import co.com.arlsura.personas.view.CustomSpinner;
import com.jakewharton.rxbinding2.widget.RxTextView;
import io.realm.Realm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import timber.log.Timber;

public class Actions extends BaseActivity {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.lblEstadoAccion) TextView lblEstadoAccion;
  @BindView(R.id.edtEstado) CustomSpinner edtEstado;
  @BindView(R.id.lblRealizasteObservacion) TextView lblRealizasteObservacion;
  @BindView(R.id.radioNO) AppCompatRadioButton radioNO;
  @BindView(R.id.lblNo) TextView lblNo;
  @BindView(R.id.radioMejora) AppCompatRadioButton radioMejora;
  @BindView(R.id.lblOportunidadMejora) TextView lblOportunidadMejora;
  @BindView(R.id.radioFelicitaciones) AppCompatRadioButton radioFelicitaciones;
  @BindView(R.id.lblFelicitaciones) TextView lblFelicitaciones;
  @BindView(R.id.lblAcuerdo) TextView lblAcuerdo;
  @BindView(R.id.edtCual) EditText edtCual;
  @BindView(R.id.btnSiguiente) Button btnSiguiente;

  public static Map<String, Object> paramsObserver = new HashMap<>();
  private boolean isTipoReporteObligatorio = true;
  private int selectedCheck = -1;

  private List<String> estadoList = new ArrayList<>();

  @Override public int getLayoutId() {
    return R.layout.activity_actions;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(view -> finish());
    isEnabled(btnSiguiente, false);

    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    WorkerObservation observation = workerObserverDao.findFirstPending();


    lblAcuerdo.setVisibility(View.INVISIBLE);
    edtCual.setVisibility(View.INVISIBLE);
    initUi();
  }

  private boolean checkEstadoSpinner(){

    if((edtEstado.getSelectedView()) != null){
      if(!((TextView)edtEstado.getSelectedView()).getText().equals(edtEstado.getPrompt()))
        return true;
    }
    return false;

  }

  private boolean checkEstado() {
    return isTipoReporteObligatorio && checkEstadoSpinner()
        || !isTipoReporteObligatorio;
  }

  private boolean validateChecks() {
    return selectedCheck != -1;
  }

  private boolean checkCual() {
    return radioMejora.isChecked() && (edtCual.getText().toString().length() > 0)
        || !radioMejora.isChecked();
  }

  private boolean checkFields() {
    return checkEstado() && validateChecks() && checkCual();
  }

  private void initUi() {

    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    estadoList.addAll(Constants.estadoList);
    edtEstado.setOnItemSelectedListener(onItemSelectedListener);
    edtEstado.initializeStringValues(estadoList, edtEstado.getPrompt().toString());

    RxTextView.textChangeEvents(edtCual).subscribe(text -> {
      Timber.e("edtCual ".concat(text.text().toString()));
      if (text.text().toString().length() > 0) {
        paramsObserver.put("acuerdos", text.text().toString());
      }
      isEnabled(btnSiguiente, checkFields());
    });
  }


  private CustomSpinner.OnItemSelectedListener onItemSelectedListener =
          new CustomSpinner.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override public void onNothingSelected(AdapterView<?> adapterView) {

            }

            @Override
            public void onAfterItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              switch (adapterView.getId()) {
                case R.id.edtEstado:
                  if (!estadoList.isEmpty() && showNext(edtEstado.getSelectedOptionString(),
                          edtEstado.getPrompt())) {
                    Timber.e("onItemSelected ".concat(estadoList.get(i)));
                    paramsObserver.put("estado", edtEstado.getSelectedOptionString());
                    isEnabled(btnSiguiente, checkFields());
                  }
                  break;
              }
            }

          };

  @OnClick({ R.id.radioNO, R.id.radioMejora, R.id.radioFelicitaciones, R.id.btnSiguiente })
  public void onViewClicked(View view) {
    switch (view.getId()) {
      case R.id.radioNO:
        selectedCheck = 0;
        radioNO.setChecked(true);
        radioMejora.setChecked(false);
        radioFelicitaciones.setChecked(false);
        paramsObserver.put("accionRealizada", "No");
        lblAcuerdo.setVisibility(View.INVISIBLE);
        edtCual.setVisibility(View.INVISIBLE);
        isEnabled(btnSiguiente, checkFields());
        break;
      case R.id.radioMejora:
        selectedCheck = 1;
        radioNO.setChecked(false);
        radioMejora.setChecked(true);
        radioFelicitaciones.setChecked(false);
        paramsObserver.put("accionRealizada", "Si, un dialogo con oportunidad de mejora");

        lblAcuerdo.setVisibility(View.VISIBLE);
        edtCual.setVisibility(View.VISIBLE);
        isEnabled(btnSiguiente, checkFields());
        break;
      case R.id.radioFelicitaciones:
        selectedCheck = 2;
        radioNO.setChecked(false);
        radioMejora.setChecked(false);
        radioFelicitaciones.setChecked(true);
        paramsObserver.put("accionRealizada", "Si, di las felicitaciones por un buen trabajo");
        lblAcuerdo.setVisibility(View.INVISIBLE);
        edtCual.setVisibility(View.INVISIBLE);
        isEnabled(btnSiguiente, checkFields());
        break;
      case R.id.btnSiguiente:
        if (edtCual.getText().toString().length() == 0) {
          paramsObserver.put("acuerdos", "");
        }
        String json = WebServices.getGson().toJson(paramsObserver);
        Timber.e("json Params ".concat(json));

        WorkerObserverDao workerObserverDao = new WorkerObserverDao();
        WorkerObservation observation = workerObserverDao.findFirstPending();
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        observation.setParams(json);
        observation.setStatus(StatusSync.Pending.toString());
        realm.commitTransaction();
        realm.close();

        workerObserverDao.save(observation);

        Responsable.paramsObserver = paramsObserver;
        goActv(Responsable.class, false);
        break;
    }
  }
}
