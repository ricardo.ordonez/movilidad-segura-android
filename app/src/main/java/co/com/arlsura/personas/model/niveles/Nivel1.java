package co.com.arlsura.personas.model.niveles;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class Nivel1 extends RealmObject {

  /**
   * id : 1
   * nombreNivel : PROCESOS
   * estado : Activo
   * nitEmpresa : 800256161
   * procesos : []
   * niveles4 : []
   * responsables : []
   */
  @PrimaryKey private int id;
  private String nombreNivel;
  private String estado;
  private String nitEmpresa;
  private RealmList<Proceso> procesos;
  private RealmList<Nivel2> niveles2;
  private RealmList<Responsable> responsables;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNombreNivel() {
    return nombreNivel;
  }

  public void setNombreNivel(String nombreNivel) {
    this.nombreNivel = nombreNivel;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

  public String getNitEmpresa() {
    return nitEmpresa;
  }

  public void setNitEmpresa(String nitEmpresa) {
    this.nitEmpresa = nitEmpresa;
  }

  public RealmList<Proceso> getProcesos() {
    return procesos;
  }

  public void setProcesos(RealmList<Proceso> procesos) {
    this.procesos = procesos;
  }

  public RealmList<Nivel2> getNiveles2() {
    return niveles2;
  }

  public void setNiveles2(RealmList<Nivel2> niveles2) {
    this.niveles2 = niveles2;
  }

  public RealmList<Responsable> getResponsables() {
    return responsables;
  }

  public void setResponsables(RealmList<Responsable> responsables) {
    this.responsables = responsables;
  }
}
