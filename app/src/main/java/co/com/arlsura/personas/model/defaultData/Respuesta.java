package co.com.arlsura.personas.model.defaultData;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/7/17.
 */

public class Respuesta extends RealmObject {

  /**
   * id : 2
   * nombre : Comportamiento a intervenir
   * valor : Comportamiento a intervenir
   * descripcion : null
   */
  @PrimaryKey private int id;
  private String nombre;
  private String valor;
  private String descripcion;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getValor() {
    return valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
}
