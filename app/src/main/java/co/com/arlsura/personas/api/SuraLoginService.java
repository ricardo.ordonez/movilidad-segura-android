package co.com.arlsura.personas.api;


import androidx.annotation.NonNull;

import java.util.Map;

import co.com.arlsura.personas.model.SuraUser;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface SuraLoginService {
    @NonNull
    @POST("sel-services/public/afiliados/verificarAfiliacionDni")
    Observable<SuraUser> verifyAfiliation(@Body Map<String, Object> param);

}
