package co.com.arlsura.personas.ui.observer;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseActivity;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;



import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import io.reactivex.Observable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import timber.log.Timber;

public class NewObservation extends BaseActivity implements com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {

@BindView(R.id.toolbarNewObvervation) Toolbar toolbarNewObvervation;
  @BindView(R.id.edtDate) EditText edtDate;
  @BindView(R.id.edtHour) EditText edtHour;
  @BindView(R.id.edtNumeroPersonas) EditText edtNumeroPersonas;
  @BindView(R.id.btnContinuar) Button btnContinuar;

  private DatePickerDialog datePickerDialog;
  private TimePickerDialog dateTimeDialog;
  SimpleDateFormat formatoDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
  SimpleDateFormat formatoHour = new SimpleDateFormat("HH:mm", Locale.getDefault());
  private Map<String, Object> paramsObserver = new HashMap<>();

  @Override public int getLayoutId() {
    return R.layout.activity_new_observation;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbarNewObvervation);
    toolbarNewObvervation.setNavigationOnClickListener(view -> finish());
    initUi();
  }

  private void initUi() {
    Calendar now = Calendar.getInstance();
    datePickerDialog = DatePickerDialog.newInstance(onDateSetListener, now.get(Calendar.YEAR),
        now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));

    dateTimeDialog = TimePickerDialog.newInstance(onTimeSetListener, true);

    edtDate.setText(formatoDate.format(now.getTime()));
    edtHour.setText(formatoHour.format(now.getTime()));
      createDatePickerDialog();

    Observable.combineLatest(RxTextView.textChangeEvents(edtDate),
        RxTextView.textChangeEvents(edtHour), RxTextView.textChangeEvents(edtNumeroPersonas),
        (d, h, numPerson) -> {
          boolean dateCheck = d.text().length() > 0;
          boolean hourCheck = h.text().length() > 0;
          boolean numPersonCheck = numPerson.text().length() > 0;
          return dateCheck && hourCheck && numPersonCheck;
        }).subscribe(aBoolean -> isEnabled(btnContinuar, aBoolean));
  }

    SpinnerDatePickerDialogBuilder builder;
    private void createDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();

        builder = new SpinnerDatePickerDialogBuilder()
                .context(NewObservation.this)
                .callback(NewObservation.this)
                .showTitle(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .maxDate(calendar.get(Calendar.YEAR) + 1, 0, 1)
                .minDate(calendar.get(Calendar.YEAR) - 1, 0, 1);
    }


  private DatePickerDialog.OnDateSetListener onDateSetListener =
      (view, year, monthOfYear, dayOfMonth) -> {
        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear, dayOfMonth);
        try {
          edtDate.setText(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
          Timber.e(e);
        }
      };

  private TimePickerDialog.OnTimeSetListener onTimeSetListener =
      (view, hourOfDay, minute, second) -> {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE,minute);

          edtHour.setText(formatoHour.format(calendar.getTime()));
      };

  @OnClick(R.id.btnContinuar) public void onViewClicked() {
    paramsObserver.put("fechaRegistro", edtDate.getText().toString());
    paramsObserver.put("horaRegistro", edtHour.getText().toString());
    paramsObserver.put("personasObservar", edtNumeroPersonas.getText().toString());
    BasicData.paramsObserver = paramsObserver;
    goActv(BasicData.class, false);
  }

  @OnClick({ R.id.edtDate, R.id.edtHour }) public void onViewClicked(View view) {
    switch (view.getId()) {
      case R.id.edtDate:
          String[] fecha = ((TextView) findViewById(R.id.edtDate)).getText().toString().split("-");

          int year = Integer.parseInt(fecha[0]);
          int month = Integer.parseInt(fecha[1]);
          int day = Integer.parseInt(fecha[2]);

          builder.defaultDate(year,month-1,day);
          builder.build().show();
          break;
      case R.id.edtHour:
        dateTimeDialog.show(getFragmentManager(), "Timepickerdialog");
        break;
    }
  }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear + 1, dayOfMonth);
        try {
            edtDate.setText(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
            Timber.e(e);
        }
    }
}
