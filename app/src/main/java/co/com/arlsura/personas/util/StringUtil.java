package co.com.arlsura.personas.util;

import java.text.Normalizer;

/**
 * Created by home on 1/9/18.
 */

public class StringUtil {
  public static String removeAccents(String msg) {
    if (msg != null) return Normalizer.normalize(msg, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    return null;
  }
}
