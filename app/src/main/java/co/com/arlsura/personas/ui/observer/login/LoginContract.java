package co.com.arlsura.personas.ui.observer.login;

import co.com.arlsura.personas.base.BaseContract;
import co.com.arlsura.personas.model.User;

/**
 * Created by home on 11/7/17.
 */

public interface LoginContract  extends BaseContract{
  void onLoadUser(User user);
  void onSuccesLogOut();
}
