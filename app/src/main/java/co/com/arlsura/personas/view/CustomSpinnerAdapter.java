package co.com.arlsura.personas.view;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import co.com.arlsura.personas.R;

public class CustomSpinnerAdapter extends ArrayAdapter<String>
{

    List<String> values;
    boolean firstRun = true;
    boolean onViewSelected = false;

    public CustomSpinnerAdapter(@NonNull Context context, int resource, List<String> values)
    {
        super(context, resource);
        this.values = values;
    }
    @Override public boolean isEnabled(int position) {
        return true;
    }

    public void setOnViewSelected(boolean state){
        this.onViewSelected  = state;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(values.size() > 1 && onViewSelected && position + 1 != values.size()){
            position++;
            notifyDataSetChanged();
        }


        if(convertView instanceof TextView)
            ((TextView) convertView).setGravity(Gravity.LEFT);

        return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(values.size() > 1){
            position++;
        }



        View view = super.getDropDownView(position, convertView, parent);
        TextView tv = (TextView) view;

        if (position == 0) {

            tv.setTextColor(ContextCompat.getColor(this.getContext(), R.color.grayText));
        } else {
            tv.setTextColor(ContextCompat.getColor(this.getContext(), R.color.grayText));
        }


        ((TextView) view).setGravity(Gravity.LEFT);

        return view;
    }


    @Override
    public int getCount() {
        return  (values.size() > 1) ? values.size() - 1 : values.size();
    }


}
