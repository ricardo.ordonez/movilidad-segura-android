package co.com.arlsura.personas.dao;

import co.com.arlsura.personas.base.BaseDaoContract;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by home on 11/7/17.
 */

public class PreguntasDao implements BaseDaoContract<Preguntas> {

  @Override public void save(Preguntas item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void update(Preguntas item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void delete(Preguntas item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item.deleteFromRealm();
    realm.commitTransaction();
  }

  @Override public Preguntas findFirst() {
    Preguntas item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item = realm.where(Preguntas.class).findFirst();
    realm.commitTransaction();
    return item;
  }

  public List<Respuesta> findById(String id) {
    List<Respuesta> list = null;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    RealmResults<Preguntas> results = realm.where(Preguntas.class).equalTo("codigo", id).findAll();
    if (!results.isEmpty()) {
      list = new ArrayList<>();
      for (Preguntas item : results) {
        list.addAll(item.getRespuestas());
      }
    }
    realm.commitTransaction();
    return list;
  }

  public List<Preguntas> findAll() {
    List<Preguntas> list = null;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    RealmResults<Preguntas> results = realm.where(Preguntas.class).findAll();
    if (!results.isEmpty()) {
      list = new ArrayList<>();
      list.addAll(results);
    }
    realm.commitTransaction();
    return list;
  }

  @Override public Preguntas createFromJson(String json) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    Preguntas item =
        realm.createObjectFromJson(Preguntas.class, json); // Save a bunch of new Customer objects
    realm.commitTransaction();
    return item;
  }

  public void createAllFromJson(String json) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.createAllFromJson(Preguntas.class, json);
    realm.commitTransaction();
  }
}
