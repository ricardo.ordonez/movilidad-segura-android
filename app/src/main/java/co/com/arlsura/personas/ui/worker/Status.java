package co.com.arlsura.personas.ui.worker;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import co.com.arlsura.personas.ui.observer.login.LoginContract;
import co.com.arlsura.personas.ui.observer.login.LoginPresenter;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import java.util.HashMap;
import java.util.Map;

public class Status extends BaseActivity {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.imgStatus) ImageView imgStatus;
  @BindView(R.id.txtStatus) TextView txtStatus;
  @BindView(R.id.btnHacerOtro) Button btnHacerOtro;
  public static Map<String, Object> paramsObserver = new HashMap<>();
  LoginPresenter presenter;

  @Override public int getLayoutId() {
    return R.layout.activity_status;
  }

  private void setToolbar(){
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    getSupportActionBar().setDisplayShowHomeEnabled(false);
  }
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setToolbar();
    setUserPresenter();

    showLoading("Cargando");

    ReactiveNetwork.observeInternetConnectivity().subscribe(isConnectedToInternet -> {
      runOnUiThread(() -> {


        imgStatus.setBackground(
            isConnectedToInternet ? ContextCompat.getDrawable(this, R.drawable.ok)
                : ContextCompat.getDrawable(this, R.drawable.signal));
        txtStatus.setText(
            isConnectedToInternet ? R.string.reporte_enviado : R.string.no_internet_reporte);
        btnHacerOtro.setVisibility(isConnectedToInternet ? View.VISIBLE : View.GONE);
      });

      if (isConnectedToInternet) {
        dissmisLoading();
        BasicDataPresenter presenter =
            new BasicDataPresenter(WebServices.getApi(), new ProcesoDao(), basicDataContract);
        presenter.ignoreSync = true;
        presenter.syncWorker();
        presenter.ignoreSync = false;

      }
    });
  }

  private void setUserPresenter(){
    LoginContract loginContract = new LoginContract() {
      @Override
      public void onLoadUser(User user) {

      }

      @Override
      public void onSuccesLogOut() {
        goActv(SelectProfile.class,true);
      }

      @Override
      public void setLoading() {
        showLoading("Cerrando Sesion");
      }

      @Override
      public void hideLoading() {
        Status.this.dissmisLoading();
      }

      @Override
      public void onErr(String err) {
        showAlertDialog(err);
      }

      @Override
      public void onErr(int err) {
        showAlertDialog(err);
      }
    };

    UserDao userDao = new UserDao();


     presenter = new LoginPresenter(loginContract,WebServices.getApi(),userDao);

  }
  private void goHome(){
         presenter.logOut();
  }

  @OnClick({ R.id.btnSalir, R.id.btnHacerOtro }) public void onViewClicked(View view) {
    boolean isIntern = new UserDao().findFirst().isIntern();
    switch (view.getId()) {
      case R.id.btnSalir:
          goHome();
        break;
      case R.id.btnHacerOtro:
        goActv(isIntern ? IngressIntern.class : IngressExternal.class, true);
        break;
    }
  }
}
