package co.com.arlsura.personas.ui.observer.assesment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.base.BaseFragment;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.StatusComportamiento;
import co.com.arlsura.personas.ui.observer.AssesmentDetail;
import co.com.arlsura.personas.util.Variable;
import co.com.arlsura.personas.view.CustomSpinner;
import com.jakewharton.rxbinding2.widget.RxTextView;
import io.realm.Realm;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import timber.log.Timber;

public class FragmentAsessment extends BaseFragment {
  @BindView(R.id.lblValoracion) TextView lblValoracion;
  @BindView(R.id.edtValoracion) CustomSpinner edtValoracion;
  @BindView(R.id.txtDescripcion) TextView txtDescripcion;
  @BindView(R.id.lblObservacion) TextView lblObservacion;
  @BindView(R.id.edtObservacion) EditText edtObservacion;
  @BindView(R.id.lblSituacion) TextView lblSituacion;
  @BindView(R.id.edtSituacion) EditText edtSituacion;
  @BindView(R.id.lbl_mejorarlo) TextView lblMejorarlo;
  @BindView(R.id.edtMejora) EditText edtMejora;
  @BindView(R.id.radioSi) AppCompatRadioButton radioSi;
  @BindView(R.id.radioNo) AppCompatRadioButton radioNo;

  private Comportamientos comportamiento;

  private List<String> listValoracion = new ArrayList<>();
  private List<Respuesta> listRespuestas;
  private AssesmentContract listenerAssesment;

  private Variable<Boolean> isObservacionRequired = new Variable<>(false);
  private Variable<Boolean> isSituacionRequired = new Variable<>(false);
  private Variable<Boolean> isMejoraRequired = new Variable<>(false);

  public static FragmentAsessment newInstance(int id, int position) {
    FragmentAsessment myFragment = new FragmentAsessment();

    Bundle args = new Bundle();
    args.putInt("id", id);
    args.putInt("position", position);
    myFragment.setArguments(args);

    return myFragment;
  }

  public FragmentAsessment() {
  }

  private boolean checkObservacion() {
    return !isObservacionRequired.get()
        || isObservacionRequired.get() && !edtObservacion.getText().toString().isEmpty();
  }

  @Override public void onPause() {
    super.onPause();
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    comportamiento.setEstado(checkState());
    realm.commitTransaction();
  }

  private boolean checkSituacion() {
    return !isSituacionRequired.get()
        || isSituacionRequired.get() && !edtSituacion.getText().toString().isEmpty();
  }

  private boolean checkMejora() {
    return !isMejoraRequired.get()
        || isMejoraRequired.get() && !edtMejora.getText().toString().isEmpty();
  }

  private boolean checkValoracion(){

    if((edtValoracion.getSelectedView()) != null){
      if(!((TextView)edtValoracion.getSelectedView()).getText().equals(edtValoracion.getPrompt()))
       return true;
    }

    return false;
  }

  private String checkState() {
    String state = StatusComportamiento.Incomplete.toString();
    if (checkObservacion() && checkSituacion() && checkMejora() && checkValoracion()) {
      state = StatusComportamiento.Succes.toString();
    }
    return state;
  }

  @Override protected int getLayoutId() {
    return R.layout.fragment_assesment;
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    loadDefaultData();
    restoreComportamiento();
    initUi();
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    listenerAssesment = (AssesmentContract) context;
  }

  private void loadDefaultData() {
    PreguntasDao daoPreguntas = new PreguntasDao();
    listValoracion.clear();

    listRespuestas = daoPreguntas.findById("edtValoracion");
    if (listRespuestas != null && !listRespuestas.isEmpty()) {
      for (Respuesta item : listRespuestas) {
        listValoracion.add(item.getNombre());
      }
    }
    edtValoracion.setOnItemSelectedListener(onItemSelectedListener);
    edtValoracion.initializeStringValues(listValoracion, edtValoracion.getPrompt().toString());
  }

  private String getRespuestaByValor(@Nonnull String valor) {
    String out = null;
    for (Respuesta item : listRespuestas) {
      if (item.getValor().equalsIgnoreCase(valor)) {
        out = item.getNombre();
        break;
      }
    }
    return out;
  }

  private int getPositionValoracionByNombre(String nombre) {
    int position = -1;
    if (nombre != null) {
      Timber.e("nombre ".concat(nombre));
      for (int i = 0; i < listValoracion.size(); i++) {
        if (nombre.equalsIgnoreCase(listValoracion.get(i))) {
          position = i;
          break;
        }
      }
    }

    return position;
  }

  private void initUi() {
    txtDescripcion.setText(comportamiento.getComportamiento());
    isObservacionRequired.asObservable()
        .subscribe(aBoolean -> edtObservacion.setHint(
            aBoolean ? R.string.edt_observacion_obligatorio : R.string.edt_observacion));

    isSituacionRequired.asObservable().subscribe(aBoolean -> {
      Timber.e("isSituacionRequired ".concat(String.valueOf(aBoolean)));
      lblSituacion.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
      edtSituacion.setVisibility(aBoolean ? View.VISIBLE : View.GONE);

      edtSituacion.setHint(aBoolean ? R.string.edt_situacion_obligario : R.string.edt_situacion);

      lblMejorarlo.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
      edtMejora.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
    });

    RxTextView.textChangeEvents(edtObservacion).subscribe(text -> {
      Realm realm = Realm.getDefaultInstance();
      realm.beginTransaction();
      comportamiento.setObservacion(text.text().toString());
      comportamiento.setEstado(checkState());
      realm.commitTransaction();
      listenerAssesment.onItemChange(comportamiento);
    });

    RxTextView.textChangeEvents(edtSituacion).subscribe(text -> {
      Realm realm = Realm.getDefaultInstance();
      realm.beginTransaction();
      comportamiento.setDescripcionSituacion(text.text().toString());
      comportamiento.setEstado(checkState());
      realm.commitTransaction();
      listenerAssesment.onItemChange(comportamiento);
    });

    RxTextView.textChangeEvents(edtMejora).subscribe(text -> {
      Realm realm = Realm.getDefaultInstance();
      realm.beginTransaction();
      comportamiento.setPlanMejora(text.text().toString());
      comportamiento.setEstado(checkState());
      realm.commitTransaction();
      listenerAssesment.onItemChange(comportamiento);
    });
  }
  private AdapterView.OnItemSelectedListener onItemSelectedListener =
          new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

              //Fix
              int fixedPosition = i;




          switch (adapterView.getId()) {
            case R.id.edtValoracion:


              if(((TextView)edtValoracion.getSelectedView()).getText().equals(edtValoracion.getPrompt()) && !edtValoracion.userTouch){
                return;
              }


                if (listRespuestas.get(fixedPosition).getNombre().equalsIgnoreCase("No aplica")) {
                  isObservacionRequired.set(true);
                } else {
                  isObservacionRequired.set(false);
                }
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();

                // fix
                comportamiento.setValoracion(String.valueOf((fixedPosition + 1)));
                comportamiento.setEstado(checkState());

              realm.commitTransaction();
              realm.close();
              listenerAssesment.onItemChange(comportamiento);

                Timber.e("comportamiento edtValoracion estado ".concat(comportamiento.getEstado()));



              break;
          }
        }

        @Override public void onNothingSelected(AdapterView<?> adapterView) {
          Timber.d("tipoDoumento onNothingSelected ");
        }


      };

  private void restoreComportamiento() {
    int id = getArguments().getInt("id");
    int positionItem = getArguments().getInt("position");

    Realm realm = Realm.getDefaultInstance();
    comportamiento = realm.where(Comportamientos.class).equalTo("id", id).findFirst();
    realm.beginTransaction();
    comportamiento.setPosition(positionItem);
    realm.commitTransaction();
    realm.close();

    int position =
        getPositionValoracionByNombre(getRespuestaByValor(comportamiento.getValoracion()));
    Timber.e("position ".concat(String.valueOf(position)));
    if (position != -1) {
      edtValoracion.setSelection(position);
      if (getRespuestaByValor(comportamiento.getValoracion()).equalsIgnoreCase("No aplica")) {
        isObservacionRequired.set(true);
      }
    }
    radioSi.setChecked(comportamiento.getCompartamientoInseguro());
    radioNo.setChecked(!comportamiento.getCompartamientoInseguro());

    isSituacionRequired.set(comportamiento.getCompartamientoInseguro());
    isMejoraRequired.set(false);

    Boolean aBoolean = comportamiento.getCompartamientoInseguro();
    lblSituacion.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
    edtSituacion.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
    edtSituacion.setHint(aBoolean ? R.string.edt_situacion_obligario : R.string.edt_situacion);
    lblMejorarlo.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
    edtMejora.setVisibility(aBoolean ? View.VISIBLE : View.GONE);

    if (comportamiento.getObservacion() != null && comportamiento.getObservacion().length() > 0) {
      edtObservacion.setText(comportamiento.getObservacion());
    }

    if (comportamiento.getDescripcionSituacion() != null
        && comportamiento.getDescripcionSituacion().length() > 0) {
      edtSituacion.setText(comportamiento.getDescripcionSituacion());
    }

    if (comportamiento.getPlanMejora() != null && comportamiento.getPlanMejora().length() > 0) {
      edtMejora.setText(comportamiento.getPlanMejora());
    }

    realm.beginTransaction();
    comportamiento.setEstado(checkState());
    realm.commitTransaction();
    realm.close();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    AssesmentDetail assesmentDetail = (AssesmentDetail) getActivity();
    assesmentDetail.handlekeyboardState();
  }

  @OnClick({ R.id.radioSi, R.id.radioNo }) public void onViewClicked(View view) {

    switch (view.getId()) {
      case R.id.radioSi:
        boolean siCheked = ((RadioButton) view).isChecked();
        radioNo.setChecked(!siCheked);
        radioSi.setChecked(siCheked);

        isSituacionRequired.set(true);
        isMejoraRequired.set(false);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        comportamiento.setCompartamientoInseguro(true);
        comportamiento.setEstado(checkState());
        realm.commitTransaction();

        listenerAssesment.onItemChange(comportamiento);

        break;
      case R.id.radioNo:
        boolean noCheked = ((RadioButton) view).isChecked();
        radioNo.setChecked(noCheked);
        radioSi.setChecked(!noCheked);

        isSituacionRequired.set(false);
        isMejoraRequired.set(false);

        Realm realm2 = Realm.getDefaultInstance();
        realm2.beginTransaction();
        comportamiento.setCompartamientoInseguro(false);
        comportamiento.setEstado(checkState());
        realm2.commitTransaction();

        listenerAssesment.onItemChange(comportamiento);
        break;
    }
  }

  @OnClick(R.id.imgInfo) public void onViewClicked() {
    showAlertDialogNeutral(R.string.infoAssesmet, new BaseActivity.confirmDialog() {
      @Override public void onPositive() {

      }

      @Override public void onNegative() {

      }
    });
  }
}
