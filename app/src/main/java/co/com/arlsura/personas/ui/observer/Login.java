package co.com.arlsura.personas.ui.observer;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.TipoDocumento;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import co.com.arlsura.personas.ui.defaultData.DefaultDataPresenter;
import co.com.arlsura.personas.ui.defaultData.defaultDataContract;
import co.com.arlsura.personas.ui.observer.login.LoginContract;
import co.com.arlsura.personas.ui.observer.login.LoginPresenter;
import co.com.arlsura.personas.util.Constants;

import com.google.gson.reflect.TypeToken;
import com.jakewharton.rxbinding2.widget.RxTextView;
import io.reactivex.Observable;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import timber.log.Timber;

public class Login extends BaseActivity implements LoginContract {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.edtTipoDocumentoEmpresa) Spinner edtTipoDocumentoEmpresa;
  @BindView(R.id.edtNumeroDocumentoEmpresa) EditText edtNumeroDocumentoEmpresa;
  @BindView(R.id.edtNumeroDocumentoPersona) EditText edtNumeroDocumentoPersona;
  @BindView(R.id.edtPasswordPersona) EditText edtPasswordPersona;
  @BindView(R.id.btnLogin) Button btnLogin;

  @Override public int getLayoutId() {
    return R.layout.activity_login_observer;
  }

  private LoginPresenter presenter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  try{
   initPresenter();
    Timber.tag("Login");
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(view -> finish());
    initUi();
    isEnabled(btnLogin, false);

  }catch (Exception e){
    showAlertDialog(e.getMessage());
  }


    /*if (Config.isUiTest()) {
      edtNumeroDocumentoEmpresa.setText("800256161");
      edtNumeroDocumentoPersona.setText("70111974");F
      edtPasswordPersona.setText("70111974");
    }*/
  }

  private void initUi() {
    List<String> itemsTipoDocumento = new ArrayList<>();


    itemsTipoDocumento.addAll(presenter.getTiposArray());


    ArrayAdapter<String> adapterCity =
        new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, itemsTipoDocumento);
    adapterCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    edtTipoDocumentoEmpresa.setAdapter(adapterCity);
    edtTipoDocumentoEmpresa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Timber.d("tipoDoumento ".concat(itemsTipoDocumento.get(i)));
      }

      @Override public void onNothingSelected(AdapterView<?> adapterView) {
        Timber.d("tipoDoumento onNothingSelected ");
      }
    });
   edtTipoDocumentoEmpresa.setSelection(1);
    //NIT
    Observable.combineLatest(RxTextView.textChangeEvents(edtNumeroDocumentoEmpresa),
        RxTextView.textChangeEvents(edtNumeroDocumentoPersona),
        RxTextView.textChangeEvents(edtPasswordPersona),
        (documentoEmpresa, documentoPerona, passPersona) -> {
          boolean documentoEmpresaCheck = documentoEmpresa.text().length() > 4;
          boolean documentoPeronaCheck = documentoPerona.text().length() > 4;
          boolean passwordCheck = passPersona.text().length() > 4;
          return documentoEmpresaCheck && documentoPeronaCheck && passwordCheck;
        }).subscribe(aBoolean -> isEnabled(btnLogin, aBoolean));


  }

  private void initPresenter() {
    presenter = new LoginPresenter(this, WebServices.getApi(), new UserDao());
    DefaultDataPresenter defaultDataPresenter = new DefaultDataPresenter(new defaultDataContract() {
      @Override public void onLoadPreguntas(List<Preguntas> preguntasList) {



        initUi();
      }

      @Override public void setLoading() {

      }

      @Override public void hideLoading() {

      }

      @Override public void onErr(String err) {

      }

      @Override public void onErr(int err) {

      }
    }, WebServices.getApi(), new PreguntasDao());
    defaultDataPresenter.loadPreguntas();
  }

  @Override public void setLoading() {
    Timber.d("setLoading");
    showLoading(getString(R.string.loading));
  }

  @Override public void hideLoading() {
    Timber.d("hideLoading");
    dissmisLoading();
  }

  @Override public void onErr(String err) {
    Timber.d("onErr");
    showAlertDialog(err);
  }

  @Override public void onErr(int err) {
    Timber.d(getString(err));
    showAlertDialog(err);
  }

  @Override public void onLoadUser(User user) {
    Timber.d("onLoadUser", user);
    goActv(History.class, true);
  }

  @Override public void onSuccesLogOut() {

  }

  @OnClick(R.id.btnLogin) public void onViewClicked() {

    if ((Constants.userFailDefault.equalsIgnoreCase(
        edtNumeroDocumentoPersona.getText().toString()))) {
      showAlertDialog(R.string.usuario_no_autorizado);
    } else if (Constants.keyFailDefault.equalsIgnoreCase(edtPasswordPersona.getText().toString())) {
      showAlertDialog(R.string.usuario_no_autorizado);
    } else {

      List<TipoDocumento> listaDocumentos = presenter.getTiposDocumentos();

      String document =   edtNumeroDocumentoEmpresa.getText().toString();

      Map<String, Object> param = new HashMap<>();
      param.put("username", edtNumeroDocumentoPersona.getText().toString());
      param.put("password", edtPasswordPersona.getText().toString());
      param.put("document", document);
      presenter.loginObserver(param);
    }
  }
}
