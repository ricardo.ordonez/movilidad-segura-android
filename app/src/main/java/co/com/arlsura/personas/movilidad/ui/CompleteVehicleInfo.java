package co.com.arlsura.personas.movilidad.ui;

import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import co.com.arlsura.personas.R;
import timber.log.Timber;

public class CompleteVehicleInfo extends AppCompatActivity implements
        View.OnClickListener,
        com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener{

    SimpleDateFormat formatoDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_vehicle_info);
        initUI();

    }

    private void initUI(){
        setToolbar();
        setupInputtext();
        setTextViewCaledars();

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void setToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_arrow_blue,getTheme()));

        TextView toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        toolbarTitle.setText(R.string.activity_title_documentacion_vehiculo);
    }
    TextInputLayout errorInputLayout, customErrorInputLayout;
    TextInputEditText errorEditText, customErrorEditText;
    TextView edtFechaMatricula;

    private void setupInputtext(){
        edtFechaMatricula = (TextView) findViewById(R.id.edtFechaMatricula);
        edtFechaMatricula.setOnClickListener(this);


    }


    SpinnerDatePickerDialogBuilder builder;
    private void setDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();

        builder = new SpinnerDatePickerDialogBuilder()
                .context(CompleteVehicleInfo.this)
                .callback(CompleteVehicleInfo.this)
                .showTitle(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .maxDate(calendar.get(Calendar.YEAR) + 1, 0, 1)
                .minDate(calendar.get(Calendar.YEAR) - 1, 0, 1);
    }

    private void setTextViewCaledars(){
        TextView edtFechaMatricula;
        TextView edtFechaSoat;
        TextView edtFechaVecSeguro;
        TextView edtFechaVecExtintor;
        TextView edtFechaRevTecMecanica;

        edtFechaMatricula = (TextView) findViewById(R.id.edtFechaMatricula);
        edtFechaSoat = (TextView) findViewById(R.id.edtFechaSoat);
        edtFechaVecSeguro = (TextView) findViewById(R.id.edtFechaVecSeguro);
        edtFechaVecExtintor = (TextView) findViewById(R.id.edtFechaVecExtintor);
        edtFechaRevTecMecanica = (TextView) findViewById(R.id.edtFechaRevTecMecanica);

        setDatePickerDialog();
        edtFechaMatricula.setOnClickListener(this);
        edtFechaSoat.setOnClickListener(this);
        edtFechaVecSeguro.setOnClickListener(this);
        edtFechaVecExtintor.setOnClickListener(this);
        edtFechaRevTecMecanica.setOnClickListener(this);


    }
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        if(viewReference == 0)
            return;

        TextView referencedText = findViewById(viewReference);

        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear + 1, dayOfMonth);
        try {
            referencedText.setText(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
            Timber.e(e);
        }
    }

    int viewReference = 0;
    @Override
    public void onClick(View v) {

       viewReference = v.getId();
       TextView referencedView = findViewById(viewReference);
       String text = referencedView.getText().toString();
       handleTextViewCalendar(text);

    }

    private void handleTextViewCalendar(String date){

        if(date.isEmpty()){
            Calendar calendar = Calendar.getInstance();
            builder.defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            builder.build().show();
        }
        else if (date.contains("-")){
            String[] fecha = date.split("-");
            int year = Integer.parseInt(fecha[0]);
            int month = Integer.parseInt(fecha[1]);
            int day = Integer.parseInt(fecha[2]);
            builder.defaultDate(year,month-1,day);
            builder.build().show();
        }else{
            Timber.e("Invalid date string format");
        }
    }
}
