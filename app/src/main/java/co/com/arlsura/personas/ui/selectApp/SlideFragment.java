package co.com.arlsura.personas.ui.selectApp;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import co.com.arlsura.personas.R;

public class SlideFragment extends Fragment {

    private ImageView slideImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_selectapp_info, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        slideImage = (ImageView) view;
        bindSlideImage();
    }

    void bindSlideImage(){


        Drawable slideDrawable = ResourcesCompat.getDrawable(getResources(),R.drawable.slide_bg,getActivity().getTheme());

        slideImage.setImageDrawable(slideDrawable);
    }
}
