package co.com.arlsura.personas.movilidad.ui.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseActivity;
import timber.log.Timber;

/**
 * Created by home on 11/14/17.
 */

public abstract class BaseFragment extends Fragment {
  protected abstract int getLayoutId();

  private Unbinder unbinder;

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(getLayoutId(), container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  public boolean showNext(Object currentSelected, CharSequence prompt) {
    return !currentSelected.toString().equalsIgnoreCase(prompt.toString());
  }

  public void showAlertDialogNeutral(int text, BaseActivity.confirmDialog listener) {
    MaterialDialog dialog = new MaterialDialog.Builder(getContext()).title(R.string.app_name)
        .content(text)
        .positiveText(R.string.agree)
        .autoDismiss(true)
        .dismissListener(dialogInterface -> {
          Timber.d("dismissListener dismiss");
          listener.onPositive();
        })
        .onPositive((dialog1, which) -> {
          Timber.d("onPositive click");
          dialog1.dismiss();
        })
        .show();
  }

  public interface confirmDialog {
    void onPositive();

    void onNegative();
  }
}
