package co.com.arlsura.personas.dao;

import co.com.arlsura.personas.base.BaseDaoContract;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.niveles.StatusSync;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by home on 11/19/17.
 */

public class WorkerObserverDao implements BaseDaoContract<WorkerObservation> {

  @Override public void save(WorkerObservation item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void update(WorkerObservation item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void delete(WorkerObservation item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item.deleteFromRealm();
    realm.commitTransaction();
  }

  @Override public WorkerObservation findFirst() {
    WorkerObservation item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item = realm.where(WorkerObservation.class).findFirst();
    realm.commitTransaction();
    return item;
  }

  @Override public WorkerObservation createFromJson(String json) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    WorkerObservation item = realm.createObjectFromJson(WorkerObservation.class,
        json); // Save a bunch of new Customer objects
    realm.commitTransaction();
    return item;
  }

  public List<WorkerObservation> findPendingSync() {
    List<WorkerObservation> list = null;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();

    RealmResults<WorkerObservation> results = realm.where(WorkerObservation.class)
        .equalTo("status", StatusSync.PendingSync.toString())
        .findAll();
    if (!results.isEmpty()) {
      list = new ArrayList<>();
      list.addAll(results);
    }

    realm.commitTransaction();
    return list;
  }

  public WorkerObservation findFirstPending() {
    WorkerObservation item;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();

    item = realm.where(WorkerObservation.class)
        .equalTo("status", StatusSync.Pending.toString())
        .findFirst();

    realm.commitTransaction();
    return item;
  }
}
