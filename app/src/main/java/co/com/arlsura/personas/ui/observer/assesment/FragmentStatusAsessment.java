package co.com.arlsura.personas.ui.observer.assesment;

import android.app.Application;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.base.BaseFragment;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.ui.observer.AssesmentDetail;
import co.com.arlsura.personas.ui.observer.History;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataContract;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import java.util.List;
import timber.log.Timber;

public class FragmentStatusAsessment extends BaseFragment {

  @BindView(R.id.imageView2) ImageView imageView2;
  @BindView(R.id.txtStatus) TextView txtStatus;
  @BindView(R.id.btnStatusAction) Button btnStatusAction;
  public static boolean isOK = false;
  public static int procesoRootId = -1;
  public boolean alreadyGone = false;
  public static FragmentStatusAsessment newInstance() {
    FragmentStatusAsessment myFragment = new FragmentStatusAsessment();

    Bundle args = new Bundle();
    // args.putInt("id", id);
    // args.putInt("position", position);
    myFragment.setArguments(args);

    return myFragment;
  }

  public void createSubcriber() {

  }
  public FragmentStatusAsessment() {
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();

  }

  @Override protected int getLayoutId() {
    return R.layout.fragment_assesment_status;
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    restoreComportamiento();
    initUi();
  }

  @Override public void onResume() {
    super.onResume();
    Timber.e("onResume ".concat(String.valueOf(isOK)));
    initUi();
  }

  @Override public void setUserVisibleHint(boolean isVisibleToUser) {
    super.setUserVisibleHint(isVisibleToUser);
    Timber.e("setUserVisibleHint ".concat(String.valueOf(isOK)));
    if (isVisibleToUser) initUi();
  }

  private void initUi() {
    Timber.e("initUi ".concat(String.valueOf(isOK)));
    txtStatus.setText(isOK ? getString(R.string.state_ok) : getString(R.string.state_error));
    btnStatusAction.setText(isOK ? getString(R.string.finalizar) : getString(R.string.regresar));
    imageView2.setBackground(isOK ? ContextCompat.getDrawable(getActivity(), R.drawable.ok)
        : ContextCompat.getDrawable(getActivity(), R.drawable.bug));

    btnStatusAction.setEnabled(true);
    alreadyGone = false;

  }

  private void restoreComportamiento() {

  }

  private BasicDataContract contract = new BasicDataContract() {
    @Override public void onLoadNiveles(List<Nivel1> list) {

    }

    @Override public void onLoad(List<Comportamientos> list) {

    }

    @Override public void onLoadRootProceso(Proceso procesoRoot) {

    }

    @Override public void setLoading() {
      if(getActivity() != null && getActivity() instanceof  BaseActivity)
        ((BaseActivity) getActivity()).showLoading("Cargando");
    }

    @Override public void hideLoading() {
      if(getActivity() != null && getActivity() instanceof  BaseActivity)
        ((BaseActivity) getActivity()).dissmisLoading();
    }

    @Override public void onErr(String err) {

    }

    @Override public void onErr(int err) {

    }
  };



  public void handleKeyboardShown(){


  }

  @OnClick(R.id.btnStatusAction) public void onViewClicked() {


    Timber.e("btnStatusAtion isOK ".concat(String.valueOf(isOK)));
    if (isOK && procesoRootId != -1) {
      btnStatusAction.setEnabled(false);
      Realm realm = Realm.getDefaultInstance();

      Proceso comportamiento = realm.where(Proceso.class).equalTo("id", procesoRootId).findFirst();
      realm.beginTransaction();
      comportamiento.setStatus(StatusSync.PendingSync.toString());
      realm.commitTransaction();
      realm.close();


      BasicDataPresenter presenter =
          new BasicDataPresenter(WebServices.getApi(), new ProcesoDao(), contract);
      Application application = getActivity().getApplication();
      if (application != null) {
        presenter.sync(application);
      }


      Consumer<Boolean> consumer = new Consumer<Boolean>() {
        @Override
        public void accept(Boolean aBoolean) throws Exception {


          BaseActivity activity = (BaseActivity) getActivity();

          contract.hideLoading();

          if(activity == null || alreadyGone)
             return;


          activity.goActv(History.class, true);
          btnStatusAction.setEnabled(true);
          alreadyGone = true;

        }
      };


      BasicDataPresenter.dataSynced
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread()).
              subscribe(consumer);

    } else {

      AssesmentDetail activity = (AssesmentDetail) getActivity();
      if(activity == null){
        getActivity().finish();
      }else{
        activity.goBack();
      }

    }
  }
}
