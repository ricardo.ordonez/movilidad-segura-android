package co.com.arlsura.personas.base;

import io.realm.RealmObject;

/**
 * Created by home on 11/7/17.
 */

public interface BaseDaoContract<T extends RealmObject> {

  void save(T item);

  void update(T item);

  void delete(T item);

  T findFirst();

  T createFromJson(String json);
}
