package co.com.arlsura.personas.movilidad.ui.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;


public class VehicleDetailsFragment extends BaseFragment {

    @BindView(R.id.vpCheckList)
    ViewPager vpCheckList;
    @BindView(R.id.tabsChecklist)
    TabLayout tabsChecklist;

    public VehicleDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_vehicle_details;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vpCheckList.setAdapter(new VehicleDetailsFragmentAdapter(getChildFragmentManager()));

        tabsChecklist.setTabMode(TabLayout.MODE_FIXED);
        tabsChecklist.setupWithViewPager(vpCheckList);
        createCustomTabs();
    }

    private void createCustomTabs() {

        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tabview,null);
        ImageView tabOneImage = tabOne.findViewById(R.id.img_tab);
        TextView tabOneText = tabOne.findViewById(R.id.tv_tab);

        tabOneImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.icon_mano_o,getActivity().getTheme()));
        tabOneText.setText("Chequeo");

        tabsChecklist.getTabAt(0).setCustomView(tabOne);

        // Second tab

        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tabview,null);
        ImageView tabTwoImage = tabTwo.findViewById(R.id.img_tab);
        TextView tabTwoText = tabTwo.findViewById(R.id.tv_tab) ;

        tabTwoImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.icon_gas_o,getActivity().getTheme()));
        tabTwoText.setText("Datos Combustible");

        tabsChecklist.getTabAt(1).setCustomView(tabTwo);

        // Third tab

        LinearLayout tabThree = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tabview,null);
        ImageView tabThreeImage = tabThree.findViewById(R.id.img_tab);
        TextView tabThreeText = tabThree.findViewById(R.id.tv_tab);

        tabThreeImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.icon_cam_o,getActivity().getTheme()));
        tabThreeText.setText("Reporte");

        tabsChecklist.getTabAt(2).setCustomView(tabThree);
    }


    public static class VehicleDetailsFragmentAdapter extends FragmentPagerAdapter {


        public VehicleDetailsFragmentAdapter(FragmentManager fragmentManager){
            super(fragmentManager);

        }
        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    return new CheckListFragment();
                case 1:
                    return new GasInfoFragment();
                case 2:
                    return new ReportFragment();
                default:
                    return null;

            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "CheckList";
                case 1:
                    return "Gas Info";
                case 2:
                    return "Report";
            }

            return null;
        }

    }

}
