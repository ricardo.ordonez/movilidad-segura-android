package co.com.arlsura.personas.movilidad.ui.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.movilidad.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckListFragment extends BaseFragment {


    public CheckListFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_check_list;
    }



}
