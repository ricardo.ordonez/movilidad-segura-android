package co.com.arlsura.personas.model;

/**
 * Created by home on 11/17/17.
 */

public class ResponseSync {
  /*
  *  var message: String!
  var status: String!*/

  private String message;
  private String status;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
