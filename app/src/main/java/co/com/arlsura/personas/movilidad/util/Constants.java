package co.com.arlsura.personas.movilidad.util;

/**
 * Created by Eduar 16-july-2018.
 */

public class Constants {
    public static String tiposDocumento =
            "[\n" +
                    "    {\"value\":\"C\",\"cdTipoDocumentoGobierno\":\"CC\",\"text\":\"Cédula de ciudadanía\",\"minimo\":3,\"maximo\":11,\"patron\":\"^[0-9]{3,11}$\",\"type\":\"numeric\",\"minAge\":18,\"maxAge\":120},\n" +
                    "\t{\"value\":\"N\",\"cdTipoDocumentoGobierno\":\"NI\",\"text\":\"NIT\",\"minimo\":8,\"maximo\":15,\"patron\":\"^[0-9]{8,15}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "    {\"value\":\"E\",\"cdTipoDocumentoGobierno\":\"CE\",\"text\":\"Cédula de extranjería\",\"minimo\":3,\"maximo\":6,\"patron\":\"^[0-9]{3,6}$\",\"type\":\"alphanumeric\",\"minAge\":18,\"maxAge\":120},\n" +
                    "\t{\"value\":\"T\",\"cdTipoDocumentoGobierno\":\"TI\",\"text\":\"Tarjeta de identidad\",\"minimo\":8,\"maximo\":11,\"patron\":\"^[0-9]{8,11}$\",\"type\":\"numeric\",\"minAge\":7,\"maxAge\":19},\n" +
                    "    {\"value\":\"D\",\"cdTipoDocumentoGobierno\":\"CD\",\"text\":\"Carné diplomático\",\"minimo\":3,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{3,11}$\",\"type\":\"alphanumeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "\t{\"value\":\"U\",\"cdTipoDocumentoGobierno\":\"NU\",\"text\":\"N.U.I.P\",\"minimo\":9,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{9,11}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "\t{\"value\":\"P\",\"cdTipoDocumentoGobierno\":\"PA\",\"text\":\"Pasaporte\",\"minimo\":6,\"maximo\":11,\"patron\":\"^[A-Za-z0-9]{6,11}$\",\"type\":\"alphanumeric\",\"minAge\":0,\"maxAge\":18},\n" +
                    "\t{\"value\":\"Z\",\"cdTipoDocumentoGobierno\":\"PE\",\"text\":\"Permiso especial\",\"minimo\":3,\"maximo\":16,\"patron\":\"^[0-9]{3,16}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120},\n" +
                    "    {\"value\":\"I\",\"cdTipoDocumentoGobierno\":\"RC\",\"text\":\"Registro civil\",\"minimo\":10,\"maximo\":11,\"patron\":\"^[0-9]{10,11}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":18},\n" +
                    "    {\"value\":\"S\",\"cdTipoDocumentoGobierno\":\"SC\",\"text\":\"Salvoconducto de permanencia\",\"minimo\":9,\"maximo\":9,\"patron\":\"^[0-9]{9}$\",\"type\":\"numeric\",\"minAge\":0,\"maxAge\":120}\n" +
                    "]";
}
