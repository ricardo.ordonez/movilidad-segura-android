package co.com.arlsura.personas.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.util.Constants;
import segmented_control.widget.custom.android.com.segmentedcontrol.SegmentedControl;
import timber.log.Timber;

public class NetworkConfiguration extends BaseActivity {

  @BindView(R.id.toolbarConfiguration) Toolbar toolbarConfiguration;
  @BindView(R.id.segmented_control) SegmentedControl segmentedControl;

  @Override public int getLayoutId() {
    return R.layout.activity_network_configuration;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbarConfiguration);
    toolbarConfiguration.setNavigationOnClickListener(view -> finish());
    restoreUi();
    initUi();
  }

  private void restoreUi() {
    SharedPreferences sharedPref =
        this.getSharedPreferences(Constants.Network, Context.MODE_PRIVATE);

    int value = sharedPref.getInt(Constants.NetworkKey, Constants.NetworkAll);
    Timber.e("restoreUi " + getTypeStr(value));
    segmentedControl.setSelectedSegment(value);
  }

  @SuppressWarnings("unchecked") private void initUi() {
    segmentedControl.addOnSegmentSelectListener((segmentViewHolder, b, b1) -> {
      Timber.e("onSegmentSelected ".concat(segmentViewHolder.getSegmentData().toString()));
      SharedPreferences sharedPref =
          this.getSharedPreferences(Constants.Network, Context.MODE_PRIVATE);

      SharedPreferences.Editor editor = sharedPref.edit();

      editor.putInt(Constants.NetworkKey,
          getTypeNetwork(segmentViewHolder.getSegmentData().toString()));

      Timber.e("save preference " + getTypeNetwork(segmentViewHolder.getSegmentData().toString()));
      editor.apply();
    });
  }

  private int getTypeNetwork(String type) {
    return type.equalsIgnoreCase(getString(R.string.type_all)) ? Constants.NetworkAll
        : Constants.NetWorkWifiOnly;
  }

  private String getTypeStr(int type) {
    return type == Constants.NetworkAll ? getString(R.string.type_all)
        : getString(R.string.type_wifi);
  }
}
