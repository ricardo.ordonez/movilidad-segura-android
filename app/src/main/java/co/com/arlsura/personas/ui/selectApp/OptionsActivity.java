package co.com.arlsura.personas.ui.selectApp;

import android.content.Intent;

import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.movilidad.navigation.MovilidadNavigation;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.util.CircleIndicator;

public class OptionsActivity extends AppCompatActivity {

    FragmentPagerAdapter adapterViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        setSliderInfo();


    }

    public void startObserverApp(View view) {
        Intent personas = new Intent(OptionsActivity.this, SelectProfile.class);
        startActivity(personas);
    }

    public void startMovilidadApp(View view){

        //MovilidadNavigation.startMovilidadLogin(this);
    }



    private void setSliderInfo(){
        ViewPager vpPager = (ViewPager) findViewById(R.id.vpSlider);
        adapterViewPager = new FragmentSliderAdapter(getSupportFragmentManager());
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        vpPager.setAdapter(adapterViewPager);
        indicator.setViewPager(vpPager);

    }


}




