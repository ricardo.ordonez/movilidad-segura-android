package co.com.arlsura.personas.ui.observer.basicData;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.ApiService;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.configuration.Config;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.dao.WorkerObserverDao;
import co.com.arlsura.personas.model.RequestError;
import co.com.arlsura.personas.model.ResponseSync;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.WorkerObservation;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.ComportamientosProcesos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.RootNivel1;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.util.Constants;
import co.com.arlsura.personas.util.HeaderUtil;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.google.gson.reflect.TypeToken;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.realm.Realm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by home on 11/8/17.
 */

public class BasicDataPresenter {
  private ApiService api;
  private ProcesoDao dao;
  private BasicDataContract contract;
  enum SYNC_OBSERVER_STATE{
    WAITING_FOR_SYNCING,
    SYNCING
  }
  enum SYNC_WORKER_STATE{
    WAITING_FOR_SYNCING,
    SYNCING
  }

  public static PublishSubject<Boolean> dataSynced = PublishSubject.create();


  private static SYNC_OBSERVER_STATE syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;
  private static SYNC_WORKER_STATE syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;

  public boolean observerSyncing(){
    return syncstateObserver == SYNC_OBSERVER_STATE.SYNCING;
  }

  public BasicDataPresenter(ApiService api, ProcesoDao dao, BasicDataContract contract) {
    this.api = api;
    this.dao = dao;
    this.contract = contract;
  }

  public void loadNiveles() {
    UserDao dao = new UserDao();
    User user = dao.findFirst();
    if (user != null) {
      String token = user.token;

      if (Config.isUiTest()) {

        contract.setLoading();

        RootNivel1 item = WebServices.getGson().fromJson(Constants.jsonNiveles, RootNivel1.class);
        List<Nivel1> nivel1List = new ArrayList<>();
        nivel1List.addAll(item.getNiveles1());
        contract.onLoadNiveles(nivel1List);

        contract.hideLoading();
      } else {

          api.niveles(user.documentCompany, HeaderUtil.getHeader(token))
            .doOnSubscribe(disposable -> contract.setLoading())
            .subscribeOn(Schedulers.io())
            .doOnComplete(() -> contract.hideLoading())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(item -> {
              contract.hideLoading();
              List<Nivel1> nivel1List = new ArrayList<>();
              nivel1List.addAll(item.getNiveles1());
              contract.onLoadNiveles(nivel1List);
            }, throwable -> {
              if (throwable instanceof HttpException) {
                HttpException error = (HttpException) throwable;
                String errorBody = error.response().errorBody().string();
                Timber.e(errorBody);
                RequestError requestError =
                    WebServices.getGson().fromJson(errorBody, RequestError.class);
                contract.onErr(requestError.getDebugMessage());
              } else {
                contract.onErr(R.string.ha_ocurrindo_un_err);
              }
              contract.hideLoading();
            }, () -> contract.hideLoading());
      }
    }
  }

  public void loadAssesmetList() {
    contract.setLoading();
    Proceso procesoRoot = dao.findFirstPending();
    List<Comportamientos> listOut = new ArrayList<>();
    for (ComportamientosProcesos item : procesoRoot.getComportamientosProcesos()) {
      listOut.add(item.getComportamientos());
    }
    contract.onLoad(listOut);
    contract.onLoadRootProceso(procesoRoot);
    contract.hideLoading();
  }

  public void loadAssesmetListById(int id) {
    contract.setLoading();
    Proceso procesoRoot = dao.findById(id);
    List<Comportamientos> listOut = new ArrayList<>();
    for (ComportamientosProcesos item : procesoRoot.getComportamientosProcesos()) {
      listOut.add(item.getComportamientos());
    }
    contract.onLoad(listOut);
    contract.onLoadRootProceso(procesoRoot);
    contract.hideLoading();
  }

  private boolean isNetworkAll(Context context) {
    boolean out;

    SharedPreferences sharedPref =
        context.getSharedPreferences(Constants.Network, Context.MODE_PRIVATE);
    int type = sharedPref.getInt(Constants.NetworkKey, Constants.NetworkAll);
    if (type == Constants.NetworkAll) {
      Timber.e("typeNetowrk preference All");
      out = true;
    } else {
      Timber.e("typeNetowrk preference WifiOnly");
      out = false;
    }

    return out;
  }

  public void sync(Application applicationContext) {
    Timber.e("sync");

    if(syncstateObserver == SYNC_OBSERVER_STATE.SYNCING
            && syncstateWorker == SYNC_WORKER_STATE.SYNCING)
      return;


    ReactiveNetwork.observeNetworkConnectivity(applicationContext.getApplicationContext())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(connectivity -> {
          Timber.e(connectivity.toString());
          final NetworkInfo.State state = connectivity.state();
          final String name = connectivity.typeName();
          Timber.e(String.format("state: %s, typeName: %s", state, name));

          if (isNetworkAll(applicationContext.getApplicationContext())) {
            Timber.e("sync on all network");
            if (state == NetworkInfo.State.CONNECTED) {
              Timber.e("syn over all ok");
              syncAll();
              syncWorker();
            }
          } else {
            Timber.e("sync on Wifi Only");
            if (state == NetworkInfo.State.CONNECTED && name.equalsIgnoreCase(Constants.WIFI)) {
              Timber.e("sync on Wifi Only OK");
              syncAll();
              syncWorker();
            }
          }
        });
  }

  public void sync(Context applicationContext) {
    Timber.e("sync");

    if(contract != null)
      contract.setLoading();


    ReactiveNetwork.observeNetworkConnectivity(applicationContext.getApplicationContext())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(connectivity -> {
          Timber.e(connectivity.toString());
          final NetworkInfo.State state = connectivity.state();
          final String name = connectivity.typeName();
          Timber.e(String.format("state: %s, typeName: %s", state, name));

          if (isNetworkAll(applicationContext.getApplicationContext())) {
            Timber.e("sync on all network");
            if (state == NetworkInfo.State.CONNECTED) {
              Timber.e("syn over all ok");
              if(contract != null)
                contract.hideLoading();

              syncAll();
              syncWorker();
            }
          } else {
            Timber.e("sync on Wifi Only");
            if (state == NetworkInfo.State.CONNECTED && name.equalsIgnoreCase(Constants.WIFI)) {
              Timber.e("sync on Wifi Only OK");
              syncAll();
              syncWorker();
            }
          }
        });
  }

  private Map<String, Object> getMapWorker(WorkerObservation workerObservation) {

    return WebServices.getGson()
        .fromJson(workerObservation.getParams(), new TypeToken<HashMap<String, Object>>() {
        }.getType());
  }

  public boolean ignoreSync = false;

  public static boolean syncWorker = false;

  public void syncWorkerIgnoreRules(){
    syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
    syncWorker();
  }

  public void syncWorker() {

      if(syncstateWorker == SYNC_WORKER_STATE.SYNCING && !ignoreSync)
          return;

    syncstateWorker = SYNC_WORKER_STATE.SYNCING;

    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
    List<WorkerObservation> observationList = workerObserverDao.findPendingSync();
    if (observationList != null && !observationList.isEmpty()) {
      ArrayList<Map<String, Object>> jsonArray = new ArrayList<>();

      for (WorkerObservation item : observationList) {
        jsonArray.add(getMapWorker(item));
      }
      Map<String, Object> objectMap = new HashMap<>();
      objectMap.put("array", jsonArray);
      UserDao dao = new UserDao();
      String token = (dao.findFirst() != null) ? dao.findFirst().token :observationList.get(0).getToken();
      String json = WebServices.getGson().toJson(objectMap);
      Timber.e("json sync worker ".concat(json));
      /*api.syncWorker(objectMap, HeaderUtil.getHeader(token))
          .doOnSubscribe(disposable -> {
            contract.setLoading();
          })
          .subscribeOn(Schedulers.io())
          .doOnComplete(() -> {
            contract.hideLoading();
            syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
          })
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(item -> {
            Timber.e("syncAll onNext ", item);
            if (item.getStatus().equalsIgnoreCase("200")) {
              List<WorkerObservation> list = workerObserverDao.findPendingSync();
              for (WorkerObservation workerItem : list) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                workerItem.setStatus(StatusSync.Succes.toString());
                realm.commitTransaction();
                realm.close();
                Timber.e("syncAll Worker ChangeStatus ".concat(workerItem.getStatus())
                    + " ID "
                    + workerItem.getId());
              }
              contract.hideLoading();
                syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;

            }
          }, throwable -> {
            Timber.e(throwable);
            syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;

            Timber.e("syncAll Worker onErr ", throwable);
            contract.hideLoading();
          }, () -> contract.hideLoading());
          */

      api.syncWorker(objectMap, HeaderUtil.getHeader(token))
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(new Observer<ResponseSync>() {
                @Override
                public void onSubscribe(Disposable d) {

                  contract.setLoading();
                }

                @Override
                public void onNext(ResponseSync item) {
                  // Timber.e("syncAll onNext ", item);
                  if (item.getStatus().equalsIgnoreCase("200")) {
                    List<WorkerObservation> list = workerObserverDao.findPendingSync();
                    for (WorkerObservation workerItem : list) {
                      Realm realm = Realm.getDefaultInstance();
                      realm.beginTransaction();
                      workerItem.setStatus(StatusSync.Succes.toString());
                      realm.commitTransaction();
                      realm.close();
                      Timber.e("syncAll Worker ChangeStatus ".concat(workerItem.getStatus())
                              + " ID "
                              + workerItem.getId());
                    }

                    syncWorker = true;

                  }
                  contract.hideLoading();
                  syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
                }

                @Override
                public void onError(Throwable e) {
                  Timber.e(e);
                  contract.hideLoading();
                  syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
                  syncWorker = false;
                  //  Timber.e("syncAll Worker onErr ", e);
                  contract.hideLoading();
                }

                @Override
                public void onComplete() {
                  syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
                  contract.hideLoading();
                }
              });


    }

  }

  public void postWorker(Map<String,Object> params, String token){

      ArrayList<Map<String, Object>> jsonArray = new ArrayList<>();
      jsonArray.add(params);
      Map<String, Object> objectMap = new HashMap<>();
      objectMap.put("array", jsonArray);


      WorkerObserverDao workerObserverDao = new WorkerObserverDao();

    api.syncWorker(objectMap, HeaderUtil.getHeader(token))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<ResponseSync>() {
              @Override
              public void onSubscribe(Disposable d) {

                contract.setLoading();
              }

              @Override
              public void onNext(ResponseSync item) {
                // Timber.e("syncAll onNext ", item);
                if (item.getStatus().equalsIgnoreCase("200")) {
                    WorkerObserverDao workerObserverDao = new WorkerObserverDao();
                    List<WorkerObservation> observationList = workerObserverDao.findPendingSync();
                    for (WorkerObservation workerItem : observationList) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        workerItem.setStatus(StatusSync.Succes.toString());
                        realm.commitTransaction();
                        realm.close();
                        Timber.e("syncAll Worker ChangeStatus ".concat(workerItem.getStatus())
                                + " ID "
                                + workerItem.getId());
                    }
                  syncWorker = true;

                }
                contract.hideLoading();
                syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
              }

              @Override
              public void onError(Throwable e) {
                Timber.e(e);
                contract.hideLoading();
                syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
                syncWorker = false;
                //  Timber.e("syncAll Worker onErr ", e);
                contract.hideLoading();
              }

              @Override
              public void onComplete() {
                syncstateWorker = SYNC_WORKER_STATE.WAITING_FOR_SYNCING;
                contract.hideLoading();
              }
            });
  }
  private void syncAll() {

    if (syncstateObserver == SYNC_OBSERVER_STATE.SYNCING)
      return;


    List<Proceso> list = dao.findPendingSync();
    ArrayList<Map<String, Object>> jsonArray = new ArrayList<>();
    if (list != null && !list.isEmpty()) {
      for (Proceso item : list) {
        jsonArray.add(getJsonObjectValoracion(item));
      }
      Map<String, Object> objectMap = new HashMap<>();
      objectMap.put("array", jsonArray);
      String token = (dao.findFirst() != null) ? dao.findFirst().getToken() : list.get(0).getToken();

      /*
      api.syncValoraciones(objectMap, HeaderUtil.getHeader(token))
          .doOnSubscribe(disposable -> {
            contract.setLoading();
          })
          .subscribeOn(Schedulers.io())
          .doOnComplete(() -> {
            syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;
            contract.hideLoading();
          })
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(item -> {
            Timber.e("syncAll onNext ", item);

            if (item.getStatus().equalsIgnoreCase("200")) {
              List<Proceso> procesoList = dao.findPendingSync();
              for (Proceso proceso : procesoList) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                proceso.setStatus(StatusSync.Succes.toString());
                realm.commitTransaction();
                realm.close();
                Timber.e("syncAll Proceso ChangeStatus ".concat(proceso.getStatus())
                    + " ID "
                    + proceso.getId());
              }
              contract.hideLoading();
              syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;

            }
          }, throwable -> {
            Timber.e("syncAll onErr ", throwable);
              syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;

              contract.hideLoading();
          }, () -> contract.hideLoading());
    }
    */

      api.syncValoraciones(objectMap, HeaderUtil.getHeader(token))
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(new Observer<ResponseSync>() {
                @Override
                public void onSubscribe(Disposable d) {
                  contract.setLoading();
                  syncstateObserver = SYNC_OBSERVER_STATE.SYNCING;
                }

                @Override
                public void onNext(ResponseSync item) {

                  if (item.getStatus().equalsIgnoreCase("200")) {

                    dataSynced.onNext(true);
                    List<Proceso> procesoList = dao.findPendingSync();
                    for (Proceso proceso : procesoList) {
                      Realm realm = Realm.getDefaultInstance();
                      realm.beginTransaction();
                      proceso.setStatus(StatusSync.Succes.toString());
                      realm.commitTransaction();
                      realm.close();
                      Timber.e("syncAll Proceso ChangeStatus ".concat(proceso.getStatus())
                              + " ID "
                              + proceso.getId());
                    }
                  }
                  syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;
                  contract.hideLoading();
                  dataSynced.onNext(false);
                }

                @Override
                public void onError(Throwable e) {
                  dataSynced.onNext(false);
                  contract.hideLoading();
                  syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;
                }

                @Override
                public void onComplete() {


                  syncstateObserver = SYNC_OBSERVER_STATE.WAITING_FOR_SYNCING;
                }
              });


    }
  }

  private Map<String, Object> getJsonObjectValoracion(Proceso proceso) {
    Map<String, Object> json = WebServices.getGson()
        .fromJson(proceso.getParams(), new TypeToken<HashMap<String, Object>>() {
        }.getType());

    ArrayList<Map<String, Object>> valoraciones = new ArrayList<>();

    for (ComportamientosProcesos comportamiento : proceso.getComportamientosProcesos()) {
      Map<String, Object> keyValue = new HashMap<>();

      keyValue.put("calificacion", comportamiento.getComportamientos().getValoracion());
      keyValue.put("condicion", comportamiento.getComportamientos().getCompartamientoInseguro());
      keyValue.put("observacion", comportamiento.getComportamientos().getObservacion());
      keyValue.put("descripcionSituacion",
          comportamiento.getComportamientos().getDescripcionSituacion());
      keyValue.put("estado",  "Abierto"); //comportamiento.getComportamientos().getEstado());
      keyValue.put("planMejora", comportamiento.getComportamientos().getPlanMejora());
      keyValue.put("accionesSugeridas", "");
      keyValue.put("accionesTomaron", "");
      keyValue.put("responsableAccion", "");
      keyValue.put("fechaAccion", json.get("fechaRegistro").toString());
      keyValue.put("idComportamientos", comportamiento.getComportamientos().getId());

      valoraciones.add(keyValue);
    }
    json.put("valoraciones", valoraciones);

    return json;
  }
}
