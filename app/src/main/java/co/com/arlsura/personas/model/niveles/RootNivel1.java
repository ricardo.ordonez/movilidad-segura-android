package co.com.arlsura.personas.model.niveles;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by home on 11/8/17.
 */

public class RootNivel1 extends RealmObject {

  private RealmList<Nivel1> niveles1;

  public RealmList<Nivel1> getNiveles1() {
    return niveles1;
  }

  public void setNiveles1(RealmList<Nivel1> niveles1) {
    this.niveles1 = niveles1;
  }
}
