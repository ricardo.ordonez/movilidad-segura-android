package co.com.arlsura.personas.ui;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.BuildConfig;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import co.com.arlsura.personas.ui.defaultData.DefaultDataPresenter;
import co.com.arlsura.personas.ui.defaultData.defaultDataContract;
import co.com.arlsura.personas.ui.observer.History;
import co.com.arlsura.personas.ui.observer.Login;
import co.com.arlsura.personas.ui.worker.IngressExternal;
import co.com.arlsura.personas.ui.worker.IngressIntern;
import co.com.arlsura.personas.ui.worker.LoginWorker;
import java.util.List;
import timber.log.Timber;

public class SelectProfile extends BaseActivity implements defaultDataContract {

  @BindView(R.id.toolbar2) Toolbar toolbar;
  @BindView(R.id.btnObserver) Button btnObserver;
  @BindView(R.id.btnWorker) Button btnWorker;
  @BindView(R.id.imageView) ImageView imageView;
  private DefaultDataPresenter presenter;

  @Override public int getLayoutId() {
    return R.layout.activity_select_profile;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initPresenter();
    presenter.loadPreguntas();
   // backupRealm();
  }

  private void initPresenter() {
    presenter = new DefaultDataPresenter(this, WebServices.getApi(), new PreguntasDao());
    User user = new UserDao().findFirst();


  }

  @OnClick({ R.id.btnObserver, R.id.btnWorker, R.id.btnConfiguracion })
  public void onViewClicked(View view) {
    switch (view.getId()) {
      case R.id.btnObserver:
        goActv(Login.class, false);
        break;
      case R.id.btnWorker:
        goActv(LoginWorker.class, false);
        break;
      case R.id.btnConfiguracion:
        goActv(NetworkConfiguration.class, false);
        break;
    }
  }

  @Override public void setLoading() {

  }

  @Override public void hideLoading() {

  }

  @Override public void onErr(String err) {
    showAlertDialog(err);
  }

  @Override public void onErr(int err) {
    showAlertDialog(err);
  }

  @Override public void onLoadPreguntas(List<Preguntas> preguntasList) {
    Timber.d("onLoadPreguntas");
    int size = presenter.findRespuestasById("edtValoracion").size();
    Timber.d("edtTipoRespuesta ".concat(String.valueOf(size)));
  }
}
