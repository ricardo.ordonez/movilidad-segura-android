package co.com.arlsura.personas.movilidad.ui;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import co.com.arlsura.personas.movilidad.ui.fragments.FragmentBicycle;
import co.com.arlsura.personas.movilidad.ui.fragments.FragmentMotorBike;
import co.com.arlsura.personas.movilidad.ui.fragments.FragmentVehicle;
import co.com.arlsura.personas.R;

public class CreateVehicle extends AppCompatActivity {

    private ImageView imageCar, imageMotorBike, imageBicycle, arrowBack;
    private RelativeLayout relativeContainerCar, relativeContainerBike, relativeContainerBicycle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_vehicle);
        imageCar = (ImageView) findViewById(R.id.imageCar);
        imageMotorBike = (ImageView) findViewById(R.id.imageMotorBike);
        imageBicycle = (ImageView) findViewById(R.id.imageBicycle);
        arrowBack = (ImageView) findViewById(R.id.arrowBack);

        relativeContainerCar = (RelativeLayout) findViewById(R.id.relativeContainerCar);
        relativeContainerBike = (RelativeLayout) findViewById(R.id.relativeContainerBike);
        relativeContainerBicycle = (RelativeLayout) findViewById(R.id.relativeContainerBicycle);



        startMethod();
    }

    private void startMethod() {
        relativeContainerCar.setBackgroundColor(Color.parseColor("#DADADA"));
        relativeContainerBike.setBackgroundColor((Color.WHITE));
        relativeContainerBicycle.setBackgroundColor((Color.WHITE));

        FragmentVehicle fragmentVehicle = new FragmentVehicle();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragmentVehicle);
        transaction.commit();
    }


    /*@Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageCar:
                relativeContainerCar.setBackgroundColor(Color.parseColor("#DADADA"));
                relativeContainerBike.setBackgroundColor((Color.WHITE));
                relativeContainerBicycle.setBackgroundColor((Color.WHITE));
                FragmentVehicle fragmentVehicle = new FragmentVehicle();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentContainer, fragmentVehicle);
                transaction.commit();
                break;

            case R.id.imageMotorBike:
                relativeContainerBike.setBackgroundColor(Color.parseColor("#DADADA"));
                relativeContainerCar.setBackgroundColor((Color.WHITE));
                relativeContainerBicycle.setBackgroundColor((Color.WHITE));
                FragmentMotorBike fragmentMotorBike = new FragmentMotorBike();
                FragmentTransaction transactionBike = getSupportFragmentManager().beginTransaction();
                transactionBike.replace(R.id.fragmentContainer, fragmentMotorBike);
                transactionBike.commit();
                break;

            case R.id.imageBicycle:
                relativeContainerBicycle.setBackgroundColor(Color.parseColor("#DADADA"));
                relativeContainerCar.setBackgroundColor((Color.WHITE));
                relativeContainerBike.setBackgroundColor((Color.WHITE));
                FragmentBicycle fragmentBicycle = new FragmentBicycle();
                FragmentTransaction transactionBicycle = getSupportFragmentManager().beginTransaction();
                transactionBicycle.replace(R.id.fragmentContainer, fragmentBicycle);
                transactionBicycle.commit();
                break;

            case R.id.arrowBack:
                onBackPressed();
                break;
        }
    }
    */
}
