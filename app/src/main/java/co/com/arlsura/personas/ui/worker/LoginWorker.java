package co.com.arlsura.personas.ui.worker;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.TipoDocumento;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.ui.observer.login.LoginContract;
import co.com.arlsura.personas.ui.observer.login.LoginPresenter;

import com.jakewharton.rxbinding2.widget.RxTextView;

import co.com.arlsura.personas.util.Constants;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class LoginWorker extends BaseActivity implements LoginContract {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.edtTipoDocumentoEmpresa) Spinner edtTipoDocumentoEmpresa;
  @BindView(R.id.edtTipoDocumentoPersona) Spinner edtEdtTipoDocumentoPersona;

  @BindView(R.id.edtNumeroDocumentoEmpresa) EditText edtNumeroDocumentoEmpresa;
  @BindView(R.id.edtNumeroDocumentoPersona) EditText edtNumeroDocumentoPersona;
  @BindView(R.id.btnLogin) Button btnLogin;
  private LoginPresenter presenter;

  @Override public int getLayoutId() {
    return R.layout.activity_login_worker;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initPresenter();
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(view -> finish());
    initUi();


    /*if (Config.isUiTest()) {
    edtNumeroDocumentoEmpresa.setText("800256161");
    edtNumeroDocumentoPersona.setText("1152198279");
    edtPasswordPersona.setText("1037606402");
    }*/
  }

  private void initUi() {
    List<String> itemsTipoDocumento = new ArrayList<>();
/*
    List<Respuesta> list = presenter.findRespuestasById("edtTypeDocument");

    if (list != null && !list.isEmpty()) {
      for (Respuesta item : list) {
        itemsTipoDocumento.add(item.getValor());
      }
    }
*/

    itemsTipoDocumento.addAll(presenter.getTiposArray());

    ArrayAdapter<String> adapterCity =
        new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, itemsTipoDocumento);
    adapterCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    edtTipoDocumentoEmpresa.setAdapter(adapterCity);
    edtTipoDocumentoEmpresa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Timber.d("tipoDoumento ".concat(itemsTipoDocumento.get(i)));
      }

      @Override public void onNothingSelected(AdapterView<?> adapterView) {
        Timber.d("tipoDoumento onNothingSelected ");
      }
    });

    edtTipoDocumentoEmpresa.setSelection(1); //NIT



    edtEdtTipoDocumentoPersona.setAdapter(adapterCity);
    edtEdtTipoDocumentoPersona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Timber.d("tipoDoumento ".concat(itemsTipoDocumento.get(i)));
      }

      @Override public void onNothingSelected(AdapterView<?> adapterView) {
        Timber.d("tipoDoumento onNothingSelected ");
      }
    });
    edtEdtTipoDocumentoPersona.setSelection(0); //CEDULA



    Observable.combineLatest(RxTextView.textChangeEvents(edtNumeroDocumentoEmpresa),
        RxTextView.textChangeEvents(edtNumeroDocumentoPersona),
        (documentoEmpresa, documentoPerona) -> {
          boolean documentoEmpresaCheck = documentoEmpresa.text().length() > 6;
          boolean documentoPeronaCheck = documentoPerona.text().length() > 6;
          return documentoEmpresaCheck && documentoPeronaCheck;
        }).subscribe(aBoolean -> isEnabled(btnLogin, aBoolean));
  }

  private void initPresenter() {
    presenter = new LoginPresenter(this, WebServices.getApi(), new UserDao());
  }

  @Override public void setLoading() {
    Timber.d("setLoading");
    showLoading(getString(R.string.loading));
  }

  @Override public void hideLoading() {
    Timber.d("hideLoading");
    dissmisLoading();
  }

  @Override public void onErr(String err) {
    Timber.d("onErr");
    showAlertDialog(err);
  }

  @Override public void onErr(int err) {
    Timber.d(getString(err));
    showAlertDialog(err);
  }

  @Override public void onLoadUser(User user) {
    //Timber.d("onLoadUser", user);
    if (user.isIntern()) {
      Timber.e(" user Internal Worker");
      goActv(IngressIntern.class, true);
    } else {
      Timber.e(" user Exnternal Worker");
      goActv(IngressExternal.class, true);
    }
  }

  @Override public void onSuccesLogOut() {

  }

  @OnClick(R.id.btnLogin) public void onViewClicked() {
    if ((Constants.userFailDefault.equalsIgnoreCase(edtNumeroDocumentoPersona.getText().toString())))
    {
      showAlertDialog(R.string.usuario_no_autorizado);
    }
    else
      {
      Map<String, Object> param = new HashMap<>();
      List<TipoDocumento> listaDocumentos = presenter.getTiposDocumentos();

      String username =  edtNumeroDocumentoPersona.getText().toString();

      String document = edtNumeroDocumentoEmpresa.getText().toString();

      param.put("username", username);
      param.put("document", document);
      presenter.loginWorker(param);
    }
  }
}
