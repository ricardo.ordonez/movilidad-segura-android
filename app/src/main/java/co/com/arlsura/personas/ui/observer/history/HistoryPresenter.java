package co.com.arlsura.personas.ui.observer.history;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.ApiService;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.configuration.Config;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.Historial;
import co.com.arlsura.personas.model.RequestError;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.ui.observer.BasicData;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import co.com.arlsura.personas.util.Constants;
import co.com.arlsura.personas.util.HeaderUtil;
import com.google.gson.reflect.TypeToken;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by home on 11/8/17.
 */

public class HistoryPresenter {
  private ApiService api;
  private HistoryContract contract;

  public HistoryPresenter(ApiService api, HistoryContract contract) {
    this.api = api;
    this.contract = contract;
    //observeDataSynced();
  }


  public void loadUser() {
    UserDao dao = new UserDao();
    User user = dao.findFirst();
    if (user != null) contract.onLoadUser(user);
  }

  public void loadHistorial() {
    UserDao dao = new UserDao();
    User user = dao.findFirst();
    if (user != null) {
      String token = user.token;

      if (Config.isUiTest()) {

        contract.setLoading();
        Type listType = new TypeToken<ArrayList<Historial>>() {
        }.getType();
        List<Historial> list = WebServices.getGson().fromJson(Constants.jsonHistorial, listType);
        contract.onLoadHistorial(list);
        contract.hideLoading();
      } else {

        api.historial(HeaderUtil.getHeader(token))
            .subscribeOn(Schedulers.io())
            .doOnComplete(() -> contract.hideLoading())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<Historial>>() {
              @Override
              public void onSubscribe(Disposable d) {
                contract.setLoading();
              }

              @Override
              public void onNext(List<Historial> list) {
                contract.onLoadHistorial(list);
                contract.hideLoading();
              }

              @Override
              public void onError(Throwable throwable) {
                contract.hideLoading();
                if (throwable instanceof HttpException) {
                  HttpException error = (HttpException) throwable;
                  String errorBody = null;
                  try {
                    errorBody = error.response().errorBody().string();
                  } catch (IOException e) {
                    e.printStackTrace();
                  }

                  Timber.e(errorBody);
                  RequestError requestError =
                          WebServices.getGson().fromJson(errorBody, RequestError.class);

                  if(requestError.getStatus().equals("401")){
                    contract.onErr("En estos momentos no tienes modulos asociados, comunícate con el administrador o ingresa como trabajador");
                    contract.close();
                  }else{
                    contract.onErr(requestError.getDebugMessage());
                  }



                } else {
                  contract.onErr(R.string.ha_ocurrindo_un_err);
                }

              }

              @Override
              public void onComplete() {
                contract.hideLoading();
              }
            });
      }
    }
  }
}
