package co.com.arlsura.personas.movilidad.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import co.com.arlsura.personas.R;
import timber.log.Timber;

public class CreateVehicleEnterprice extends AppCompatActivity implements com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private EditText etFechaMatricula, etFechaSoat, etFechaVenSeguro, etFechaVenExtintor, etFechaVenTenomecanico;
    private EditText etPlaca, etMarca, etModelo, etLinea;
    private Button btnGuardarVehiculo;
    private int opcionFecha = 0;
    private SimpleDateFormat formatoDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private SpinnerDatePickerDialogBuilder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_vehicle_enterprice);
        initUi();
    }

    private void initUi() {
        etFechaMatricula = (EditText) findViewById(R.id.etFechaMatricula);
        etFechaSoat = (EditText) findViewById(R.id.etFechaSoat);
        etFechaVenSeguro = (EditText) findViewById(R.id.etFechaVenSeguro);
        etFechaVenExtintor = (EditText) findViewById(R.id.etFechaVenExtintor);
        etFechaVenTenomecanico = (EditText) findViewById(R.id.etFechaVenTenomecanico);
        btnGuardarVehiculo = (Button) findViewById(R.id.btnGuardarVehiculo);
        etPlaca = (EditText) findViewById(R.id.etPlaca);
        etMarca = (EditText) findViewById(R.id.etMarca);
        etModelo = (EditText) findViewById(R.id.etModelo);
        etLinea = (EditText) findViewById(R.id.etLinea);

        etFechaMatricula.setOnClickListener(this);
        etFechaSoat.setOnClickListener(this);
        etFechaVenSeguro.setOnClickListener(this);
        etFechaVenExtintor.setOnClickListener(this);
        etFechaVenTenomecanico.setOnClickListener(this);
        btnGuardarVehiculo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        opcionFecha = view.getId();
        switch (opcionFecha) {
            case R.id.etFechaMatricula:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaSoat:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaVenSeguro:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaVenExtintor:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.etFechaVenTenomecanico:
                createDatePickerDialog();
                builder.build().show();
                break;

            case R.id.btnGuardarVehiculo:
                if (validateFields()) {
                    Intent vehicleMovilidad = new Intent(getBaseContext(), VehicleMovilidad.class);
                    vehicleMovilidad.putExtra("llave", "CreateVehicleMovilidad");
                    startActivity(vehicleMovilidad);
                }
                break;
        }
    }

    private boolean validateFields() {

        boolean validacion = true;

        if (!(!etPlaca.getText().toString().trim().isEmpty() && etPlaca.getText().toString().length() == 6)) {
            validacion = false;
            etPlaca.setError("La placa no se ha diligenciado correctamente");
        }
        if (!(!etMarca.getText().toString().trim().isEmpty() && etMarca.getText().toString().length() >= 3)) {
            validacion = false;
            etMarca.setError("La marca no se ha diligenciado correctamente");
        }
        if (!(!etModelo.getText().toString().trim().isEmpty() && etModelo.getText().toString().length() >= 4)) {
            validacion = false;
            etModelo.setError("El modelo no se ha diligenciado correctamente");
        }
        if (!(!etLinea.getText().toString().trim().isEmpty() && etLinea.getText().toString().length() >= 1)) {
            validacion = false;
            etLinea.setError("La linea no se ha diligenciado correctamente");
        }
        if (!(!etFechaMatricula.getText().toString().trim().isEmpty() && etFechaMatricula.getText().toString().length() == 10)) {
            validacion = false;
            etFechaMatricula.setError("La fecha de matricula no se ha diligenciado correctamente");
        }
        if (!(!etFechaSoat.getText().toString().trim().isEmpty() && etFechaSoat.getText().toString().length() == 10)) {
            validacion = false;
            etFechaSoat.setError("La fecha de vencimiento del soat no se ha diligenciado correctamente");
        }
        if (!(!etFechaVenSeguro.getText().toString().trim().isEmpty() && etFechaVenSeguro.getText().toString().length() == 10)) {
            validacion = false;
            etFechaVenSeguro.setError("La fecha de vencimiento del seguro no se ha diligenciado correctamente");
        }
        if (!(!etFechaVenExtintor.getText().toString().trim().isEmpty() && etFechaVenExtintor.getText().toString().length() == 10)) {
            validacion = false;
            etFechaVenExtintor.setError("La fecha de cambio del extintor no se ha diligenciado correctamente");
        }
        if (!(!etFechaVenTenomecanico.getText().toString().trim().isEmpty() && etFechaVenTenomecanico.getText().toString().length() == 10)) {
            validacion = false;
            etFechaVenTenomecanico.setError("La fecha de revisión técnomecanica del no se ha diligenciado correctamente");
        }

        if (validacion) {
            return validacion;
        } else {
            return validacion;
        }
    }

    private void createDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        builder = new SpinnerDatePickerDialogBuilder()
                .context(this)
                .callback(CreateVehicleEnterprice.this)
                .showTitle(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .maxDate(calendar.get(Calendar.YEAR) + 1, 0, 1)
                .minDate(calendar.get(Calendar.YEAR) - 1, 0, 1);
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener =
            (view, year, monthOfYear, dayOfMonth) -> {
                String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear, dayOfMonth);
                try {
                    optionChangeDate(formatoDate.format(formatoDate.parse(date)));
                } catch (ParseException e) {
                    Timber.e(e);
                }
            };

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear + 1, dayOfMonth);
        try {
            optionChangeDate(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
            Timber.e(e);
        }
    }

    private void optionChangeDate(String formatoDate) {
        switch (opcionFecha) {
            case R.id.etFechaMatricula:
                etFechaMatricula.setText(formatoDate);
                break;

            case R.id.etFechaSoat:
                etFechaSoat.setText(formatoDate);
                break;

            case R.id.etFechaVenSeguro:
                etFechaVenSeguro.setText(formatoDate);
                break;

            case R.id.etFechaVenExtintor:
                etFechaVenExtintor.setText(formatoDate);
                break;

            case R.id.etFechaVenTenomecanico:
                etFechaVenTenomecanico.setText(formatoDate);
                break;
        }
    }
}
