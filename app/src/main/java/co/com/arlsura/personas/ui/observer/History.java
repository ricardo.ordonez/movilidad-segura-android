package co.com.arlsura.personas.ui.observer;

import android.app.Application;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.Historial;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import co.com.arlsura.personas.ui.observer.history.HistoryAdapter;
import co.com.arlsura.personas.ui.observer.history.HistoryContract;
import co.com.arlsura.personas.ui.observer.history.HistoryPresenter;
import co.com.arlsura.personas.ui.observer.login.LoginContract;
import co.com.arlsura.personas.ui.observer.login.LoginPresenter;
import java.util.List;

import timber.log.Timber;

public class History extends BaseActivity implements HistoryContract {

  @BindView(R.id.toolbarHisotory) Toolbar toolbarHisotory;
  @BindView(R.id.lblHola) TextView lblHola;
  @BindView(R.id.lblNmaeEmpresa) TextView lblNmaeEmpresa;
  @BindView(R.id.btnNuevaObservacion) Button btnNuevaObservacion;
  @BindView(R.id.listHistorial) RecyclerView listHistorial;
  private LoginPresenter loginPresenter;
  private HistoryPresenter presenter;
  private HistoryAdapter adapter;
  private LinearLayoutManager mLayoutManager;


  @Override public int getLayoutId() {
    return R.layout.activity_history;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initPresenter();
    initUi();
    presenter.loadUser();
    toolbarHisotory.inflateMenu(R.menu.menu_salir);
    toolbarHisotory.setOnMenuItemClickListener(item -> {
      switch (item.getItemId()) {
        case R.id.action_salir:
          showAlertDialog(R.string.confim_exit, new confirmDialog() {
            @Override public void onPositive() {
              loginPresenter.logOut();
            }

            @Override public void onNegative() {

            }
          });
          break;
      }
      return true;
    });
  }

  private void initUi() {
    mLayoutManager = new LinearLayoutManager(this);
    listHistorial.setLayoutManager(mLayoutManager);
  }

  private void initPresenter() {
    loginPresenter = new LoginPresenter(loginContract, WebServices.getApi(), new UserDao());
    presenter = new HistoryPresenter(WebServices.getApi(), this);
  }

  private LoginContract loginContract = new LoginContract() {
    @Override public void onLoadUser(User user) {

    }

    @Override public void onSuccesLogOut() {
      goActv(SelectProfile.class, true);
    }

    @Override public void setLoading() {
      Timber.d("setLoading");
      showLoading(getString(R.string.loading));
    }

    @Override public void hideLoading() {
      Timber.d("hideLoading");
      dissmisLoading();
    }

    @Override public void onErr(String err) {
      Timber.d("onErr");
      showAlertDialog(err);
    }

    @Override public void onErr(int err) {
      Timber.d(getString(err));
      showAlertDialog(err);
    }
  };

  @OnClick(R.id.btnNuevaObservacion) public void onViewClicked() {
    goActv(NewObservation.class, false);
  }

  @Override protected void onResume() {
    super.onResume();
    checkPending();
   presenter.loadHistorial();

  }

  private void checkPending() {
    ProcesoDao dao = new ProcesoDao();
    Proceso proceso = dao.findFirstPending();
    if (proceso != null) {
      showAlertDialogNeutral(R.string.observaciones_pendientes, new confirmDialog() {
        @Override public void onPositive() {
          goActv(AssessmentList.class, false);
        }

        @Override public void onNegative() {

        }
      });
    }
    checkPendingSync();
  }

  private void checkPendingSync() {
    BasicDataPresenter presenter =
        new BasicDataPresenter(WebServices.getApi(), new ProcesoDao(), basicDataContract);

    Application application = this.getApplication();
    if (application != null) {
      presenter.sync(application);
    }


  }

  @Override public void setLoading() {
    Timber.d("setLoading");
    showLoading(getString(R.string.loading));
  }

  @Override public void hideLoading() {
    Timber.d("hideLoading");
    dissmisLoading();

  }

  @Override public void onErr(String err) {
    Timber.d("onErr");
    showAlertDialog(err);
  }

  @Override public void onErr(int err) {
    Timber.d(getString(err));
    showAlertDialog(err);
  }

  @Override public void onLoadHistorial(List<Historial> list) {
    Timber.d("onLoadHistorial ".concat(String.valueOf(list.size())));
    adapter = new HistoryAdapter(this, list);
    runOnUiThread(() -> listHistorial.setAdapter(adapter));
  }

  @Override public void onLoadUser(User user) {
    Resources res = getResources();
    String hello = String.format(res.getString(R.string.hola_usuario), user.name);
    runOnUiThread(() -> {
      lblHola.setText(hello);
      lblNmaeEmpresa.setText(user.companyName);
    });
  }

  @Override
  public void close() {
    startOver();
  }


}
