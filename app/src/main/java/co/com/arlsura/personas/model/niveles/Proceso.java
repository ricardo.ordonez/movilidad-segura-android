package co.com.arlsura.personas.model.niveles;

import co.com.arlsura.personas.api.WebServices;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class Proceso extends RealmObject {

  /**
   * id : 2
   * proceso : METALMECANICA
   * nitEmpresa : 800256161
   * actividadesProcesos : [{"id":2,"actividades":{"id":2,"actividad":"METALMECÁNICA","nitEmpresa":"800256161"}}]
   * comportamientosProcesos : [{"id":2,"comportamientos":{"id":2,"comportamiento":"TRABAJO CON
   * PULIDORA SIN PROTECCIÓN AUDITIVA","estado":"Activo","nitEmpresa":"800256161"}}]
   */
  @PrimaryKey private int id;
  private String proceso;
  private String nitEmpresa;
  private RealmList<actividadesProceso> actividadesProcesos;
  private RealmList<ComportamientosProcesos> comportamientosProcesos;
  private RealmList<EstandaresProceso> estandaresProcesos;

  private String params;
  private String status;
  private String token;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProceso() {
    return proceso;
  }

  public void setProceso(String proceso) {
    this.proceso = proceso;
  }

  public String getNitEmpresa() {
    return nitEmpresa;
  }

  public void setNitEmpresa(String nitEmpresa) {
    this.nitEmpresa = nitEmpresa;
  }

  public RealmList<actividadesProceso> getActividadesProcesos() {
    return actividadesProcesos;
  }

  public void setActividadesProcesos(RealmList<actividadesProceso> actividadesProcesos) {
    this.actividadesProcesos = actividadesProcesos;
  }

  public RealmList<ComportamientosProcesos> getComportamientosProcesos() {
    return comportamientosProcesos;
  }

  public void setComportamientosProcesos(
      RealmList<ComportamientosProcesos> comportamientosProcesos) {
    this.comportamientosProcesos = comportamientosProcesos;
  }

  public RealmList<EstandaresProceso> getEstandaresProcesos() {
    return estandaresProcesos;
  }

  public void setEstandaresProcesos(RealmList<EstandaresProceso> estandaresProcesos) {
    this.estandaresProcesos = estandaresProcesos;
  }

  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

}
