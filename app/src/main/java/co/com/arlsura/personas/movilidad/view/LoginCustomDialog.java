package co.com.arlsura.personas.movilidad.view;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import co.com.arlsura.personas.movilidad.navigation.MovilidadNavigation;
import co.com.arlsura.personas.R;

/**
 * Created by Eduar on 16/07/18.
 */
public class LoginCustomDialog extends DialogFragment {

    private LayoutInflater inflater;
    private View customView;
    private Button btnComenzar;

    public Dialog onCreateDialog(Bundle savedInstanceState){

        inflater = getActivity().getLayoutInflater();
        customView = inflater.inflate(R.layout.custom_dialog_movilidad, null);
        btnComenzar = customView.findViewById(R.id.btnComezar);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        btnComenzar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("LoginCustomDialog", "Iniciando vehiculos");
                MovilidadNavigation.startMovilidadDashboard(getContext());
            }
        });

        builder.setView(customView);

        return builder.create();
    }
}