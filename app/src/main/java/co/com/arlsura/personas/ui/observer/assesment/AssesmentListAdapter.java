package co.com.arlsura.personas.ui.observer.assesment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import com.subinkrishna.widget.CircularImageView;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import java.util.List;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;

/**
 * Created by home on 11/8/17.
 */

public class AssesmentListAdapter extends RecyclerView.Adapter<AssesmentListAdapter.ViewHolder> {
  private Context context;
  private List<Comportamientos> list;

  private final PublishSubject<Integer> onClickSubject = PublishSubject.create();

  @NonNull public Observable<Integer> getPositionClicks() {
    return onClickSubject;
  }

  public AssesmentListAdapter(Context context, List<Comportamientos> list) {
    this.context = context;
    this.list = list;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    final View view = LayoutInflater.from(context).inflate(R.layout.item_assesment, parent, false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    Comportamientos item = list.get(position);

    if (item != null) {
      //  holder.btnStatusAssesment.setText(String.valueOf(position + 1));
      holder.lblDescripcion.setText(item.getComportamiento());

      holder.cardRoot.setOnClickListener(view -> onClickSubject.onNext(position));
      holder.lblDescripcion.setOnClickListener(view -> onClickSubject.onNext(position));
      holder.btnStatusAssesment.setOnClickListener(view -> onClickSubject.onNext(position));

      switch (item.getEstado()) {
        case "Activo":

          holder.btnStatusAssesment.setPlaceholder(String.valueOf(position + 1),
              ContextCompat.getColor(context, R.color.pending),
              ContextCompat.getColor(context, R.color.pendingText));

          holder.btnStatusAssesment.setBorderWidth(COMPLEX_UNIT_DIP, 1);

          holder.btnStatusAssesment.setBorderColor(
              ContextCompat.getColor(context, R.color.pendingBoder));


          break;

        case "Cerrado":

          holder.btnStatusAssesment.setPlaceholder(String.valueOf(position + 1),
              ContextCompat.getColor(context, R.color.succes),
              ContextCompat.getColor(context, R.color.white));

          holder.btnStatusAssesment.setBorderWidth(COMPLEX_UNIT_DIP, 0);

          break;

        case "Pendiente":
          holder.btnStatusAssesment.setPlaceholder(String.valueOf(position + 1),
              ContextCompat.getColor(context, R.color.incomplete),
              ContextCompat.getColor(context, R.color.white));

          holder.btnStatusAssesment.setBorderWidth(COMPLEX_UNIT_DIP, 0);

          break;
      }
    }
  }

  @Override public int getItemCount() {
    return list.size();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.btnStatusAssesment) CircularImageView btnStatusAssesment;
    @BindView(R.id.lblDescripcion) TextView lblDescripcion;
    @BindView(R.id.cardRoot) CardView cardRoot;

    ViewHolder(@NonNull View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
