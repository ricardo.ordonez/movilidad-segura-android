package co.com.arlsura.personas.ui.worker.ingress;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.ApiService;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.configuration.Config;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.Area;
import co.com.arlsura.personas.model.RequestError;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.RootNivel1;
import co.com.arlsura.personas.util.Constants;
import co.com.arlsura.personas.util.HeaderUtil;

import com.google.gson.reflect.TypeToken;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.HttpException;
import timber.log.Timber;

public class IngressPresenter {
    private IngressContract contract;
    private ApiService api;

    public IngressPresenter(IngressContract contract, ApiService api) {
        this.contract = contract;
        this.api = api;
    }

    public void loadUser() {
        UserDao dao = new UserDao();
        User user = dao.findFirst();
        if (user != null) contract.onLoadUser(user);
    }

    public void loadNiveles() {
        UserDao dao = new UserDao();
        User user = dao.findFirst();
        if (user != null) {
            String token = user.token;

            if (Config.isUiTest()) {

                contract.setLoading();

                RootNivel1 item = WebServices.getGson().fromJson(Constants.jsonNiveles, RootNivel1.class);
                List<Nivel1> nivel1List = new ArrayList<>();
                nivel1List.addAll(item.getNiveles1());
                contract.onLoadNiveles(nivel1List);

                contract.hideLoading();
            } else {

                api.niveles(user.documentCompany, HeaderUtil.getHeader(token))
                        .doOnSubscribe(disposable -> contract.setLoading())
                        .subscribeOn(Schedulers.io())
                        .doOnComplete(() -> contract.hideLoading())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(item -> {
                            contract.hideLoading();
                            List<Nivel1> nivel1List = new ArrayList<>();
                            nivel1List.addAll(item.getNiveles1());
                            contract.onLoadNiveles(nivel1List);
                        }, throwable -> {
                            if (throwable instanceof HttpException) {
                                HttpException error = (HttpException) throwable;
                                String errorBody = error.response().errorBody().string();
                                Timber.e(errorBody);
                                RequestError requestError =
                                        WebServices.getGson().fromJson(errorBody, RequestError.class);
                                contract.onErr(requestError.getDebugMessage());

                                if (requestError.getStatus().equals("401")) {
                                    contract.onErr("En estos momentos no tienes modulos asociados, comunícate con el administrador o ingresa como trabajador");

                                    contract.onClose();
                                }

                            } else {
                                contract.onErr(R.string.ha_ocurrindo_un_err);
                            }
                            contract.hideLoading();
                        }, () -> contract.hideLoading());
            }
        }
    }

    public void loadInstalaciones() {
        PreguntasDao preguntasDao = new PreguntasDao();
        List<Respuesta> arr = preguntasDao.findById("edtInstalaciones");

        if (arr != null && !arr.isEmpty()) {
            List<Area> listOut = new ArrayList<>();
            for (Respuesta item : arr) {
                Area area = new Area();

                area.setId(item.getId());
                if (item.getNombre().equals("CLIENTE")) {
                    area.setNombreUbicacion("VISITANTE");

                } else {
                    area.setNombreUbicacion(item.getNombre());

                }
                area.setEstado(item.getValor());

                listOut.add(area);
            }

            contract.onLoadInstalaciones(listOut);
            contract.hideLoading();

        }


    }

    public void loadInstalacionesExterno() {
        PreguntasDao preguntasDao = new PreguntasDao();
//    List<Respuesta> arr = preguntasDao.findById("edtInstalaciones");
//    if (arr != null && !arr.isEmpty()) {
//      List<Area> listOut = new ArrayList<>();
//      for (Respuesta item : arr) {
//        Area area = new Area();
//        area.setNombreUbicacion(item.getNombre());
//        area.setId(item.getId());
//        area.setEstado(item.getValor());
//        listOut.add(area);
//      } }
        List<Area> listOut = new ArrayList<>();
        Area instalacion = new Area();
        instalacion.setEstado("Empresa");
        instalacion.setId(99);
        instalacion.setNombreUbicacion("EMPRESA");
        listOut.add(instalacion);
        contract.onLoadInstalaciones(listOut);


    }

    public void loadAreasExterno() {
        PreguntasDao preguntasDao = new PreguntasDao();
        List<Respuesta> arr = preguntasDao.findById("edtAreas");
        List<Area> listOut = new ArrayList<>();
        if (arr != null && !arr.isEmpty()) {
            for (Respuesta item : arr) {
                Area area = new Area();
                area.setNombreUbicacion(item.getNombre());
                area.setId(item.getId());
                area.setEstado(item.getValor());
                listOut.add(area);
            }
        }
    /*List<Area> listOut = new ArrayList<>();
    Area instalacion = new Area();
    instalacion.setEstado("Empresa");
    instalacion.setId(99);
    instalacion.setNombreUbicacion("EMPRESA");
    listOut.add(instalacion);*/

        contract.onLoadAreas(listOut);

    }

    public void loadAreas() {
        UserDao dao = new UserDao();
        User user = dao.findFirst();
        if (user != null) {
            if (Config.isUiTest()) {
                contract.setLoading();
                Type listType = new TypeToken<ArrayList<Area>>() {
                }.getType();
                List<Area> list = WebServices.getGson().fromJson(Constants.jsonAreas, listType);
                contract.hideLoading();
                contract.onLoadAreas(list);
            } else {
                api.areas(user.documentCompany, HeaderUtil.getHeader(user.token))
                        .doOnSubscribe(disposable -> contract.setLoading())
                        .subscribeOn(Schedulers.io())
                        .doOnComplete(() -> contract.hideLoading())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            contract.hideLoading();
                            contract.onLoadAreas(list);
                        }, throwable -> {
                            if (throwable instanceof HttpException) {
                                HttpException error = (HttpException) throwable;
                                String errorBody = error.response().errorBody().string();
                                Timber.e(errorBody);
                                RequestError requestError =
                                        WebServices.getGson().fromJson(errorBody, RequestError.class);
                                contract.onErr(requestError.getDebugMessage());
                            } else {
                                contract.onErr(R.string.ha_ocurrindo_un_err);
                            }
                            contract.hideLoading();
                        }, () -> contract.hideLoading());
            }
        }
    }
}
