package co.com.arlsura.personas.model.niveles;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */


public  class Actividades extends RealmObject {
  /**
   * id : 2
   * actividad : METALMECÁNICA
   * nitEmpresa : 800256161
   */

  @PrimaryKey
  private int id;
  private String actividad;
  private String nitEmpresa;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getActividad() {
    return actividad;
  }

  public void setActividad(String actividad) {
    this.actividad = actividad;
  }

  public String getNitEmpresa() {
    return nitEmpresa;
  }

  public void setNitEmpresa(String nitEmpresa) {
    this.nitEmpresa = nitEmpresa;
  }
}
