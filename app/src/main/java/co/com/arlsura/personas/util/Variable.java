package co.com.arlsura.personas.util;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by home on 11/15/17.
 */

public class Variable<T> {
  private T value;

  private final PublishSubject<T> serializedSubject = PublishSubject.create();

  public Variable(T value) {
    this.value = value;
  }

  public synchronized T get() {
    return value;
  }

  public synchronized void set(T value) {
    this.value = value;
    serializedSubject.onNext(this.value);
  }

  public Observable<T> asObservable() {
    return serializedSubject;
  }
}
