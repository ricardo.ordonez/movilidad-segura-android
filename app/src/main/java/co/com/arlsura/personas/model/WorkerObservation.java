package co.com.arlsura.personas.model;

import co.com.arlsura.personas.model.niveles.Responsable;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.UUID;

/**
 * Created by home on 11/19/17.
 */

public class WorkerObservation extends RealmObject {
  /*
  *  @objc dynamic var id = 0
  @objc dynamic var params = ""
  @objc dynamic var status = ""
  var responsables = List<Responsable>()
  @objc dynamic var isTipoReporteObligatorio = false
  @objc dynamic var token = ""
  * */

  @PrimaryKey private int id = UUID.randomUUID().hashCode();
  private String params;
  private String status;
  private String token;
  private RealmList<Responsable> responsables;
  private Boolean isTipoReporteObligatorio = false;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public RealmList<Responsable> getResponsables() {
    return responsables;
  }

  public void setResponsables(RealmList<Responsable> responsables) {
    this.responsables = responsables;
  }

  public Boolean getTipoReporteObligatorio() {
    return isTipoReporteObligatorio;
  }

  public void setTipoReporteObligatorio(Boolean tipoReporteObligatorio) {
    isTipoReporteObligatorio = tipoReporteObligatorio;
  }
}
