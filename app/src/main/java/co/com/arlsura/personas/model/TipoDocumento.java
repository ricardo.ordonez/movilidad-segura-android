package co.com.arlsura.personas.model;

import co.com.arlsura.personas.model.defaultData.Respuesta;

public class TipoDocumento
{
    private String cdTipoDocumentoGobierno;

    private String maxAge;

    private String text;

    private String patron;

    private String minAge;

    private String value;

    private String type;

    private String maximo;

    private String minimo;

    public String getCdTipoDocumentoGobierno ()
    {
        return cdTipoDocumentoGobierno;
    }

    public void setCdTipoDocumentoGobierno (String cdTipoDocumentoGobierno)
    {
        this.cdTipoDocumentoGobierno = cdTipoDocumentoGobierno;
    }




    public String getMaxAge ()
    {
        return maxAge;
    }

    public void setMaxAge (String maxAge)
    {
        this.maxAge = maxAge;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getPatron ()
    {
        return patron;
    }

    public void setPatron (String patron)
    {
        this.patron = patron;
    }

    public String getMinAge ()
    {
        return minAge;
    }

    public void setMinAge (String minAge)
    {
        this.minAge = minAge;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getMaximo ()
    {
        return maximo;
    }

    public void setMaximo (String maximo)
    {
        this.maximo = maximo;
    }

    public String getMinimo ()
    {
        return minimo;
    }

    public void setMinimo (String minimo)
    {
        this.minimo = minimo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cdTipoDocumentoGobierno = "+cdTipoDocumentoGobierno+", maxAge = "+maxAge+", text = "+text+", patron = "+patron+", minAge = "+minAge+", value = "+value+", type = "+type+", maximo = "+maximo+", minimo = "+minimo+"]";
    }


}
