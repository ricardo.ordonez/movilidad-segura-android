package co.com.arlsura.personas.ui.observer.assesment;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import java.util.List;

/**
 * Created by home on 11/14/17.
 */

public class AsesmentViewPagerAdapter extends FragmentStatePagerAdapter {
  List<Fragment> fragments;
  Context context;

  public AsesmentViewPagerAdapter(FragmentManager fm, List<Fragment> fragments, Context context) {
    super(fm);
    this.fragments = fragments;
    this.context = context;
  }

  @Override public Fragment getItem(int position) {
    return fragments.get(position);
  }

  @Override public int getCount() {
    return fragments.size();
  }
}
