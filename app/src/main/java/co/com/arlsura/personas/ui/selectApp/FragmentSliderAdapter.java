package co.com.arlsura.personas.ui.selectApp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class FragmentSliderAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> fragments = new ArrayList<>();



    public FragmentSliderAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        fragments.add(new SlideFragment());
        fragments.add(new SlideFragment());
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return fragments.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Slide " + position;
    }

}