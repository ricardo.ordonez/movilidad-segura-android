package co.com.arlsura.personas.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/19/17.
 */

public class Area extends RealmObject {

  /**
   * id : 1
   * nombreUbicacion : ADMINISTRACION
   * estado : Activo
   */
  @PrimaryKey private int id;
  private String nombreUbicacion;
  private String estado;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNombreUbicacion() {
    return nombreUbicacion;
  }

  public void setNombreUbicacion(String nombreUbicacion) {
    this.nombreUbicacion = nombreUbicacion;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }
}
