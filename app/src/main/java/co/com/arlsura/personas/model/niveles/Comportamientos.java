package co.com.arlsura.personas.model.niveles;

import co.com.arlsura.personas.api.WebServices;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class Comportamientos extends RealmObject {
  /**
   * id : 2
   * comportamiento : TRABAJO CON PULIDORA SIN PROTECCIÓN AUDITIVA
   * estado : Activo
   * nitEmpresa : 800256161
   */
  @PrimaryKey private int id;
  private String comportamiento;
  private String estado;
  private String nitEmpresa;

  // MARK Local properties

  private String observacion;
  private String descripcionSituacion;
  private String planMejora;
  private String valoracion;
  private Boolean compartamientoInseguro = false;
  private int position;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getComportamiento() {
    return comportamiento;
  }

  public void setComportamiento(String comportamiento) {
    this.comportamiento = comportamiento;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

  public String getNitEmpresa() {
    return nitEmpresa;
  }

  public void setNitEmpresa(String nitEmpresa) {
    this.nitEmpresa = nitEmpresa;
  }

  public String getObservacion() {
    return observacion;
  }

  public void setObservacion(String observacion) {
    this.observacion = observacion;
  }

  public String getDescripcionSituacion() {
    return descripcionSituacion;
  }

  public void setDescripcionSituacion(String descripcionSituacion) {
    this.descripcionSituacion = descripcionSituacion;
  }

  public String getPlanMejora() {
    return planMejora;
  }

  public void setPlanMejora(String planMejora) {
    this.planMejora = planMejora;
  }

  public String getValoracion() {
    return valoracion;
  }

  public void setValoracion(String valoracion) {
    this.valoracion = valoracion;
  }

  public Boolean getCompartamientoInseguro() {
    return compartamientoInseguro;
  }

  public void setCompartamientoInseguro(Boolean compartamientoInseguro) {
    this.compartamientoInseguro = compartamientoInseguro;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  @Override public String toString() {
    return WebServices.getGson().toJson(this);
  }
}