package co.com.arlsura.personas.view;

/**
 * Created by home on 11/9/17.
 */

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

public class InstantAutoComplete extends AppCompatAutoCompleteTextView {

  public InstantAutoComplete(Context context) {
    super(context);
  }

  public InstantAutoComplete(Context arg0, AttributeSet arg1) {
    super(arg0, arg1);
  }

  public InstantAutoComplete(Context arg0, AttributeSet arg1, int arg2) {
    super(arg0, arg1, arg2);
  }

  @Override public boolean enoughToFilter() {
    return true;
  }

  @Override
  protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
    super.onFocusChanged(focused, direction, previouslyFocusedRect);
    if (focused) {
      //performFiltering(getText(), 0);
      showDropDown();
    }
  }
}