package co.com.arlsura.personas.ui.observer.assesment;

import co.com.arlsura.personas.model.niveles.Comportamientos;

/**
 * Created by home on 11/13/17.
 */

public interface AssesmentContract  {
  void onItemChange(Comportamientos item);
  void onListOk(boolean isOk);
}
