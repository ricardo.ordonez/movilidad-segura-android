package co.com.arlsura.personas.ui.observer;

import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.widget.Toolbar;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.dao.ProcesoDao;
import co.com.arlsura.personas.dao.UserDao;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.EstandaresProceso;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Nivel2;
import co.com.arlsura.personas.model.niveles.Nivel3;
import co.com.arlsura.personas.model.niveles.Nivel4;
import co.com.arlsura.personas.model.niveles.Nivel5;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.model.niveles.StatusSync;
import co.com.arlsura.personas.model.niveles.actividadesProceso;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataContract;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataPresenter;
import co.com.arlsura.personas.view.CustomSpinner;
import io.realm.Realm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import timber.log.Timber;

public class BasicData extends BaseActivity implements BasicDataContract {
  public static Map<String, Object> paramsObserver = new HashMap<>();
  @BindView(R.id.toolbarDatosBasicos) Toolbar toolbarDatosBasicos;
  @BindView(R.id.lblNiveles) TextView lblNiveles;
  @BindView(R.id.edtNivel1) CustomSpinner edtNivel1;
  @BindView(R.id.edtNivel2) CustomSpinner edtNivel2;
  @BindView(R.id.edtNivel3) CustomSpinner edtNivel3;
  @BindView(R.id.edtNivel4) CustomSpinner edtNivel4;
  @BindView(R.id.edtNivel5) CustomSpinner edtNivel5;
  @BindView(R.id.lblProcesos) TextView lblProcesos;
  @BindView(R.id.edtNivel6) CustomSpinner edtNivel6;
  @BindView(R.id.lblEstadarEvaluar) TextView lblEstadarEvaluar;
  @BindView(R.id.edtNivel7) CustomSpinner edtNivel7;
  @BindView(R.id.lblTarea) TextView lblTarea;
  @BindView(R.id.edtNivel8) CustomSpinner edtNivel8;
  @BindView(R.id.btnContinuar) Button btnContinuar;

  private BasicDataPresenter presenter;
  private List<Nivel1> nivel1List = new ArrayList<>();
  private List<Nivel2> nivel2List = new ArrayList<>();
  private List<Nivel3> nivel3List = new ArrayList<>();
  private List<Nivel4> nivel4List = new ArrayList<>();
  private List<Nivel5> nivel5List = new ArrayList<>();

  private List<Proceso> procesoList = new ArrayList<>(); // 6
  private List<EstandaresProceso> estandaresProcesoList = new ArrayList<>(); // 7
  private List<actividadesProceso> actividadesProcesoList = new ArrayList<>(); //8

  private Proceso currentProceso;

  private List<String> nivel1ListStr = new ArrayList<>();
  private List<String> nivel2ListStr = new ArrayList<>();
  private List<String> nivel3ListStr = new ArrayList<>();
  private List<String> nivel4ListStr = new ArrayList<>();
  private List<String> nivel5ListStr = new ArrayList<>();

  private List<String> nivel6ListStr = new ArrayList<>();
  private List<String> nivel7ListStr = new ArrayList<>();
  private List<String> nivel8ListStr = new ArrayList<>();

  //Process States
  private SparseArray<List<Proceso>> processHistory = new SparseArray<>();
  boolean userTouch = false;
  int selectedLevel;
  String actualMessage;
  int processLevel;


  @Override public int getLayoutId() {
    return R.layout.activity_basic_data;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initPresenter();
    presenter.loadNiveles();
    setSupportActionBar(toolbarDatosBasicos);
    toolbarDatosBasicos.setNavigationOnClickListener(view -> finish());
    isEnabled(btnContinuar, false);
    initUi();
  }

  private void initUi() {
    edtNivel1.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel2.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel3.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel4.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel5.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel6.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel7.setOnItemSelectedListener(onItemSelectedListener);
    edtNivel8.setOnItemSelectedListener(onItemSelectedListener);


  }


  private void updateItems(List<Nivel1> nivel1List) {

    for (Nivel1 item1 : nivel1List) {
      nivel1ListStr.add(item1.getNombreNivel());

      for (Nivel2 item2 : item1.getNiveles2()) {
        nivel2List.add(item2);
        nivel2ListStr.add(item2.getNombreNivel());
        for (Nivel3 item3 : item2.getNiveles3()) {
          nivel3List.add(item3);
          nivel3ListStr.add(item3.getNombreNivel());
          for (Nivel4 item4 : item3.getNiveles4()) {
            nivel4List.add(item4);
            nivel4ListStr.add(item4.getNombreNivel());
            for (Nivel5 item5 : item4.getNiveles5()) {
              nivel5List.add(item5);
              nivel5ListStr.add(item5.getNombreNivel());
            }
          }
        }
      }
    }

    edtNivel1.initializeStringValues(nivel1ListStr, edtNivel1.getPrompt().toString());
    edtNivel2.initializeStringValues(nivel2ListStr, edtNivel2.getPrompt().toString());
    edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());

  }

  private void updateItems2(List<Nivel2> list) {
    this.nivel2List.clear();
    this.nivel2ListStr.clear();
    this.nivel3List.clear();
    this.nivel3ListStr.clear();
    this.nivel4List.clear();
    this.nivel4ListStr.clear();
    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel2 item2 : list) {
      nivel2List.add(item2);
      nivel2ListStr.add(item2.getNombreNivel());
      for (Nivel3 item3 : item2.getNiveles3()) {
        nivel3List.add(item3);
        nivel3ListStr.add(item3.getNombreNivel());
        for (Nivel4 item4 : item3.getNiveles4()) {
          nivel4List.add(item4);
          nivel4ListStr.add(item4.getNombreNivel());
          for (Nivel5 item5 : item4.getNiveles5()) {
            nivel5List.add(item5);
            nivel5ListStr.add(item5.getNombreNivel());
          }
        }
      }
    }

    edtNivel2.initializeStringValues(nivel2ListStr, edtNivel2.getPrompt().toString());
    edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems3(List<Nivel3> list) {
    this.nivel3List.clear();
    this.nivel3ListStr.clear();
    this.nivel4List.clear();
    this.nivel4ListStr.clear();
    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel3 item3 : list) {
      nivel3List.add(item3);
      nivel3ListStr.add(item3.getNombreNivel());
      for (Nivel4 item4 : item3.getNiveles4()) {
        nivel4List.add(item4);
        nivel4ListStr.add(item4.getNombreNivel());
        for (Nivel5 item5 : item4.getNiveles5()) {
          nivel5List.add(item5);
          nivel5ListStr.add(item5.getNombreNivel());
        }
      }
    }

    edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems4(List<Nivel4> list) {

    this.nivel4List.clear();
    this.nivel4ListStr.clear();
    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel4 item4 : list) {
      nivel4List.add(item4);
      nivel4ListStr.add(item4.getNombreNivel());
      for (Nivel5 item5 : item4.getNiveles5()) {
        nivel5List.add(item5);
        nivel5ListStr.add(item5.getNombreNivel());
      }
    }

    edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateItems5(List<Nivel5> list) {

    this.nivel5List.clear();
    this.nivel5ListStr.clear();

    for (Nivel5 item5 : list) {
      nivel5List.add(item5);
      nivel5ListStr.add(item5.getNombreNivel());
    }
    edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
  }

  private void updateProcesosStringList() {
    if(procesoList.isEmpty())
      return;

    estandaresProcesoList.clear();
    nivel7ListStr.clear();
    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (Proceso itemProceso : procesoList) {
      nivel6ListStr.add(itemProceso.getProceso());
      for (EstandaresProceso itemEstandaresProceso : itemProceso.getEstandaresProcesos()) {
        nivel7ListStr.add(itemEstandaresProceso.getEstandares().getEstandar());
        estandaresProcesoList.add(itemEstandaresProceso);
      }
      for (actividadesProceso itemActividadesProceso : itemProceso.getActividadesProcesos()) {
        nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
        actividadesProcesoList.add(itemActividadesProceso);
      }
    }

  }

  private void updateProcesos7(Proceso itemProceso) {
    estandaresProcesoList.clear();
    nivel7ListStr.clear();

    for (EstandaresProceso itemEstandaresProceso : itemProceso.getEstandaresProcesos()) {
      nivel7ListStr.add(itemEstandaresProceso.getEstandares().getEstandar());
      estandaresProcesoList.add(itemEstandaresProceso);
    }

    edtNivel7.initializeStringValues(nivel7ListStr, edtNivel7.getPrompt().toString());
  }


  private void updateProcesos8(Proceso itemProceso) {
    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (actividadesProceso itemActividadesProceso : itemProceso.getActividadesProcesos()) {
      nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
      actividadesProcesoList.add(itemActividadesProceso);
    }

    edtNivel8.initializeStringValues(nivel8ListStr, edtNivel8.getPrompt().toString());
  }

  private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      userTouch = true;
      return false;
    }
  };

  private CustomSpinner.OnItemSelectedListener onItemSelectedListener = new CustomSpinner.OnItemSelectedListener() {

    @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //i = index;



    }

    @Override public void onNothingSelected(AdapterView<?> adapterView) {
      Timber.d("tipoDoumento onNothingSelected ");
    }

    @Override
    public void onAfterItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
      int fixedPosition = i;


      actualMessage = "fixedPosition ".concat(String.valueOf(fixedPosition)).concat(" - ");

      Timber.d("fixedPosition ".concat(String.valueOf(fixedPosition)));

      switch (adapterView.getId()) {
        case R.id.edtNivel1:
          paramsObserver.put("idNivel1Empresa", nivel1List.get(fixedPosition).getId());
          handleFormUIChanges(nivel1List.get(fixedPosition));
          break;
        case R.id.edtNivel2:
          handleFormUIChanges(nivel2List.get(fixedPosition));
          paramsObserver.put("idNivel2Empresa", nivel2List.get(fixedPosition).getId());
          break;
        case R.id.edtNivel3:
          handleFormUIChanges(nivel3List.get(fixedPosition));
          paramsObserver.put("idNivel3Empresa", nivel3List.get(fixedPosition).getId());
          break;
        case R.id.edtNivel4:
          handleFormUIChanges(nivel4List.get(fixedPosition));
          paramsObserver.put("idNivel4Empresa", nivel4List.get(fixedPosition).getId());
          break;
        case R.id.edtNivel5:
          handleFormUIChanges(nivel5List.get(fixedPosition));
          paramsObserver.put("idNivel5Empresa", nivel5List.get(fixedPosition).getId());

          break;
        case R.id.edtNivel6:
          currentProceso = procesoList.get(fixedPosition);



          handleFormUIChanges(currentProceso);
          paramsObserver.put("idProcesos", currentProceso.getId());
          break;
        case R.id.edtNivel7:

          paramsObserver.put("idEstandares",
                  estandaresProcesoList.get(fixedPosition).getEstandares().getId());

          break;
        case R.id.edtNivel8:
          paramsObserver.put("idActividades",
                  actividadesProcesoList.get(fixedPosition).getActividades().getId());
          break;
      }
      isEnabled(btnContinuar,isProcessSet());
    }
  };

  private boolean isProcessSet(){

    boolean isProcessSet;
    boolean isActivitySet;
    boolean isStandartSet;

    isProcessSet = paramsObserver.containsKey("idProcesos") &&  numberIsSet(paramsObserver.get("idProcesos"));
    isActivitySet = paramsObserver.containsKey("idActividades")  && numberIsSet(paramsObserver.get("idActividades"));
    isStandartSet = paramsObserver.containsKey("idEstandares")  && numberIsSet(paramsObserver.get("idEstandares"));

    return isProcessSet || isActivitySet || isStandartSet;
  }

  private boolean numberIsSet(Object object){
    if(object instanceof  String)
      return !((String) object).isEmpty();
    else if(object instanceof Integer)
      return ((Integer) object) != 0;
    return false;
  }


  private void searchParentProcess(Object actualLevel){

    if(actualLevel instanceof Nivel1)
      return;

    int levelNumber = getLevelNumber(actualLevel);
    String parentKey = String.format(Locale.US,"idNivel%dEmpresa",levelNumber);
    int parentId = Integer.parseInt((String)paramsObserver.get(parentKey));

    actualMessage = "Current Level is ".concat(String.valueOf(levelNumber))
            .concat(" - parentKey is ").concat(String.valueOf(parentKey))
            .concat(" - parentId is ").concat(String.valueOf(parentId));
  }


  private void initPresenter() {
    presenter = new BasicDataPresenter(WebServices.getApi(), new ProcesoDao(), this);
  }

  @OnClick(R.id.btnContinuar) public void onViewClicked() {

    ArrayList<Map<String, Object>> maps = new ArrayList<>();
    paramsObserver.put("valoraciones", maps);
    String json = WebServices.getGson().toJson(paramsObserver);
    Timber.d("json Params -> ".concat(json));
    User user = new UserDao().findFirst();

    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    currentProceso.setToken(user.token);
    currentProceso.setStatus(StatusSync.Pending.toString());
    currentProceso.setParams(json);
    realm.commitTransaction();
    ProcesoDao procesoDao = new ProcesoDao();
    procesoDao.save(currentProceso);

    AssessmentList.paramsObserver = paramsObserver;
    AssessmentList.procesoRoot = currentProceso;
    Timber.d("json Proceso -> ".concat(currentProceso.toString()));
    goActv(AssessmentList.class, false);
  }

  @Override public void setLoading() {
    Timber.d("setLoading");
    showLoading(getString(R.string.loading));
  }

  @Override public void hideLoading() {
    Timber.d("hideLoading");
    dissmisLoading();
  }

  @Override public void onErr(String err) {
    Timber.d("onErr");
    showLoading(err);
  }

  @Override public void onErr(int err) {
    Timber.d(getString(err));
    showAlertDialog(err);
  }

  @Override public void onLoadNiveles(List<Nivel1> list) {
    Timber.d("onLoadNiveles ".concat(String.valueOf(list.size())));
    this.nivel1List = list;


    for(int i = 0; i<nivel1List.size(); i++){
      nivel1ListStr.add(nivel1List.get(i).getNombreNivel());
    }
    selectedLevel = 1;
    populateLevelSpinners(1);
  }

  @Override public void onLoad(List<Comportamientos> list) {

  }

  @Override public void onLoadRootProceso(Proceso procesoRoot) {

  }



  // Handle the From UI
  private void handleFormUIChanges(Object nivel){

    updateData(nivel);

    populateLevelSpinners(selectedLevel + 1);
    updateSpinnersVisibility(selectedLevel + 1);

    populateProcessSpinners();
    updateProcessSpinnersVisibility();

    if(isProcessEmpty()){
      showAlertDialog("Actualmente no tienes procesos para evaluar asignados, intenta mas tarde");
      final Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          //Do something after 100ms
          goActv(History.class,true);
        }
      }, 5000);
    }


  }

  private boolean isProcessEmpty(){

    switch (selectedLevel){
      case 1:
        return nivel2List.isEmpty() && procesoList.isEmpty();
      case 2:
        return nivel3List.isEmpty() && procesoList.isEmpty();
      case 3:
        return nivel4List.isEmpty() && procesoList.isEmpty();
      case 4:
        return nivel5List.isEmpty() && procesoList.isEmpty();
        default:
          return false;
    }
  }


  private void updateData(Object nivel){
    selectedLevel = getLevelNumber(nivel);

    if(selectedLevel > 0 && selectedLevel < 6){

      clearLevelsFrom(selectedLevel);
      fillNextLevelLists(nivel);
      updateLevelProcess(nivel);




    }else if(nivel instanceof Proceso){
      updateSubProcessData((Proceso) nivel);
    }

  }
  private void updateSpinnersVisibility(int newLevel){

    edtNivel2.setVisibility((newLevel > 1 && !nivel2ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
    edtNivel3.setVisibility((newLevel > 2 && !nivel3ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
    edtNivel4.setVisibility((newLevel > 3 && !nivel4ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
    edtNivel5.setVisibility((newLevel > 4 && !nivel5ListStr.isEmpty()) ? View.VISIBLE : View.GONE );
  }

  private void updateProcessSpinnersVisibility(){

    lblProcesos.setVisibility(procesoList.isEmpty() ? View.GONE : View.VISIBLE);
    edtNivel6.setVisibility(procesoList.isEmpty() ? View.GONE : View.VISIBLE);

    if(currentProceso == null){
      lblEstadarEvaluar.setVisibility(View.GONE);
      lblTarea.setVisibility(View.GONE);
      edtNivel7.setVisibility(View.GONE);
      edtNivel8.setVisibility(View.GONE);
      return;
    }

    lblEstadarEvaluar.setVisibility(currentProceso.getEstandaresProcesos().isEmpty() ? View.GONE : View.VISIBLE);
    edtNivel7.setVisibility(currentProceso.getEstandaresProcesos().isEmpty() ? View.GONE : View.VISIBLE);
    lblTarea.setVisibility(currentProceso.getEstandaresProcesos().isEmpty() ? View.GONE : View.VISIBLE);
    edtNivel8.setVisibility(currentProceso.getActividadesProcesos().isEmpty() ? View.GONE : View.VISIBLE);
  }

  private void populateProcessSpinners(){
      if(selectedLevel != 6)
        edtNivel6.initializeStringValues(nivel6ListStr, edtNivel6.getPrompt().toString());
        edtNivel7.initializeStringValues(nivel7ListStr, edtNivel7.getPrompt().toString());
        edtNivel8.initializeStringValues(nivel8ListStr, edtNivel8.getPrompt().toString());
  }
  private void populateLevelSpinners(int nivel){

    switch (nivel){
      case 1:
        edtNivel1.initializeStringValues(nivel1ListStr, edtNivel1.getPrompt().toString());
        break;
      case 2:
        edtNivel2.initializeStringValues(nivel2ListStr, edtNivel2.getPrompt().toString());
        break;
      case 3:
        edtNivel3.initializeStringValues(nivel3ListStr, edtNivel3.getPrompt().toString());
        break;
      case 4:
        edtNivel4.initializeStringValues(nivel4ListStr, edtNivel4.getPrompt().toString());
        break;
      case 5:
        edtNivel5.initializeStringValues(nivel5ListStr, edtNivel5.getPrompt().toString());
          break;
    }


  }

  // Handle the From Data







  private void updateLevelProcess(Object nivel){

    int levelNumber = getLevelNumber(nivel);
    List<Proceso> procesos = new ArrayList<>();

    switch (levelNumber){
      case 1:
        procesos = ((Nivel1) nivel).getProcesos();
        break;
      case 2:
        procesos = ((Nivel2) nivel).getProcesos();
        break;
      case 3:
        procesos = ((Nivel3) nivel).getProcesos();
        break;
      case 4:
        procesos = ((Nivel4) nivel).getProcesos();
        break;
      case 5:
        procesos = ((Nivel5) nivel).getProcesos();
        break;
    }


    if(!isListSet(procesos)){

      for (int parentNumber = (levelNumber - 1); parentNumber>0; parentNumber--){

        procesos = searchLevelProcess(parentNumber);
        if(isListSet(procesos)){
          break;
        }

      }
    }

    procesoList = new ArrayList<>(procesos);
    updateProcesosStringList();
  }

  private List<Proceso> searchLevelProcess(int levelNumber){
    List<Proceso> procesos = new ArrayList<>();
    String levelKey = String.format(Locale.US,"idNivel%dEmpresa",levelNumber);

    int levelId = (Integer) paramsObserver.get(levelKey);

    switch (levelNumber){
      case 1:
        for(Nivel1 nivel1 : nivel1List){
          if(nivel1.getId() == levelId && !nivel1.getProcesos().isEmpty())
            procesos = nivel1.getProcesos();
        }
        break;
      case 2:
        for(Nivel2 nivel2 : nivel2List){
          if(nivel2.getId() == levelId && !nivel2.getProcesos().isEmpty())
            procesos = nivel2.getProcesos();
        }
        break;
      case 3:
        for(Nivel3 nivel3 : nivel3List){
          if(nivel3.getId() == levelId && !nivel3.getProcesos().isEmpty())
            procesos = nivel3.getProcesos();
        }
        break;
      case 4:
        for(Nivel4 nivel4 : nivel4List){
          if(nivel4.getId() == levelId && !nivel4.getProcesos().isEmpty())
            procesos = nivel4.getProcesos();
        }
        break;

      case 5:
        for(Nivel5 nivel5 : nivel5List){
          if(nivel5.getId() == levelId && !nivel5.getProcesos().isEmpty())
            procesos = nivel5.getProcesos();
        }
        break;
    }



    return procesos;
  }

  private void fillNextLevelLists(Object level){
      int actual = selectedLevel;
      switch (actual){
        case 1:
          Nivel1 nivel1 = (Nivel1) level;
            nivel2List.addAll(nivel1.getNiveles2());
          for(int i = 0; i<nivel1.getNiveles2().size(); i++){
            nivel2ListStr.add(nivel2List.get(i).getNombreNivel());
          }

          break;
        case 2:
          Nivel2 nivel2 = (Nivel2) level;
          nivel3List.addAll(nivel2.getNiveles3());
          for(int i = 0; i<nivel2.getNiveles3().size(); i++){
            nivel3ListStr.add(nivel3List.get(i).getNombreNivel());
          }

          break;
         case 3:
           Nivel3 nivel3 = (Nivel3) level;
           nivel4List.addAll(nivel3.getNiveles4());
           for(int i = 0; i<nivel3.getNiveles4().size(); i++){
             nivel4ListStr.add(nivel4List.get(i).getNombreNivel());
           }

           break;
       case 4:
         Nivel4 nivel4 = (Nivel4) level;
         nivel5List.addAll(nivel4.getNiveles5());
         for(int i = 0; i<nivel4.getNiveles5().size(); i++){
           nivel5ListStr.add(nivel5List.get(i).getNombreNivel());
         }
         break;
      }

  }



  private void updateProcessData(List<Proceso> procesos){

    if(procesos.isEmpty()){
      return;
    }


    procesoList.clear();
    nivel6ListStr.clear();
    nivel7ListStr.clear();
    nivel8ListStr.clear();


    // Populate Procesos
    procesoList.addAll(procesos);
    for(Proceso proceso : procesos){
      nivel6ListStr.add(proceso.getProceso());
    }
  }

  private void updateSubProcessData(Proceso proceso){

    estandaresProcesoList.clear();
    nivel7ListStr.clear();

    for(EstandaresProceso estandares : proceso.getEstandaresProcesos()){
      nivel7ListStr.add(estandares.getEstandares().getEstandar());
      estandaresProcesoList.add(estandares);
    }


    actividadesProcesoList.clear();
    nivel8ListStr.clear();

    for (actividadesProceso itemActividadesProceso : proceso.getActividadesProcesos()) {
      nivel8ListStr.add(itemActividadesProceso.getActividades().getActividad());
      actividadesProcesoList.add(itemActividadesProceso);
    }



  }

  private void clearLevelsFrom(int level){

    if(level < 2){
      nivel2List.clear();
      nivel2ListStr.clear();
    }


    if(level < 3){
      nivel3List.clear();
      nivel3ListStr.clear();
    }

    if(level < 4){
      nivel4List.clear();
      nivel4ListStr.clear();
    }

    if(level < 5){
      nivel5List.clear();
      nivel5ListStr.clear();
    }

    if(level < 2){
      currentProceso = null;
      procesoList.clear();
      estandaresProcesoList.clear();
      actividadesProcesoList.clear();
    }


    nivel6ListStr.clear();
    nivel7ListStr.clear();
    nivel8ListStr.clear();

    if(paramsObserver.containsKey("idProcesos")){paramsObserver.remove("idProcesos");}
    if(paramsObserver.containsKey("idActividades")){paramsObserver.remove("idActividades");}
    if(paramsObserver.containsKey("idEstandares")){paramsObserver.remove("idEstandares");}

  }


  // fix
  private int getLevelNumber(Object level){

    if(level instanceof Nivel1)
      return 1;

    if(level instanceof Nivel2)
      return 2;

    if(level instanceof Nivel3)
      return 3;

    if(level instanceof Nivel4)
      return 4;

    if(level instanceof Nivel5)
      return 5;

    if(level instanceof Proceso)
      return 6;

      return -1;
  }



}
