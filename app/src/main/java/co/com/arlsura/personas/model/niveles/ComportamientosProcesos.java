package co.com.arlsura.personas.model.niveles;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class ComportamientosProcesos extends RealmObject {

  /**
   * id : 2
   * comportamientos : {"id":2,"comportamiento":"TRABAJO CON PULIDORA SIN PROTECCIÓN
   * AUDITIVA","estado":"Activo","nitEmpresa":"800256161"}
   */
  @PrimaryKey private int id;
  private Comportamientos comportamientos;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Comportamientos getComportamientos() {
    return comportamientos;
  }

  public void setComportamientos(Comportamientos comportamientos) {
    this.comportamientos = comportamientos;
  }

  public ComportamientosProcesos() {
  }
}
