package co.com.arlsura.personas.base;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import co.com.arlsura.personas.BuildConfig;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import co.com.arlsura.personas.ui.SelectProfile;
import co.com.arlsura.personas.ui.observer.basicData.BasicDataContract;
import co.com.arlsura.personas.util.RealmBackupRestore;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tbruyelle.rxpermissions2.RxPermissions;
import java.util.List;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {

  private MaterialDialog loading;

  public abstract int getLayoutId();

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getLayoutId());
    ButterKnife.bind(this);
    //backupRealm();
  }

  @Override protected void onResume() {
    super.onResume();
    //backupRealm();
  }

  protected void backupRealm() {
    if (BuildConfig.DEBUG) {
      RxPermissions rxPermissions = new RxPermissions(this);
      rxPermissions.setLogging(BuildConfig.DEBUG);
      rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(granted -> {
        Timber.e("Permisin has " + granted);
        if (granted) {
          RealmBackupRestore realmBackupRestore = new RealmBackupRestore(this);
         // realmBackupRestore.backup();
        }
      });
    }//
  }

  public void goActv(Class<?> cls, boolean clear) {
    Intent intent = new Intent(getApplicationContext(), cls);
    if (clear) intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
  }

  public void goActv(@NonNull Intent intent, boolean clear) {
    if (clear) intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
  }

  public void isEnabled(View view, boolean enabled) {
    view.setEnabled(enabled);
    view.setAlpha(enabled ? 1.0f : 0.5f);
  }

  public void showLoading(@NonNull String text) {
    runOnUiThread(() -> {
      MaterialDialog.Builder builder = new MaterialDialog.Builder(this).title(R.string.app_name)
          .content(text)
          .canceledOnTouchOutside(false)
          .progress(true, 0)
          .autoDismiss(false)
          .progressIndeterminateStyle(true);
      loading = builder.build();
      loading.show();
    });
  }

  public void dissmisLoading() {
    runOnUiThread(() -> {
      if(loading != null) {loading.dismiss();}
    });
  }

  public void showAlertDialog(int text) {
    MaterialDialog dialog = new MaterialDialog.Builder(this).title(R.string.app_name)
        .content(text)
        .positiveText(R.string.agree)
        .autoDismiss(true)
        .canceledOnTouchOutside(false)
        .dismissListener(dialogInterface -> Timber.d("dismissListener dismiss"))
        .onPositive((dialog1, which) -> {
          Timber.d("onPositive click");
          dialog1.dismiss();
        })
        .show();
  }

  public void showAlertDialog(String text) {
    MaterialDialog dialog = new MaterialDialog.Builder(this).title(R.string.app_name)
        .content(text)
        .positiveText(R.string.agree)
        .autoDismiss(true)
        .canceledOnTouchOutside(false)
        .dismissListener(dialogInterface -> Timber.d("dismissListener dismiss"))
        .onPositive((dialog1, which) -> {
          Timber.d("onPositive click");
          dialog1.dismiss();
        })
        .show();
  }
  public void showAlertDialog(int text, confirmDialog listener) {
    MaterialDialog dialog = new MaterialDialog.Builder(this).title(R.string.app_name)
        .content(text)
        .positiveText(R.string.agree)
        .negativeText(R.string.dissagree)
        .canceledOnTouchOutside(false)
        .autoDismiss(true)
        .dismissListener(dialogInterface -> Timber.d("dismissListener dismiss"))
        .onPositive((dialog1, which) -> {
          Timber.d("onPositive click");
          listener.onPositive();
          dialog1.dismiss();
        })
        .onNegative((dialog1, which) -> {
          Timber.d("onNegative click");
          listener.onNegative();
          dialog1.dismiss();
        })
        .show();
  }

  public void showAlertDialogNeutral(int text, confirmDialog listener) {
    MaterialDialog dialog = new MaterialDialog.Builder(this).title(R.string.app_name)
        .content(text)
        .positiveText(R.string.agree)
        .canceledOnTouchOutside(false)
        .autoDismiss(true)
        .dismissListener(dialogInterface -> {
          Timber.d("dismissListener dismiss");
          listener.onPositive();
        })
        .onPositive((dialog1, which) -> {
          Timber.d("onPositive click");
          dialog1.dismiss();
        })
        .show();
  }

  public interface confirmDialog {
    void onPositive();

    void onNegative();
  }

  public boolean showNext(Object currentSelected, CharSequence prompt) {
    return !currentSelected.toString().equalsIgnoreCase(prompt.toString());
  }

  public boolean showSubLevls(List<?> list) {
    return list != null && list.size() > 0;
  }

  public boolean isListSet(List<?> list) {
    return list != null && list.size() > 0;
  }
  public BasicDataContract basicDataContract = new BasicDataContract() {
    @Override public void onLoadNiveles(List<Nivel1> list) {

    }

    @Override public void onLoad(List<Comportamientos> list) {

    }

    @Override public void onLoadRootProceso(Proceso procesoRoot) {

    }

    @Override public void setLoading() {
        showLoading("Cargando");
    }

    @Override public void hideLoading() {
      dissmisLoading();
    }

    @Override public void onErr(String err) {

    }

    @Override public void onErr(int err) {

    }
  };

  public void startOver(){
    try{

      final Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          //Do something after 100ms
          goActv(SelectProfile.class,true);

        }
      }, 1000);
    }catch (Exception e){
      Timber.d("Error starting over");
    }

  }

  public interface KeyboardVisibilityListener {

    void onKeyboardVisibilityChanged(boolean keyboardVisible);

  }

  public static int mAppHeight;

  public static int currentOrientation = -1;

  public static boolean blockScroll;


}
