package co.com.arlsura.personas.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class Historial extends RealmObject {

  /**
   * id : 39
   * nitEmpresa : 800256161
   * codigoObservacion : 201710611247
   * fechaRegistro : 2017-11-04 00:00:00.0
   * horaRegistro : 16:53
   * personasObservar : 1
   * idProceso : 1
   * infoProceso : PRUEBAS FUNCIONAMIENTO
   * idEstandar : 1
   * infoEstandar : VERIFICACION DE COMPONENTES
   * idActividad : 1
   * infoActividad : VALIDACION DE PROCESOS
   * idNivel1Empresa : 1
   * infoNivel1Empresa : ARL SURA
   * idNivel2Empresa : 1
   * infoNivel2Empresa : MEDELLIN
   * idNivel3Empresa : 0
   * infoNivel3Empresa :
   * idNivel4Empresa : 0
   * infoNivel4Empresa :
   * idNivel5Empresa : 0
   * infoNivel5Empresa :
   */
  @PrimaryKey private int id;
  private String nitEmpresa;
  private String codigoObservacion;
  private String fechaRegistro;
  private String horaRegistro;
  private int personasObservar;
  private int idProceso;
  private String infoProceso;
  private int idEstandar;
  private String infoEstandar;
  private int idActividad;
  private String infoActividad;
  private int idNivel1Empresa;
  private String infoNivel1Empresa;
  private int idNivel2Empresa;
  private String infoNivel2Empresa;
  private int idNivel3Empresa;
  private String infoNivel3Empresa;
  private int idNivel4Empresa;
  private String infoNivel4Empresa;
  private int idNivel5Empresa;
  private String infoNivel5Empresa;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNitEmpresa() {
    return nitEmpresa;
  }

  public void setNitEmpresa(String nitEmpresa) {
    this.nitEmpresa = nitEmpresa;
  }

  public String getCodigoObservacion() {
    return codigoObservacion;
  }

  public void setCodigoObservacion(String codigoObservacion) {
    this.codigoObservacion = codigoObservacion;
  }

  public String getFechaRegistro() {
    return fechaRegistro;
  }

  public void setFechaRegistro(String fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }

  public String getHoraRegistro() {
    return horaRegistro;
  }

  public void setHoraRegistro(String horaRegistro) {
    this.horaRegistro = horaRegistro;
  }

  public int getPersonasObservar() {
    return personasObservar;
  }

  public void setPersonasObservar(int personasObservar) {
    this.personasObservar = personasObservar;
  }

  public int getIdProceso() {
    return idProceso;
  }

  public void setIdProceso(int idProceso) {
    this.idProceso = idProceso;
  }

  public String getInfoProceso() {
    return infoProceso;
  }

  public void setInfoProceso(String infoProceso) {
    this.infoProceso = infoProceso;
  }

  public int getIdEstandar() {
    return idEstandar;
  }

  public void setIdEstandar(int idEstandar) {
    this.idEstandar = idEstandar;
  }

  public String getInfoEstandar() {
    return infoEstandar;
  }

  public void setInfoEstandar(String infoEstandar) {
    this.infoEstandar = infoEstandar;
  }

  public int getIdActividad() {
    return idActividad;
  }

  public void setIdActividad(int idActividad) {
    this.idActividad = idActividad;
  }

  public String getInfoActividad() {
    return infoActividad;
  }

  public void setInfoActividad(String infoActividad) {
    this.infoActividad = infoActividad;
  }

  public int getIdNivel1Empresa() {
    return idNivel1Empresa;
  }

  public void setIdNivel1Empresa(int idNivel1Empresa) {
    this.idNivel1Empresa = idNivel1Empresa;
  }

  public String getInfoNivel1Empresa() {
    return infoNivel1Empresa;
  }

  public void setInfoNivel1Empresa(String infoNivel1Empresa) {
    this.infoNivel1Empresa = infoNivel1Empresa;
  }

  public int getIdNivel2Empresa() {
    return idNivel2Empresa;
  }

  public void setIdNivel2Empresa(int idNivel2Empresa) {
    this.idNivel2Empresa = idNivel2Empresa;
  }

  public String getInfoNivel2Empresa() {
    return infoNivel2Empresa;
  }

  public void setInfoNivel2Empresa(String infoNivel2Empresa) {
    this.infoNivel2Empresa = infoNivel2Empresa;
  }

  public int getIdNivel3Empresa() {
    return idNivel3Empresa;
  }

  public void setIdNivel3Empresa(int idNivel3Empresa) {
    this.idNivel3Empresa = idNivel3Empresa;
  }

  public String getInfoNivel3Empresa() {
    return infoNivel3Empresa;
  }

  public void setInfoNivel3Empresa(String infoNivel3Empresa) {
    this.infoNivel3Empresa = infoNivel3Empresa;
  }

  public int getIdNivel4Empresa() {
    return idNivel4Empresa;
  }

  public void setIdNivel4Empresa(int idNivel4Empresa) {
    this.idNivel4Empresa = idNivel4Empresa;
  }

  public String getInfoNivel4Empresa() {
    return infoNivel4Empresa;
  }

  public void setInfoNivel4Empresa(String infoNivel4Empresa) {
    this.infoNivel4Empresa = infoNivel4Empresa;
  }

  public int getIdNivel5Empresa() {
    return idNivel5Empresa;
  }

  public void setIdNivel5Empresa(int idNivel5Empresa) {
    this.idNivel5Empresa = idNivel5Empresa;
  }

  public String getInfoNivel5Empresa() {
    return infoNivel5Empresa;
  }

  public void setInfoNivel5Empresa(String infoNivel5Empresa) {
    this.infoNivel5Empresa = infoNivel5Empresa;
  }
}
