package co.com.arlsura.personas.ui.defaultData;

import co.com.arlsura.personas.base.BaseContract;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import java.util.List;

/**
 * Created by home on 11/7/17.
 */

public interface defaultDataContract extends BaseContract {
  void onLoadPreguntas(List<Preguntas> preguntasList);
}
