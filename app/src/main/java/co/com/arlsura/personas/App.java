package co.com.arlsura.personas;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import co.com.arlsura.personas.api.WebServices;
import co.com.arlsura.personas.dao.PreguntasDao;
import co.com.arlsura.personas.model.defaultData.Preguntas;
import co.com.arlsura.personas.ui.defaultData.DefaultDataPresenter;
import co.com.arlsura.personas.ui.defaultData.defaultDataContract;
import co.com.arlsura.personas.util.Constants;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.pushwoosh.Pushwoosh;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import java.util.List;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by AreaMovil on 26/10/17.
 */

public class App extends Application {

  private FirebaseRemoteConfig mFirebaseRemoteConfig;

  @Override public void onCreate() {
    super.onCreate();
    Fabric.with(this);
    Pushwoosh.getInstance().registerForPushNotifications();

    Realm.init(this);
    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }
    initRemoteConfig();

    //removeDatabase();
  }

  private void removeDatabase() {
    if (BuildConfig.DEBUG) {
      RealmConfiguration configuration = Realm.getDefaultConfiguration();
      if (configuration != null) {
        //Timber.e("deleteDatabase");
        //Realm.deleteRealm(configuration);
      }
    }
  }

  private void installFonts(){
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/FS_Joey.otf")
            .setFontAttrId(R.attr.fontPath)
            .build()
    );

  }
  private void initRemoteConfig() {
    mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    FirebaseRemoteConfigSettings configSettings =
        new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG)
            .build();
    mFirebaseRemoteConfig.setConfigSettings(configSettings);

    mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

    long cacheExpiration = 3600 * 6; // 6 hour in seconds.
    // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
    // retrieve values from the service.
    if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
      cacheExpiration = 0;
    }
    Timber.e("urlEndPoint ".concat(mFirebaseRemoteConfig.getString(Constants.KEY_ENDPOINT_LAB)));
    // [START fetch_config_with_callback]
    // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
    // will use fetch data from the Remote Config service, rather than cached parameter values,
    // if cached parameter values are more than cacheExpiration seconds old.
    // See Best Practices in the README for more information.
    mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(task -> {
      if (task.isSuccessful()) {
        Timber.e("fetch firebase successful");
        Timber.e(
            "urlEndPoint ".concat(mFirebaseRemoteConfig.getString(Constants.KEY_ENDPOINT_LAB)));
        mFirebaseRemoteConfig.activateFetched();
        DefaultDataPresenter presenter =
            new DefaultDataPresenter(contract, WebServices.getApi(), new PreguntasDao());

        presenter.loadPreguntas();
      } else {
        Timber.e("fetch firebase failed");
      }
    });
  }

  private defaultDataContract contract = new defaultDataContract() {
    @Override public void onLoadPreguntas(List<Preguntas> preguntasList) {

    }

    @Override public void setLoading() {

    }

    @Override public void hideLoading() {

    }

    @Override public void onErr(String err) {

    }

    @Override public void onErr(int err) {

    }
  };

  @Override protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }
}

