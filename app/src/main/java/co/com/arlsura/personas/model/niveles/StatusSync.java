package co.com.arlsura.personas.model.niveles;

/**
 * Created by home on 11/8/17.
 */

public enum StatusSync {
  Pending("Pending"),
  Succes("Succes"),
  PendingSync("PendingSync");

  private final String text;

  StatusSync(final String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }
}
