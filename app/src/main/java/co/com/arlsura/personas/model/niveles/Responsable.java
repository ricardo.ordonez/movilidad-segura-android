package co.com.arlsura.personas.model.niveles;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class Responsable extends RealmObject {

  /**
   * cedula : 8105306
   * nombre : JUAN CAMILO CARTAGENA
   * cargo : DESARROLLADOR
   */

  @PrimaryKey private String cedula;
  private String nombre;
  private String cargo;

  public String getCedula() {
    return cedula;
  }

  public void setCedula(String cedula) {
    this.cedula = cedula;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getCargo() {
    return cargo;
  }

  public void setCargo(String cargo) {
    this.cargo = cargo;
  }
}
