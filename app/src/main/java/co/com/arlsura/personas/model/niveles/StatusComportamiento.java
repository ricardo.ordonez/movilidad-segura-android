package co.com.arlsura.personas.model.niveles;

/**
 * Created by home on 11/13/17.
 */

public enum StatusComportamiento {
  Pending("Activo"), Succes("Cerrado"), Incomplete("Pendiente"), PendingSync(
      "PedienteDeSincronizar");

  private final String text;

  StatusComportamiento(final String text) {
    this.text = text;
  }

  @Override public String toString() {
    return text;
  }

}


