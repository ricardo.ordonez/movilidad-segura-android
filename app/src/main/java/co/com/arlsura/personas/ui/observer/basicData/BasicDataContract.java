package co.com.arlsura.personas.ui.observer.basicData;

import co.com.arlsura.personas.base.BaseContract;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.model.niveles.Nivel1;
import co.com.arlsura.personas.model.niveles.Proceso;
import java.util.List;

/**
 * Created by home on 11/8/17.
 */

public interface BasicDataContract extends BaseContract {
  void onLoadNiveles(List<Nivel1> list);
  void onLoad(List<Comportamientos> list);
  void  onLoadRootProceso(Proceso procesoRoot);
}
