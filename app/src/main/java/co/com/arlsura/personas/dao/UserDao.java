package co.com.arlsura.personas.dao;

import co.com.arlsura.personas.base.BaseDaoContract;
import co.com.arlsura.personas.model.User;
import io.realm.Realm;

/**
 * Created by home on 11/7/17.
 */

public class UserDao implements BaseDaoContract<User> {

  public static String token = "";

  @Override public void save(User item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void update(User item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.copyToRealmOrUpdate(item);
    realm.commitTransaction();
  }

  @Override public void delete(User item) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    item.deleteFromRealm();
    realm.commitTransaction();
  }

  @Override public User findFirst() {
    User user;
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    user = realm.where(User.class).findFirst();
    realm.commitTransaction();
    return user;
  }

  public void cleanUsers() {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    realm.where(User.class).findAll().deleteAllFromRealm();
    realm.commitTransaction();
  }
  @Override public User createFromJson(String json) {
    Realm realm = Realm.getDefaultInstance();
    realm.beginTransaction();
    User user =
        realm.createObjectFromJson(User.class, json); // Save a bunch of new Customer objects
    realm.commitTransaction();
    return user;
  }


}
