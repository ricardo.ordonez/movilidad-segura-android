package co.com.arlsura.personas.ui.observer;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.base.BaseActivity;
import co.com.arlsura.personas.model.niveles.Comportamientos;
import co.com.arlsura.personas.ui.observer.assesment.AsesmentViewPagerAdapter;
import co.com.arlsura.personas.ui.observer.assesment.AssesmentContract;
import co.com.arlsura.personas.ui.observer.assesment.FragmentAsessment;
import co.com.arlsura.personas.ui.observer.assesment.FragmentStatusAsessment;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;

public class AssesmentDetail extends BaseActivity implements AssesmentContract {

  @BindView(R.id.toolbarAssesmentDetail) Toolbar toolbarAssesmentDetail;
  @BindView(R.id.detailAssementRootContainer) ViewPager detailAssementRootContainer;
  @BindView(R.id.txtCurrentPosition) TextView txtCurrentPosition;
  @BindView(R.id.bottonContaiter) RelativeLayout bottonContainer;


  public static List<Comportamientos> list;
  public static int currentPosition;
  private List<Fragment> fragmentList = new ArrayList<>();
  public static AssesmentContract listenerAssesment;

  @Override public int getLayoutId() {
    return R.layout.activity_assesment_detail;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbarAssesmentDetail);
    toolbarAssesmentDetail.setNavigationOnClickListener(view -> finish());
    AssessmentList.listenerAssesment = this;
    //String json = list.get(currentPosition).toString();
    //Timber.e("json Err ".concat(json));
    initUi();
  }

  @Override
  protected void onResume() {
    super.onResume();
    handlekeyboardState();
  }

  private void initUi() {
    if (list != null && !list.isEmpty()) {
      for (Comportamientos item : list) {
        fragmentList.add(FragmentAsessment.newInstance(item.getId(), currentPosition));
      }
      fragmentList.add(FragmentStatusAsessment.newInstance());
      AsesmentViewPagerAdapter adapter =
          new AsesmentViewPagerAdapter(getSupportFragmentManager(), fragmentList, this);
      detailAssementRootContainer.setAdapter(adapter);
      detailAssementRootContainer.setCurrentItem(currentPosition);
      detailAssementRootContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override public void onPageSelected(int position) {
          updateCurrentPosition(detailAssementRootContainer.getCurrentItem());

            handlekeyboardState();
        }

        @Override public void onPageScrollStateChanged(int state) {

        }
      });
      updateCurrentPosition(detailAssementRootContainer.getCurrentItem());
    }
  }

  boolean viewstate;

  public void handlekeyboardState(){

    Fragment fragment = fragmentList.get(detailAssementRootContainer.getCurrentItem());

    if(!(fragment instanceof FragmentAsessment)){
      blockScroll = true;
      return;
    }


      try{

      blockScroll = false;

      ScrollView scrollView = (ScrollView) fragment.getView();

      setKeyboardVisibilityListener(new KeyboardVisibilityListener() {
        @Override
        public void onKeyboardVisibilityChanged(boolean keyboardVisible) {
          Timber.d(keyboardVisible ? "SHOWN KEYBOARD" : "HIDDDEN");

          if(viewstate == keyboardVisible){
            return;
          }else
            viewstate = keyboardVisible;


          bottonContainer.setVisibility(keyboardVisible && !blockScroll ? View.INVISIBLE : View.VISIBLE);
          View view = getCurrentFocus();
          scrollView.smoothScrollTo(0,view.getTop());


        }
      },scrollView);
    }catch (Exception e ){
      Timber.d("Could not set keyboard listener on fragment scroll");
    }


  }



  public void setKeyboardVisibilityListener(final KeyboardVisibilityListener keyboardVisibilityListener,View contentView) {

    if(blockScroll)
      return;

    Activity activity = this;

    contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

      private int mPreviousHeight;

      @Override

      public void onGlobalLayout() {

        int newHeight = contentView.getHeight();

        if (newHeight == mPreviousHeight)

          return;

        mPreviousHeight = newHeight;

        if (activity.getResources().getConfiguration().orientation != currentOrientation) {

          currentOrientation = activity.getResources().getConfiguration().orientation;

          mAppHeight =0;

        }

        if (newHeight >= mAppHeight) {

          mAppHeight = newHeight;

        }

        if (newHeight != 0) {

          if (mAppHeight > newHeight) {

// Height decreased: keyboard was shown

            keyboardVisibilityListener.onKeyboardVisibilityChanged(true);

          } else

          {

// Height increased: keyboard was hidden

            keyboardVisibilityListener.onKeyboardVisibilityChanged(false);

          }

        }

      }

    });

  }



  private void updateCurrentPosition(int position) {
    Resources res = this.getResources();
    String text =
        String.format(res.getString(R.string.current_position_detail), String.valueOf(position + 1),
            String.valueOf(fragmentList.size()));
    txtCurrentPosition.setText(text);
  }

  @OnClick({ R.id.imgPrevious, R.id.imgNextTap }) public void onViewClicked(View view) {
    int position = detailAssementRootContainer.getCurrentItem();
    switch (view.getId()) {
      case R.id.imgPrevious:
        goBack();
        break;
      case R.id.imgNextTap:
        detailAssementRootContainer.setCurrentItem(position + 1);
        updateCurrentPosition(detailAssementRootContainer.getCurrentItem());
        break;
    }
  }

  public void goBack(){
    int position = detailAssementRootContainer.getCurrentItem();
    detailAssementRootContainer.setCurrentItem(position - 1);
    updateCurrentPosition(detailAssementRootContainer.getCurrentItem());
  }



  @Override public void onItemChange(Comportamientos item) {
    if (listenerAssesment != null) {
      Timber.e("onItemChange Detail");
      listenerAssesment.onItemChange(item);
    }
  }

  @Override public void onListOk(boolean isOk) {
    Timber.e("onListOk ".concat(String.valueOf(isOk)));
    FragmentStatusAsessment.isOK = isOk;
  }
}
