package co.com.arlsura.personas.ui;


import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import co.com.arlsura.personas.R;
import co.com.arlsura.personas.ui.selectApp.OptionsActivity;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Splash.this, OptionsActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
}
