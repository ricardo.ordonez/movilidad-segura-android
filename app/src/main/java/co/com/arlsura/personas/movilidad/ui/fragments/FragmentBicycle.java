package co.com.arlsura.personas.movilidad.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import co.com.arlsura.personas.movilidad.ui.VehicleMovilidad;
import co.com.arlsura.personas.R;
import timber.log.Timber;


public class FragmentBicycle extends Fragment implements com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener, View.OnClickListener {


    private EditText etMarca, etFechaVenSeguro, etModeloReferencia;
    private Button btnGuardarVehiculo;
    private int opcionFecha = 0;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog dateTimeDialog;
    SpinnerDatePickerDialogBuilder builder;
    SimpleDateFormat formatoDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public FragmentBicycle() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bicycle, container, false);
        initUi(view);

        return view;
    }

    private View initUi(View view) {
        etMarca = view.findViewById(R.id.etMarca);
        etModeloReferencia = view.findViewById(R.id.etModeloReferencia);
        etFechaVenSeguro = view.findViewById(R.id.etFechaVenSeguro);
        btnGuardarVehiculo = view.findViewById(R.id.btnGuardarVehiculo);

        etFechaVenSeguro.setOnClickListener(this);
        btnGuardarVehiculo.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        opcionFecha = view.getId();
        switch (opcionFecha) {
            case R.id.etFechaVenSeguro:
                createDatePickerDialog();
                builder.build().show();
                break;
            case R.id.btnGuardarVehiculo:
                if (validateFields()) {
                    Intent vehicleMovilidad = new Intent(getContext(), VehicleMovilidad.class);
                    vehicleMovilidad.putExtra("llave", "FragmentVehicle");
                    startActivity(vehicleMovilidad);
                }
                break;
        }
    }

    private boolean validateFields() {

        boolean validacion = true;

        if (!(!etModeloReferencia.getText().toString().trim().isEmpty() && etModeloReferencia.getText().toString().length() >= 4)) {
            validacion = false;
            etModeloReferencia.setError("El modelo de referencia no se ha diligenciado correctamente");
        }
        if (!(!etMarca.getText().toString().trim().isEmpty() && etMarca.getText().toString().length() >= 2)) {
            validacion = false;
            etMarca.setError("La marca no se ha diligenciado correctamente");
        }
        if (!(!etFechaVenSeguro.getText().toString().trim().isEmpty() && etFechaVenSeguro.getText().toString().length() == 10)) {
            validacion = false;
            etFechaVenSeguro.setError("La fecha de vencimiento del seguro no se ha diligenciado correctamente");
        }

        if (validacion) {
            return validacion;
        } else {
            return validacion;
        }
    }


    private void createDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        builder = new SpinnerDatePickerDialogBuilder()
                .context(getContext())
                .callback(FragmentBicycle.this)
                .showTitle(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .maxDate(calendar.get(Calendar.YEAR) + 1, 0, 1)
                .minDate(calendar.get(Calendar.YEAR) - 1, 0, 1);
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener =
            (view, year, monthOfYear, dayOfMonth) -> {
                String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear, dayOfMonth);
                try {
                    optionChangeDate(formatoDate.format(formatoDate.parse(date)));
                } catch (ParseException e) {
                    Timber.e(e);
                }
            };





    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String date = String.format(Locale.getDefault(), "%d-%d-%d", year, monthOfYear + 1, dayOfMonth);
        try {
            optionChangeDate(formatoDate.format(formatoDate.parse(date)));
        } catch (ParseException e) {
            Timber.e(e);
        }
    }

    private void optionChangeDate(String formatoDate) {
        switch (opcionFecha) {
            case R.id.etFechaVenSeguro:
                etFechaVenSeguro.setText(formatoDate);
                break;
        }
    }



}
