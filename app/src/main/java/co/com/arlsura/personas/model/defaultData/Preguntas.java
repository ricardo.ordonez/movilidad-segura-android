package co.com.arlsura.personas.model.defaultData;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/7/17.
 */

public class Preguntas extends RealmObject {
  /**
   * id : 1
   * codigo : VALCOMP
   * nombre : Valoración de Comportamientos
   * descripcion : null
   * respuestas : []
   */
  @PrimaryKey private int id;
  private String codigo;
  private String nombre;
  private String descripcion;
  private RealmList<Respuesta> respuestas;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public RealmList<Respuesta> getRespuestas() {
    return respuestas;
  }

  public void setRespuestas(RealmList<Respuesta> respuestas) {
    this.respuestas = respuestas;
  }
}
