package co.com.arlsura.personas.ui.observer.history;

import co.com.arlsura.personas.base.BaseContract;
import co.com.arlsura.personas.model.Historial;
import co.com.arlsura.personas.model.User;
import java.util.List;

/**
 * Created by home on 11/8/17.
 */

public interface HistoryContract extends BaseContract {
  void onLoadHistorial(List<Historial> list);
  void onLoadUser(User user);
  void close();
}
