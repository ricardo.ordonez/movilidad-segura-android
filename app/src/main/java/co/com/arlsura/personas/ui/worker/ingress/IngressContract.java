package co.com.arlsura.personas.ui.worker.ingress;

import co.com.arlsura.personas.base.BaseContract;
import co.com.arlsura.personas.model.Area;
import co.com.arlsura.personas.model.User;
import co.com.arlsura.personas.model.defaultData.Respuesta;
import co.com.arlsura.personas.model.niveles.Nivel1;
import java.util.List;

/**
 * Created by home on 11/17/17.
 */

public interface IngressContract extends BaseContract {
  void onLoadUser(User user);

  void onLoadNiveles(List<Nivel1> list);
  void onLoadInstalaciones(List<Area> areas);
  void onLoadAreas(List<Area> areas);
  void onClose();
}
