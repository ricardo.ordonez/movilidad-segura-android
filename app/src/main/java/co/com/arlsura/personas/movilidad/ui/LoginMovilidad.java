package co.com.arlsura.personas.movilidad.ui;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatEditText;
import butterknife.BindView;
import co.com.arlsura.personas.R;
import co.com.arlsura.personas.movilidad.ui.base.BaseActivity;
import co.com.arlsura.personas.movilidad.view.LoginCustomDialog;

public class  LoginMovilidad extends BaseActivity implements View.OnClickListener {

    private final static String CC = "CC", NIT = "NIT", TI = "TI";

    @BindView(R.id.btnLogin) Button btnIniciarSession;
    @BindView(R.id.edtDocumentType)
    AppCompatEditText edtDocumentType;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login_movilidad_new;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        initUi();
    }



    private void customizeEditTexts(){
        edtDocumentType.setInputType(InputType.TYPE_NULL);
        edtDocumentType.setOnTouchListener((view, motionEvent) -> {
            if(motionEvent.getAction() ==  MotionEvent.ACTION_DOWN)
                showDocumentTypeSelector(view);

            return true;
        });
    }

    public void showDocumentTypeSelector(View view){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Tipo de Documento");
        String[] types = {"Cedula", "Pasaporte","Tarjeta de Identidad"};
        b.setItems(types, (dialog, which) -> {
            dialog.dismiss();
            edtDocumentType.setText(types[which]);
        });

        b.show();
    }

    private void initUi() {

        btnIniciarSession.setEnabled(true);

        String[] documents = {"   NIT", "   CC", "   TI"};
        ArrayAdapter<String> documentsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, documents);
        documentsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       // spinnerDocuments.setAdapter(documentsAdapter);
        //etNumDocument.setOnClickListener(this);
        btnIniciarSession.setOnClickListener(this);
        customizeEditTexts();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etNumDocument:
                //validateFields();
                break;

            case R.id.btnLogin:
                LoginCustomDialog dialog = new LoginCustomDialog();
                dialog.show(getSupportFragmentManager(), "Alert");
                break;
        }
    }

    /*private boolean validateFields() {
        String selection = spinnerDocuments.getSelectedItem().toString().trim();
        switch (selection) {
            case NIT:
                if (etNumDocument.getText().toString().trim().length() >= 8 && etNumDocument.getText().toString().trim().length() <= 15) {
                    btnIniciarSession.setEnabled(true);
                } else {
                    etNumDocument.setError("Los campos no han sido diligenciados correctamente");
                }
                break;
            case CC:
                if (etNumDocument.getText().toString().trim().length() >= 3 && etNumDocument.getText().toString().trim().length() <= 11) {
                    btnIniciarSession.setEnabled(true);
                } else {
                    etNumDocument.setError("Los campos no han sido diligenciados correctamente");
                }
                break;
            case TI:
                if (etNumDocument.getText().toString().trim().length() >= 8 && etNumDocument.getText().toString().trim().length() <= 11) {
                    btnIniciarSession.setEnabled(true);
                } else {
                    etNumDocument.setError("Los campos no han sido diligenciados correctamente");
                }
                break;
        }
        return true;
    }*/


}
