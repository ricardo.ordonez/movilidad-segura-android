package co.com.arlsura.personas.model.niveles;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 11/8/17.
 */

public class actividadesProceso extends RealmObject {

  /**
   * id : 2
   * actividades : {"id":2,"actividad":"METALMECÁNICA","nitEmpresa":"800256161"}
   */
  @PrimaryKey private int id;
  private Actividades actividades;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Actividades getActividades() {
    return actividades;
  }

  public void setActividades(Actividades actividades) {
    this.actividades = actividades;
  }
}
