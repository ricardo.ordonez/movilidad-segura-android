package co.com.arlsura.personas.configuration;

/**
 * Created by home on 11/7/17.
 */

public enum Enviroment {
  Debug("debug"),
  Vpn("Vpn"),
  Uitest("uitest"),
  Release("release");

  private final String text;

  Enviroment(final String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }
}