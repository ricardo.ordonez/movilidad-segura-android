package co.com.arlsura.personas.movilidad.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import co.com.arlsura.personas.movilidad.ui.fragments.FragmentBicycle;
import co.com.arlsura.personas.movilidad.ui.fragments.FragmentMotorBike;
import co.com.arlsura.personas.movilidad.ui.fragments.FragmentVehicle;

/**
 * Created by xkiRox on 16/07/18.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public PagerAdapter(FragmentManager fm, int NumberOfTabs) {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                FragmentVehicle tab1 = new FragmentVehicle();
                return tab1;
            case 1:
                FragmentMotorBike tab2 = new FragmentMotorBike();
                return tab2;
            case 2:
                FragmentBicycle tab3 = new FragmentBicycle();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}